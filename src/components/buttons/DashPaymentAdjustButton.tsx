import React from "react";
import Icon from "@material-ui/icons/BorderColor";
import { DashButton, Enum } from "./";

export const DashPaymentAdjustButton = ({ label, record }: any) => (
  <DashButton
    Icon={Icon}
    crud={Enum.Crud.Update}
    label={label || "Adjust"}
    record={record}
    resource={Enum.Resource.Contributions}
    variant="outlined"
  />
);
