import React from "react";
import Icon from "@material-ui/icons/Add";
import { DashButton, Enum } from "./";

export const DashCreateButton = props => (
  <DashButton Icon={Icon} crud={Enum.Crud.Create} label="Create" resource={props.resource} variant="flat" />
);
