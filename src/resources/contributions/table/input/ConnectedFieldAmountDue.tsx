import { React, connect, IState } from "../";
import { FieldAmountDue, ComponentSignature } from "../readonly/FieldAmountDue";

export const ConnectedFieldAmountDue = connect<any, any, ComponentSignature>(
  (
    {
      form: {
        "record-form": { values: record = {} }
      }
    }: IState,
    { source }: any
  ) => {
    return { record };
  }
)(FieldAmountDue);
