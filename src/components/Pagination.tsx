import * as React from "react";
import { Pagination as _P } from "react-admin";
export const Pagination = props => <_P rowsPerPageOptions={[5, 10, 20]} {...props} />;
