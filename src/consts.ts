export const CONTENT_MAX_WIDTH = 1600;
export const CONTENT_MIN_WIDTH = 720;
export const VERTICAL_DASH_MAX_WIDTH = "100%";
export const HORIZONTAL_DASH_MAX_HEIGHT = "100%";
