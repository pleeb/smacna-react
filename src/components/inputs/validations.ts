export const required = v => (v ? undefined : "This field is required.");
