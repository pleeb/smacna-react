import React from "react";
import Icon from "@material-ui/icons/LocalAtm";
import { DashButton, Enum } from "./";

export const DashPaymentPayButton = props => (
  <DashButton Icon={Icon} crud={Enum.Crud.Create} label="Pay" record={props.record} resource={Enum.Resource.Payments} variant="outlined" />
);
