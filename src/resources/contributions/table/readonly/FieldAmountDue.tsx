import { React, parse, TSourceRecord, AmountSource } from "../";

export const FieldAmountDue = ({ record, source }: TSourceRecord<AmountSource>) => <span>{parse.money(record[source])}</span>;
export type ComponentSignature = React.StatelessComponent<TSourceRecord<AmountSource>>;
