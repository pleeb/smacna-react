import React from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { CardActions, withStyles } from "@material-ui/core";
import * as choices from "./choices";
import {
  Pagination,
  ISort,
  Button,
  Enum,
  Alias,
  Field,
  DatagridMapper,
  DatagridMapperXX,
  GridItem,
  GridContainer,
  SimpleShowContainer,
  Actionbar,
  Input,
  ReferenceManyContainer,
  ReferenceManyField
} from "../";

const Filters = ({ permissions, ...props }) => (
  <Field.Filter {...props}>
    <Input.DateRangeInput alwaysOn source="reporting_week_end" />
    <Input.SelectInput alwaysOn source="pay_status" choices={choices.pay_status} label={Alias["pay_status"]} />
    <Input.SelectInput alwaysOn source="pay_method" choices={choices.pay_method} label={Alias["pay_method"]} />
  </Field.Filter>
);
const Actions = ({
  bulkActions,
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
  total,
  permissions
}: any) => (
  <CardActions>
    {bulkActions &&
      React.cloneElement(bulkActions, {
        basePath,
        filterValues,
        resource,
        selectedIds,
        onUnselectItems
      })}
    {filters &&
      React.cloneElement(filters, {
        resource,
        showFilter,
        displayedFilters,
        filterValues,
        context: "button"
      })}
    <Button.ExportButton disabled={total === 0} resource={resource} sort={currentSort} filter={filterValues} exporter={exporter} />
    <Button.RefreshButton />
  </CardActions>
);

const colorMap = {
  [Enum.PayStatus.FAILED]: "tomato",
  [Enum.PayStatus.PENDING]: "gold",
  [Enum.PayStatus.SUCCESS]: "limegreen"
};
const getColor = (status: Enum.PayStatus) => colorMap[status];
const rowStyle = (record: any = {}) => ({
  background: `linear-gradient(to right, ${getColor(record.pay_status)}, transparent 10%)`
  // borderLeft: `5px solid ${getColor(record.pay_status)}`
});
const rowStyleX = (record: any = {}) => ({
  background: `linear-gradient(to right, ${getColor(record.pay_status)}, transparent 5%)`
  // borderLeft: `5px solid ${getColor(record.pay_status)}`
});
interface IRefMany {
  count?: number;
  permissions: Enum.Role;
  basePath?: string;
  resource?: string;
  source?: any;
  filter?: { pay_status: Enum.PayStatus };
  target?: "contribution_id" | "user_id" | "company_id";
  label?: string;
}
const defaultSort: ISort = { field: "payment_date", order: "DESC" };
export const ReferenceManyPayment = ({ permissions, ...props }: IRefMany) => {
  let target;
  switch (props.resource) {
    case Enum.Resource.Companies:
      target = "company_id";
      break;
    case Enum.Resource.Users:
      target = "user_id";
      break;
    case Enum.Resource.Contributions:
      target = "contribution_id";
      break;
    default:
      break;
  }
  return (
    <ReferenceManyContainer reference="payments" target={target} source="id" {...props} sort={defaultSort}>
      <DatagridMapper rowStyle={rowStyle} context={Enum.Context.Show} permissions={permissions} fixed />
    </ReferenceManyContainer>
  );
};
export const ReferenceManyPaymentX = ({ permissions, ...props }) => (
  <ReferenceManyField {...props} sort={defaultSort} reference="payments" target="contribution_id" source="id">
    <DatagridMapperXX rowStyle={rowStyleX} context={Enum.Context.ListExpand} permissions={permissions} />
  </ReferenceManyField>
);
export const PaymentList = connect(
  ({ session: { role, company_id } }: any) => ({ role, company_id }),
  {}
)(({ classes, role, permissions, company_id, ...props }: any) => {
  return (
    <Field.List
      {...props}
      pagination={<Pagination />}
      sort={defaultSort}
      filter={role === Enum.Role.User ? { company_id } : null}
      filters={<Filters permissions={permissions} />}
      actions={<Actions permissions={permissions} />}
      bulkActionButtons={false}
    >
      <DatagridMapper rowStyle={rowStyle} permissions={permissions} context={Enum.Context.List} />
    </Field.List>
  );
});
