import React from "react";
import { Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@material-ui/core";
import { SimpleShowContainer, Field } from "../";
import { connect } from "react-redux";
import { ReferenceManyField, ReferenceField } from "react-admin";

export const ReadonlyRateTable = connect(({ router: { location } }: any) => ({ location }))(({ ...props }: any) => {
  return (
    <SimpleShowContainer resource="rates" basePath="/rates" record={{ id: 1 }} label="Current Rates" withBorder>
      <ReferenceField
        fullWidth
        resource="rates"
        basePath="/rates"
        perPage={10}
        sort={{ field: "updated_at", order: "DESC" }}
        pagination={null}
        source="id"
        reference="rates"
        target="id"
        label=""
        linkType={false}
      >
        <Wrap />
      </ReferenceField>
    </SimpleShowContainer>
  );
});
// <Card style={{ maxWidth: 600, maxHeight: 300 }}>
//   <Wrap />
// </Card>

const Wrap = ({ record, ...props }: any) => {
  return (
    <Table padding="checkbox">
      <TableHead>
        <TableRow>
          <TableCell>
            <strong>Building Trade Mechanics</strong>
          </TableCell>
          <TableCell>
            <strong>Apprentice Workers</strong>
          </TableCell>
          <TableCell>
            <strong>Light Commercial Workers</strong>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell>{record.rate_mechanic}</TableCell>
          <TableCell>{record.rate_apprentice}</TableCell>
          <TableCell>{record.rate_light_commercial}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  );
};
