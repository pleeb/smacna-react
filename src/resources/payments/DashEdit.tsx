import React, { Fragment } from "react";
import BButton from "@material-ui/core/Button";
import Electronic from "@material-ui/icons/AccountBalance";
import Mail from "@material-ui/icons/Mail";
import { connect } from "react-redux";
import { change, Field, destroy } from "redux-form";
import * as choices from "./choices";
import { ReadonlyTable } from "../contributions/table";
import {
  Enum,
  DashPanel,
  Input,
  Button,
  Alias,
  GridContainer,
  GridItem,
  sourceFieldFromMap,
  sourceField,
  SimpleShowContainer,
  SimpleFormContainer,
  Actionbar,
  mui
} from "../";
import { closeAllDash, closeDash } from "../../actions";
import { refreshView } from "react-admin";
import * as validate from "./validate";

const Actions = ({ permissions, ...props }) => <Actionbar verb="Update Payment" title={(props.data || {})["cid"]} {...props} />;
const Dash = ({ permissions, anchor, record, ...props }: any) => {
  return (
    <Input.Edit
      {...props}
      title={` Update Payment`}
      resource="payments"
      basePath="/payments"
      actions={<Actions permissions={permissions} dash data={record} anchor={anchor} />}
    >
      <WrapForm permissions={permissions} value={record} />
    </Input.Edit>
  );
};
const payStatus = function(due, paid) {
  if (!+paid) return +due ? Enum.Status.FAILED : Enum.Status.SUCCESS;
  switch (Math.sign(+due - +paid)) {
    case -1:
      return Enum.Status.OVERPAY;
    case 1:
      return Enum.Status.SHORT;
    default:
      return Enum.Status.SUCCESS;
  }
};
const ConnectedStatusField = connect(({ form: { "record-form": { values: { amount_paid, amount_total } = {} as any } } }: any) => {
  return { value: payStatus(amount_total, amount_paid) };
})(({ value, label }: any) => (
  <mui.TextField
    style={{ color: value === "FAILED" || value === "SHORT" ? Enum.ColorMap.Status.DELINQUENT : Enum.ColorMap.Status.SUCCESS }}
    value={value}
    label={label}
    fullWidth
  />
)) as any;
const selectValue = e => e.target.select();
const isInvalid = e => e.target.value === "" || isNaN(e.target.value);
const options = {
  inputProps: {
    min: 0,
    onFocus: selectValue
  }
};
const normalizeDate = (value, previousValue, allValues, previousAllValues) => {
  console.log(value);
  return toDate(value);
};
const toDate = (value: string, name?: string) => (value ? value.match(/^[^\T]*/)[0] : "");
const toNum = (value, name?) => (isNaN(value) ? 0 : +value);
const ConnectedDateInput = ({ label, source, validate }: any) => {
  return (
    <Field
      component={DateField}
      name={source}
      label={label}
      type="date"
      validate={validate}
      normalize={normalizeDate}
      parse={toDate}
      format={toDate}
    />
  );
};
const ConnectedNumberInput = ({ label, source, change, validate }: any) => {
  return (
    <Field<any>
      component={NumberField}
      name={source}
      label={label}
      type="number"
      validate={validate}
      normalize={(value, previousValue, allValues, previousAllValues) => {
        if (+value && !allValues["receive_date"]) change("record-form", "receive_date", toDate(new Date().toISOString()));
        return toNum(value);
      }}
      // format={(v = 0) => {
      //   const vv = (+v || 0).toFixed(2);
      //   console.log("format", vv);
      //   return vv;
      // }}
      // parse={v => {
      //   const vv = (+v || 0).toFixed(2);
      //   console.log("parse", vv);
      //   return vv;
      // }}
    />
  );
};
const DateField = ({ input, label, type, meta: { touched, error, warning }, ...props }) => {
  return (
    <mui.TextField
      type="date"
      fullWidth
      {...input}
      InputLabelProps={{ shrink: true }}
      label={label}
      {...props}
      error={Boolean(error)}
      helperText={error}
    />
  );
};
const NumberField = ({ input, label, type, meta: { touched, error, warning }, ...props }) => {
  return (
    <mui.TextField
      type="number"
      fullWidth
      {...input}
      InputProps={{ onFocus: selectValue }}
      InputLabelProps={{ shrink: true }}
      label={label}
      {...props}
      error={Boolean(error)}
      helperText={error}
    />
  );
};
const WrapForm = connect(
  null,
  dispatch => ({
    redirect: (basePath, id, data) => {
      dispatch(closeDash(Enum.Resource.Payments, Enum.Crud.Update));
      dispatch(refreshView());
      dispatch(destroy("record-form"));
      return false;
    },
    change: (form, source, value) => dispatch(change(form, source, value))
  })
)(({ value, redirect, change, ...props }: any) => {
  return (
    <Fragment>
      <SimpleShowContainer record={value} basePath="/contributions" resource="contributions" withPad>
        {sourceField("company_name__ref")}
        {sourceField("company_id")}
        {sourceField("user_id")}
        <HR />
        {sourceField("reporting_week")}
        {sourceField("reporting_week_count")}
        {sourceField("reporting_week_start")}
        {sourceField("reporting_week_end")}
        {sourceField("payment_date")}
        <HR />
        {sourceField("amount_total")}
        {sourceField("amount_paid")}
      </SimpleShowContainer>
      <SimpleFormContainer {...props} redirect={redirect} toolbar={<Toolbar />} withPad>
        <ConnectedStatusField fullWidth source="pay_status" label={Alias["pay_status"]} />
        <ConnectedNumberInput
          validate={validate.amountPaid}
          options={options}
          change={change}
          fullWidth
          source="amount_paid"
          label={Alias["amount_paid"]}
        />
        <Input.TextInput options={options} fullWidth source="comment" label={Alias["comment"]} />
        <ConnectedDateInput validate={validate.receiveDate} source="receive_date" label={Alias["receive_date"]} />
        <ConnectedDateInput validate={validate.depositDate} source="deposit_date" label={Alias["deposit_date"]} />
      </SimpleFormContainer>
    </Fragment>
  );
}) as any;
const HR = props => <hr />;
const MarginDiv = ({ children }) => <div style={{ margin: 24 }}>{children}</div>;
const Toolbar = (props: any) => (
  <Input.Toolbar {...props} style={{ backgroundColor: "lavender" }}>
    <Button.SaveButton color="primary" disabled={props.invalid} label="Update" />
  </Input.Toolbar>
);
export const PaymentProcessDashEdit = (props: any) => {
  return (
    <DashPanel resource={Enum.Resource.Payments} anchor={Enum.Anchor.Right} crud={Enum.Crud.Update} {...props}>
      <Dash />
    </DashPanel>
  );
};
const spacerStyle = { marginLeft: 14 };
const Spacer = props => <span style={spacerStyle} />;
