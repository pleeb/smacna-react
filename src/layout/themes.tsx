import { createMuiTheme } from "@material-ui/core";

export const darkTheme = {
  palette: {
    type: "dark" // Switching the dark mode on is a single property value change.
  }
};

export const lightTheme = {
  palette: {
    secondary: {
      light: "#5f5fc4",
      main: "#283593",
      dark: "#001064",
      contrastText: "#fff"
    }
  }
};
export const theme = createMuiTheme({
  palette: {
    secondary: {
      light: "#5f5fc4",
      main: "#283593",
      dark: "#001064",
      contrastText: "#fff"
    },
    error: {
      light: "#5f5fc4",
      main: "#283593",
      dark: "#001064",
      contrastText: "#fff"
    }
  },
  breakpoints: {
    values: { xs: 0, sm: 980, md: 1080, lg: 1440, xl: 1600 }
  }
});
