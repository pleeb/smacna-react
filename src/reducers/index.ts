import { session } from "./session";
import { report } from "./report";
import { dash } from "./dash";
import { theme } from "./theme";
import { print } from "./print";
import { focus } from "./focus";
import { contribution } from "./contribution";

export default { session, report, dash, theme, print, focus, contribution };
