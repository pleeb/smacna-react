import React, { Children, Fragment, cloneElement } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export const DialogPayment = ({ button }) => (
  <DialogWrapper title="Hi John, Liz," button={button}>
    This is a work in progress dialog box that will hold the relevant content following your recent action.
    <br />
    There will be a message notifying the user that a receipt has been sent to his email.
    <br />
    There will also be a button to print a receipt.
    <br />I will let you know when it is complete, among other improvements.
  </DialogWrapper>
);
export const DialogUpdating = ({}) => (
  <DialogWrapper title="Hi John, Liz," open>
    I assume you are here to check out the completed User Portal edits.
    <br />
    I am currently remigrating the data and also rebuilding the site code.
    <br />
    Thank you for your interest but please check back later!
  </DialogWrapper>
);
interface IProps {
  button?: React.ReactElement;
  open?: boolean;
  title: string;
  children?: any;
}
export class DialogWrapper extends React.Component<IProps, any> {
  state = {
    open: this.props.open || false
  };
  handleClickOpen = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    const { button, children, title } = this.props;
    return (
      <Fragment>
        {button && cloneElement(button, { onClick: this.handleClickOpen })}
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">{children}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Okay, thanks.
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}
