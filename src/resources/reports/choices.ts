import { Alias } from "../";
import * as companies from "../companies";
import * as contributions from "../contributions";

export const enumerate = [
  { id: "association_code", name: Alias["association_code"] },
  // { id: "croup", name: Alias["croup"] },
  { id: "work_code", name: Alias["work_code"] },
  { id: "calculation", name: Alias["calculation"] },
  { id: "status", name: Alias["status"] },
  // { id: "pay_method", name: Alias["pay_method"] },
  // { id: "pay_status", name: Alias["pay_status"] },
  // { id: "payment_type", name: Alias["payment_type"] },
  { id: "association", name: Alias["association"] },
  { id: "permanent", name: Alias["permanent"] },
  { id: "ifus", name: Alias["ifus"] },
  { id: "company_id", name: Alias["company_id"] }
];
export const select = [
  { id: "company_name", name: Alias["company_name"] },
  { id: "address", name: Alias["address"] },
  { id: "city", name: Alias["city"] },
  { id: "state", name: Alias["state"] },
  { id: "zipcode", name: Alias["zipcode"] },
  { id: "croup", name: Alias["croup"] },
  { id: "county", name: Alias["county"] },
  { id: "phone", name: Alias["phone"] },
  { id: "fax", name: Alias["fax"] },
  { id: "contact_name", name: Alias["contact_name"] },
  { id: "ifus", name: Alias["ifus"] },
  { id: "association", name: Alias["association"] },
  { id: "permanent", name: Alias["permanent"] },
  { id: "work_code", name: Alias["work_code"] },
  { id: "association_code", name: Alias["association_code"] },
  { id: "start_date", name: Alias["start_date"] },
  { id: "outbus_date", name: Alias["outbus_date"] },
  { id: "inactive_date", name: Alias["inactive_date"] },
  { id: "chap11_date", name: Alias["chap11_date"] }
];
export const attribute = [
  { id: "num_apprentice", name: Alias["num_apprentice"] },
  { id: "num_mechanic", name: Alias["num_mechanic"] },
  { id: "num_light_commercial", name: Alias["num_light_commercial"] },
  { id: "num_total", name: Alias["num_total"] },
  { id: "hours_apprentice", name: Alias["hours_apprentice"] },
  { id: "hours_mechanic", name: Alias["hours_mechanic"] },
  { id: "hours_light_commercial", name: Alias["hours_light_commercial"] },
  { id: "hours_total", name: Alias["hours_total"] },
  { id: "amount_apprentice", name: Alias["amount_apprentice"] },
  { id: "amount_mechanic", name: Alias["amount_mechanic"] },
  {
    id: "amount_light_commercial",
    name: Alias["amount_light_commercial"]
  },
  { id: "amount_total", name: Alias["amount_total"] },
  { id: "amount_paid", name: Alias["amount_paid"] },
  { id: "amount_owed", name: Alias["amount_owed"] }
];
export { status, calculation } from "../contributions/choices";
export { state } from "../companies/choices";
