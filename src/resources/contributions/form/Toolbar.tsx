import React from "react";
import { Certification } from "./Certification";
import { SubmitOptions, SaveButton } from "./SubmitOptions";
import { Enum, Input, isMaster } from "../../";

export const Toolbar = (props: any) => (
  <Input.Toolbar {...props} style={{ justifyContent: "space-around", margin: 0, backgroundColor: "lavender" }}>
    {isMaster(props.permissions) ? null : <Certification />}
    {isMaster(props.permissions) ? <SaveButton /> : <SubmitOptions />}
  </Input.Toolbar>
);
