import React from "react";
import { Field } from "redux-form";
import { connect } from "react-redux";
import {
  withStyles,
  TableFooter,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField
} from "@material-ui/core";
import { Card } from "@material-ui/core";
import compose from "recompose/compose";
import { Alias, GridContainer, GridItem } from "../";

const input = {
  inputProps: { style: { textAlign: "right" } },
  selectText: e => e.target.select(),
  renderNumberField: ({ classes, input, label, meta: { touched, error }, ...custom }: any) => (
    <TextField
      // test={console.log("input", input, "custom", custom)}
      // value={input.value}
      label={null}
      error={!!(touched && error)}
      helperText={touched && error}
      {...input}
      {...custom}
    />
  ),
  renderRenderField: ({ inputProps: { rate }, input: { name, value }, ...rest }) => (
    <span>
      {value} x ${rate} {console.log(name, value, rest)}
    </span>
  )
};
const NumInput = ({ name, defaultValue }: any) => (
  <Field
    defaultValue={defaultValue}
    component={input.renderNumberField}
    inputProps={input.inputProps}
    name={name}
    type="number"
    onFocus={input.selectText}
  />
);
const RenderField = ({ name }) => (
  <Field component={input.renderRenderField} inputProps={input.inputProps} name={name} type="text" />
);
const enhance = compose(
  withStyles({
    mdash: {
      marginRight: 10
    }
  }),
  connect(
    (state: any, props: any) => {
      let contribution;
      try {
        contribution = state.admin.resources.contributions.data[props.record.contribution_id];
        console.log("RECORD FORM", state.form["record-form"].values.hours_mechanic, state);
      } catch (err) {
        contribution = {};
      }
      return {
        contribution
      };
    },
    {}
  )
);
export const InputTable = enhance(({ classes, contribution, ...rest }) => {
  contribution = contribution || {};
  console.log(contribution, rest);
  return (
    <Card>
      <Table padding="checkbox">
        <TableHead>
          <TableRow>
            <TableCell>{null}</TableCell>
            <TableCell numeric>Building Trade Mechanics</TableCell>
            <TableCell numeric>Apprentice Workers</TableCell>
            <TableCell numeric>Light Commercial Workers</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>Number of Employees</TableCell>
            <TableCell numeric>
              <NumInput name="num_apprentice" />
            </TableCell>
            <TableCell numeric>
              <NumInput name="num_mechanic" />
            </TableCell>
            <TableCell numeric>
              <NumInput name="num_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>+ Hours Paid</TableCell>
            <TableCell numeric>
              <NumInput name="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <NumInput name="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <NumInput name="hours_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>&ndash; Target Hours</TableCell>
            <TableCell numeric>
              <NumInput name="hours_target" />
            </TableCell>
            <TableCell numeric>
              <span className={classes.mdash}>&mdash;</span>
            </TableCell>
            <TableCell numeric>
              <span className={classes.mdash}>&mdash;</span>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Contribution Hour</TableCell>
            <TableCell numeric>
              <RenderField name="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <RenderField name="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <RenderField name="hours_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Contribution Due</TableCell>
            <TableCell numeric>
              <NumInput name="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <NumInput name="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <NumInput name="hours_light_commercial" />
            </TableCell>
          </TableRow>
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell>cell content</TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </Card>
  );
});
