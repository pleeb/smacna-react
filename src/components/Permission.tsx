import React, { Fragment } from "react";
import { Role } from "../enums";

interface IPermissions {
  permissions: Role;
  children: any;
}
interface IPermitE {
  permitE?: Role;
  permitGTE?: never;
}
interface IPermitGTE {
  permitE?: never;
  permitGTE?: Role;
}
type Permissions = IPermissions & (IPermitE | IPermitGTE);

export const Permission = ({ permitE, permitGTE, permissions, children }: Permissions) => {
  if (permitE && permitE !== permissions) return null;
  if (permitGTE && permitGTE > permissions) return null;
  return <Fragment>{children}</Fragment>;
};
