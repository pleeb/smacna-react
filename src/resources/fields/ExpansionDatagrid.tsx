import React, { Component, isValidElement, Children, cloneElement } from "react";
import PropTypes from "prop-types";
import { sanitizeListRestProps } from "ra-core";
import { withStyles, createStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import Loading from "@material-ui/core/LinearProgress";

import classnames from "classnames";

import DatagridHeaderCell from "ra-ui-materialui/esm/list/DatagridHeaderCell";
import DatagridBody from "./ExpansionDatagridBody";
import DatagridLoading from "ra-ui-materialui/esm/list/DatagridLoading";
import ExpansionDatagridRow from "./ExpansionDatagridRow";

const styles = theme =>
  createStyles({
    table: {
      tableLayout: "auto"
    },
    thead: {},
    tbody: {
      height: "inherit"
    },
    headerRow: {},
    headerCell: {
      // maxWidth: 225,
      padding: "0 8px 0 0",
      "&:last-child": {
        padding: "0 8px 0 0"
      }
    },
    headerCell0: {
      // maxWidth: 225,
      textAlign: "right",
      padding: "0 8px 0 0"
    },
    checkbox: {},
    hover: {
      backgroundColor: "lavender!important",
      opacity: 0.3
    },
    row: {
      "&:hover": {
        backgroundColor: "lavender!important"
      },
      "&:nth-child(odd)": {
        backgroundColor: "oldlace!important"
      },
      "&:hover:nth-child(odd)": {
        backgroundColor: "lavender!important"
      }
    },
    rowX: {
      backgroundColor: "oldlace",
      "&:hover": {
        backgroundColor: "lavender!important"
      }
    },
    rowXX: {
      backgroundColor: "aliceblue",
      "&:hover": {
        backgroundColor: "lavender!important"
      },
      "&:first-child": {
        opacity: "1!important" as any
      },
      "&:nth-last-child(2)": {
        opacity: 0.8
      },
      "&:nth-last-child(1)": {
        opacity: 0.6
      }
    },
    clickableRow: {
      cursor: "pointer"
    },
    rowEven: {},
    rowOdd: {},
    rowCell: {
      // maxWidth: 225,
      padding: "0 8px 0 0",
      "&:last-child": {
        padding: "0 8px 0 0",
        textAlign: "center"
      }
    },
    rowCell0: {
      // maxWidth: 225,
      textAlign: "right",
      padding: "0 8px 0 0"
    },
    expandHeader: {
      padding: 0,
      width: 48
    },
    expandIconCell: {
      width: 48
    },
    expandIcon: {
      transform: "rotate(-90deg)",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    expanded: {
      transform: "rotate(0deg)"
    }
  });

/**
 * The Datagrid component renders a list of records as a table.
 * It is usually used as a child of the <List> and <ReferenceManyContainer> components.
 *
 * Props:
 *  - rowStyle
 *
 * @example Display all posts as a datagrid
 * const postRowStyle = (record, index) => ({
 *     backgroundColor: record.nb_views >= 500 ? '#efe' : 'white',
 * });
 * export const PostList = (props) => (
 *     <List {...props}>
 *         <Datagrid rowStyle={postRowStyle}>
 *             <TextField source="id" />
 *             <TextField source="title" />
 *             <TextField source="body" />
 *             <Input.EditButton />
 *         </Datagrid>
 *     </List>
 * );
 *
 * @example Display all the comments of the current post as a datagrid
 * <ReferenceManyContainer reference="comments" target="post_id">
 *     <Datagrid>
 *         <TextField source="id" />
 *         <TextField source="body" />
 *         <DateField source="created_at" />
 *         <Input.EditButton />
 *     </Datagrid>
 * </ReferenceManyContainer>
 */
class Datagrid extends Component<any, any> {
  updateSort = event => {
    event.stopPropagation();
    this.props.setSort(event.currentTarget.dataset.sort);
  };

  handleSelectAll = event => {
    const { onSelect, ids, selectedIds } = this.props;
    if (event.target.checked) {
      onSelect(
        ids.reduce(
          (idList, id) => (idList.includes(id) ? idList : idList.concat(id)),

          selectedIds
        )
      );
    } else {
      onSelect([]);
    }
  };

  render() {
    const {
      basePath,
      body,
      children,
      classes,
      className,
      currentSort,
      data,
      expand,
      hasBulkActions,
      hover,
      ids,
      isLoading,
      loadedOnce,
      onSelect,
      onToggleItem,
      resource,
      rowClick,
      rowStyle,
      selectedIds,
      setSort,
      total,
      version,
      ...rest
    } = this.props;

    /**
     * if loadedOnce is false, the list displays for the first time, and the dataProvider hasn't answered yet
     * if loadedOnce is true, the data for the list has at least been returned once by the dataProvider
     * if loadedOnce is undefined, the Datagrid parent doesn't track loading state (e.g. ReferenceArrayField)
     */
    if (expand === true && loadedOnce === false) {
      return <Loading variant="query" />;
    }

    /**
     * Once loaded, the data for the list may be empty. Instead of
     * displaying the table header with zero data rows,
     * the datagrid displays nothing in this case.
     */
    if (!isLoading && (ids.length === 0 || total === 0)) {
      return null;
    }

    /**
     * After the initial load, if the data for the list isn't empty,
     * and even if the data is refreshing (e.g. after a filter change),
     * the datagrid displays the current data.
     */
    // if (expand && hasBulkActions) throw "INTERNAL: Cannot expand and have bulk actions.";
    return (
      <Table className={classnames(classes.table, className)} {...sanitizeListRestProps(rest)}>
        {expand === true ? null : (
          <TableHead className={classes.thead}>
            <TableRow className={classnames(classes.headerRow)}>
              {hasBulkActions ? (
                <TableCell padding="none">
                  <Checkbox
                    className="select-all"
                    color="primary"
                    checked={selectedIds.length > 0 && ids.length > 0 && !ids.find(it => selectedIds.indexOf(it) === -1)}
                    onChange={this.handleSelectAll}
                  />
                </TableCell>
              ) : (
                <TableCell className={classes.expandHeader} />
              )}
              {Children.map(children, (field: any, index) =>
                (isValidElement(field) as any) ? (
                  <DatagridHeaderCell
                    className={expand && index === 0 ? classes.headerCell0 : classes.headerCell}
                    currentSort={currentSort}
                    field={field}
                    isSorting={currentSort.field === (field.props.sortBy || field.props.source)}
                    key={field.props.source || index}
                    resource={resource}
                    updateSort={this.updateSort}
                  />
                ) : null
              )}
            </TableRow>
          </TableHead>
        )}
        {cloneElement(
          body,
          {
            basePath,
            className: classes.tbody,
            classes,
            expand,
            rowClick,
            data,
            hasBulkActions,
            hover,
            ids,
            isLoading,
            onToggleItem,
            resource,
            rowStyle,
            selectedIds,
            version
          },
          children
        )}
      </Table>
    );
  }
}

(Datagrid as any).propTypes = {
  basePath: PropTypes.string,
  body: PropTypes.element.isRequired,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object,
  className: PropTypes.string,
  currentSort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string
  }),
  data: PropTypes.object.isRequired,
  expand: PropTypes.oneOfType([PropTypes.node, PropTypes.bool]),
  hasBulkActions: PropTypes.bool.isRequired,
  hover: PropTypes.bool,
  ids: PropTypes.arrayOf(PropTypes.any).isRequired,
  isLoading: PropTypes.bool,
  onSelect: PropTypes.func,
  onToggleItem: PropTypes.func,
  resource: PropTypes.string,
  rowClick: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  rowStyle: PropTypes.func,
  selectedIds: PropTypes.arrayOf(PropTypes.any).isRequired,
  setSort: PropTypes.func,
  total: PropTypes.number,
  version: PropTypes.number
};

(Datagrid as any).defaultProps = {
  data: {},
  hasBulkActions: false,
  ids: [],
  selectedIds: [],
  body: <DatagridBody row={<ExpansionDatagridRow />} />
};

export default withStyles(styles)(Datagrid);
