import React from "react";
import { ShowGuesser, EditGuesser } from "react-admin";

// export const RatesShow = ShowGuesser;
export { RatesShow } from "./Show";

export { ReadonlyRateTable } from "./ReadonlyRateTable";
// export { InputRateTable } from "./InputRateTable";
export { RateDashEdit } from "./DashEdit";
