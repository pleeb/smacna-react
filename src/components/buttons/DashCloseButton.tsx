import React from "react";
import Icon from "@material-ui/icons/Close";
import { connect } from "react-redux";
import { DashButton, Enum } from "./";
import { dashAction, DashAction, closeAllDash } from "../../actions/dash";
import { Button } from "@material-ui/core";
import { destroy } from "redux-form";

export const DashCloseButton = connect(
  null,
  dispatch => ({
    closeDash: e => {
      e.preventDefault();
      dispatch(closeAllDash());
      dispatch(destroy("record-form"));
    }
  })
)(({ closeDash, iconOnly }: any) => (
  <Button
    color="secondary"
    onClick={closeDash}
    variant="fab"
    style={iconOnly ? { width: 36, height: 36, borderRadius: 5 } : { borderRadius: 5, padding: "0 14px", width: "auto", height: "auto" }}
  >
    <Icon />
    {iconOnly ? null : <span style={{ marginLeft: 14 }}>Close</span>}
  </Button>
)) as any;
