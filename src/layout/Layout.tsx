import React from "react";
import { connect } from "react-redux";
import { Layout, Sidebar } from "react-admin";
import AppBar from "./AppBar";
// import { darkTheme, lightTheme, theme } from "./themes";

const CustomSidebar = props => <Sidebar size={185} {...props} />;
const CustomLayout = props => <Layout appBar={AppBar} sidebar={CustomSidebar} {...props} />;

export default connect(
  ({ theme }: any) => ({
    theme
  }),
  {}
)(CustomLayout);
