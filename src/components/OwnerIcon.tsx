import React from "react";
import compose from "recompose/compose";
import { connect } from "react-redux";
import { withStyles, Chip } from "@material-ui/core";
import { Icon, Role } from "../enums";
import Face from "@material-ui/icons/Face";
import SubDir from "@material-ui/icons/SubdirectoryArrowRight";

const kindofUser = ({ username }) => Boolean(username);
const kindofPayment = ({ cid, pay_method }) => Boolean(cid && pay_method);
const kindofCompany = ({ company_name, company_id }) => Boolean(company_name && !company_id);
interface IIds {
  user_id: number;
  company_id: number;
}
const parseIds = (record): IIds => {
  let user_id, company_id;
  if (kindofUser(record)) user_id = record.id;
  else if (kindofPayment(record)) user_id = record.user_id;
  else if (kindofCompany(record)) company_id = record.id;
  return { user_id, company_id };
};
const mapState = ({ session: { id: owner_user_id, company_id: owner_company_id } }, { record }) => {
  if (!record) return { isOwner: false };
  const { user_id, company_id } = parseIds(record);
  return {
    isOwner: user_id ? user_id === owner_user_id : company_id ? company_id === owner_company_id : false
  };
};
const styles = {
  chip: {
    backgroundColor: "rgb(26,170,85)",
    color: "white",
    fontWeight: "bold",
    border: "1px solid lightslategray",
    borderRadius: "5px!important",
    fontSize: "1.1rem",
    height: 28
  },
  item: {
    backgroundColor: "rgb(26,170,85)",
    color: "white",
    borderRadius: "50%"
  }
} as any;
const enhance = compose(
  withStyles(styles),
  connect(mapState)
);
interface IProps {
  record?: any;
  type: "item" | "subitem" | "chip";
}
const Owner = ({ classes, isOwner, type }) => {
  switch (type) {
    case "item":
      if (isOwner) return <Face className={classes.item} />;
      return null;
    case "subitem":
      if (isOwner) return <Face className={classes.item} />;
      return <SubDir />;
    default:
      if (!isOwner) return <span>&mdash;</span>;
      return <Chip label="YOU" className={classes.chip} />;
  }
};
export const OwnerIcon = enhance(Owner) as React.StatelessComponent<IProps>;
const Profile = ({ role, ...props }) => {
  switch (role) {
    case Role.User:
      return <Icon.User {...props} />;
    case Role.Admin:
      return <Icon.Admin {...props} />;
    case Role.Master:
      return <Icon.Master {...props} />;
    default:
      return null;
  }
};
export const ProfileIcon = Profile;
