import { Alias, Enum } from "./";

type TDateOption<T extends CompanyDateField | ContributionDateField> = {
  id: T;
  name: string;
};
export type CompanyDateField = "start_date" | "outbus_date" | "inactive_date" | "chap11_date" | "created_at" | "updated_at";

export type ContributionDateField = "reporting_week_end" | "receive_date" | "deposit_date" | "created_at" | "updated_at";

export const dateInputs: {
  companies: TDateOption<CompanyDateField>[];
  contributions: TDateOption<ContributionDateField>[];
} = {
  companies: [
    { id: "start_date", name: Alias["start_date"] },
    { id: "outbus_date", name: Alias["outbus_date"] },
    { id: "inactive_date", name: Alias["inactive_date"] },
    { id: "chap11_date", name: Alias["chap11_date"] },
    { id: "created_at", name: Alias["created_at"] },
    { id: "updated_at", name: Alias["updated_at"] }
  ],
  contributions: [
    { id: "reporting_week_end", name: Alias["reporting_week_end"] },
    { id: "receive_date", name: Alias["receive_date"] },
    { id: "deposit_date", name: Alias["deposit_date"] },
    { id: "created_at", name: Alias["created_at"] },
    { id: "updated_at", name: Alias["updated_at"] }
  ]
};

type SelectionOperators = { id: Enum.SelectionOperator; name: string }[];
export const selectionOperators = [
  { id: Enum.SelectionOperator.NOOP, name: Alias[Enum.SelectionOperator.NOOP] },
  { id: Enum.SelectionOperator.IN, name: Alias[Enum.SelectionOperator.IN] },
  { id: Enum.SelectionOperator.AND, name: Alias[Enum.SelectionOperator.AND] },
  { id: Enum.SelectionOperator.OR, name: Alias[Enum.SelectionOperator.OR] },
  { id: Enum.SelectionOperator.ANY, name: Alias[Enum.SelectionOperator.ANY] }
];
type NumberOperators = { id: Enum.NumberOperator; name: string }[];
export const numberOperators = [
  { id: Enum.NumberOperator.NOOP, name: Alias[Enum.NumberOperator.NOOP] },
  { id: Enum.NumberOperator.GREATER_THAN, name: Alias[Enum.NumberOperator.GREATER_THAN] },
  { id: Enum.NumberOperator.GREATER_THAN_EQUAL, name: Alias[Enum.NumberOperator.GREATER_THAN_EQUAL] },
  { id: Enum.NumberOperator.LESS_THAN, name: Alias[Enum.NumberOperator.LESS_THAN] },
  { id: Enum.NumberOperator.LESS_THAN_EQUAL, name: Alias[Enum.NumberOperator.LESS_THAN_EQUAL] },
  { id: Enum.NumberOperator.NOT_EQUAL, name: Alias[Enum.NumberOperator.NOT_EQUAL] },
  { id: Enum.NumberOperator.EQUAL, name: Alias[Enum.NumberOperator.EQUAL] },
  { id: Enum.NumberOperator.BETWEEN, name: Alias[Enum.NumberOperator.BETWEEN] },
  { id: Enum.NumberOperator.NOT_BETWEEN, name: Alias[Enum.NumberOperator.NOT_BETWEEN] }
];
type StringOperators = { id: Enum.StringOperator; name: string }[];
export const stringOperators: StringOperators = [
  { id: Enum.StringOperator.NOOP, name: Alias[Enum.StringOperator.NOOP] },
  { id: Enum.StringOperator.STARTS_WITH, name: Alias[Enum.StringOperator.STARTS_WITH] },
  { id: Enum.StringOperator.CONTAINS, name: Alias[Enum.StringOperator.CONTAINS] },
  { id: Enum.StringOperator.ENDS_WITH, name: Alias[Enum.StringOperator.ENDS_WITH] }
];

export type OperatorChoices = { id: Enum.StringOperator; name: string }[];
