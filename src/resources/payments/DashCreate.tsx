import React from "react";
import Print from "@material-ui/icons/Print";
import BButton from "@material-ui/core/Button";
import Electronic from "@material-ui/icons/AccountBalance";
import Mail from "@material-ui/icons/Mail";
import { TextField, withStyles, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import { closeDash, openDash } from "../../actions";
import * as choices from "./choices";
import { refreshView } from "react-admin";
import { ReadonlyTable } from "../contributions/table";
import { UserFields } from "../users/UserFields";
import { PaymentFields } from "../contributions/form/PaymentFields";
import {
  Field,
  Enum,
  DashPanel,
  Input,
  Button,
  Alias,
  GridContainer,
  GridItem,
  sourceFieldFromMap,
  sourceField,
  SimpleShowContainer,
  SimpleFormContainer,
  Actionbar,
  DialogPayment,
  DialogWrapper,
  dataProvider
} from "../";

const Hidden = props => (
  <div style={{ display: "none" }}>
    <Input.NumberInput source="company_id" defaultValue={props.company_id} />
    <Input.NumberInput source="contribution_id" defaultValue={props.contribution_id} />
    <Input.NumberInput source="user_id" defaultValue={props.user_id} />
    <Input.TextInput source="pay_method" defaultValue={props.pay_method || "MAIL"} />
  </div>
);
// <Input.NumberInput {...props} source="amount_paid" defaultValue={props.amount_paid} />

const Actions = ({ permissions, ...props }) => <Actionbar verb="Pay Delinquency" title={(props.data || {})["cid"]} {...props} />;
const Dash = ({ permissions, anchor, record, ...props }: any) => {
  return (
    <Input.Create
      {...props}
      resource="payments"
      basePath="/payments"
      title={` Pay Delinquency`}
      actions={<Actions permissions={permissions} dash anchor={anchor} data={record} />}
    >
      <WrapForm permissions={permissions} value={record} />
    </Input.Create>
  );
};
const WrapForm = connect(
  ({ session: { id: user_id, company_id, ...User } }: any) => ({ user_id, company_id, User }),
  dispatch => ({
    redirect: (basePath, id, data) => {
      dispatch(refreshView());
      dispatch(closeDash(Enum.Resource.Contributions, Enum.Crud.Update));
      dispatch(openDash(Enum.Resource.Payments, Enum.Crud.None, data));
      return `/contributions/${data.contribution_id}/show`;
    }
  })
)(({ value, user_id, company_id, User, redirect, ...props }: any) => {
  const owed = Math.abs(value.amount_owed || 0);
  const record = { ...value, user_id, company_id, contribution_id: value.id, User };
  return (
    <React.Fragment>
      <SimpleShowContainer record={record} basePath="/contributions" resource="contributions" withPad>
        {sourceField("User.full_name")}
        {sourceField("User.title")}
        {sourceField("User.email")}
        {sourceField("User.phone")}
        <HR />
        {sourceField("reporting_week")}
        {sourceField("reporting_week_count")}
        {sourceField("reporting_week_start")}
        {sourceField("reporting_week_end")}
        <HR />
        {sourceField("amount_total")}
        {sourceField("amount_paid")}
        {sourceField("amount_owed")}
      </SimpleShowContainer>
      <SimpleFormContainer {...props} redirect={redirect} toolbar={<Toolbar />} withPad>
        <GridContainer>
          <GridItem xs>
            <Field.PayField value={owed} label="Pay Amount" />
            <Hidden {...record} />
          </GridItem>
        </GridContainer>
      </SimpleFormContainer>
    </React.Fragment>
  );
}) as any;
const HR = props => <hr />;
const PayRemainingField = ({ value }) => (
  <div style={{ margin: "0 24px" }}>
    <Field.PayField value={value} label="Pay Amount" />
  </div>
);
// <PayRemainingInput value={owed} />
// const PayRemainingInput = ({ value }) => (
//   <div style={{ visibility: "hidden" }}>
//     <Input.NumberInput disabled fullWidth source="amount_paid" label="Pay Amount" defaultValue={value} />
//   </div>
// );
const styles = withStyles(theme => ({
  col: {
    display: "flex",
    flexDirection: "column",
    width: "100%"
  },
  row: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  label: { marginRight: "1em", fontWeight: "bold", marginBottom: "1em" }
}));
const PaymentOptions = styles(({ classes, invalid, ...props }: any) => (
  <div className={classes.col}>
    <Typography color="primary" variant="subheading" className={classes.label}>
      Submit payment by:
    </Typography>
    <div className={classes.row}>
      <Button.SaveButton {...props} disabled={invalid} icon={<Mail />} label="Mail" />
      <Spacer />
      <DialogPayment
        button={
          <BButton color="primary" disabled={invalid}>
            <Electronic /> <span style={{ marginLeft: 10 }}>Electronic</span>
          </BButton>
        }
      />
    </div>
  </div>
));
const Toolbar = (props: any) => (
  <Input.Toolbar {...props} style={{ margin: 0, paddingTop: 14, paddingBottom: 14, backgroundColor: "lavender" }}>
    <PaymentOptions />
  </Input.Toolbar>
);
export const PaymentOutstandingDashCreate = (props: any) => {
  return (
    <DashPanel resource={Enum.Resource.Payments} anchor={Enum.Anchor.Right} crud={Enum.Crud.Create} {...props}>
      <Dash />
    </DashPanel>
  );
};
const spacerStyle = { marginLeft: 14, marginRight: 14 };
const Spacer = props => <span style={spacerStyle} />;
