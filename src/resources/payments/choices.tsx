import React from "react";
import { createChoice, FieldType, Enum } from "../";

export const pay_status = [
  createChoice("_", "All"),
  createChoice(Enum.Status.FAILED),
  createChoice(Enum.Status.SHORT),
  createChoice(Enum.Status.PENDING),
  createChoice(Enum.Status.OVERPAY),
  createChoice(Enum.Status.SUCCESS)
];
export const pay_method = [createChoice(Enum.PayMethod.ELECTRONIC), createChoice(Enum.PayMethod.MAIL)];
export const attribute = [
  createChoice("id"),
  createChoice("username"),
  createChoice("company_id"),
  createChoice("contribution_id"),
  createChoice("amount_total"),
  createChoice("amount_paid"),
  createChoice("pay_status"),
  createChoice("pay_method"),
  createChoice("comment"),
  createChoice("created_at"),
  createChoice("updated_at")
];
