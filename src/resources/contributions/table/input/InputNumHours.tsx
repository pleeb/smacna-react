import { component, getAmountSource, getRateSource, React, connect, IState, HoursSource, RateSource, TSourceRecord } from "../";
import { compose, change } from "./";
// import { NumberInput } from "react-admin";
import { Field } from "redux-form";
import { mui } from "../../../";
import { TextField } from "@material-ui/core";
import { validationMap } from "./validation";

const { inputStyles, createOptions } = component;

const mapPropsFromState = ({
  form: {
    "record-form": { values = {}, syncErrors = {} }
  }
}: any) => {
  return {
    values,
    syncErrors
  };
};
const selectValue = e => e.target.select();
const toNum = (value, name?) => (isNaN(value) ? 0 : +value);
// const Field = _Field as any;
const NumberField = ({ name, input, label, type, meta: { touched, error, warning, dispatch }, ...props }) => {
  return (
    <TextField
      type="number"
      fullWidth
      {...input}
      InputLabelProps={{ shrink: true }}
      inputProps={{
        min: 0,
        style: { textAlign: "right", width: "100%" },
        onFocus: selectValue
      }}
      InputProps={{ margin: "none" as any, disableUnderline: true, style: { marginTop: 0 }, classes: props.classes }}
      FormHelperTextProps={{ style: { display: "flex", flexDirection: "column" } }}
      label={label}
      error={Boolean(error)}
      helperText={error}
    />
  );
};
const ConnectedNumberInput = connect(
  null,
  { change }
)(({ source, classes, change, ...props }: any) => {
  return (
    <Field<any>
      component={NumberField}
      name={source}
      label=""
      type="number"
      validate={validationMap[source]}
      normalize={(value, previousValue, allValues, previousAllValues) => {
        change("record-form", getAmountSource(source), calcAmountTotal(allValues, source));
        change("record-form", "amount_total", calcAmountTotal(allValues, "hours_light_commercial", "hours_mechanic", "hours_apprentice"));
        return toNum(value);
      }}
      props={{ classes }}
    />
  );
}) as any;

interface IProps {
  classes?: any;
  source: HoursSource;
}
export const InputNumHours = inputStyles(
  ({ source, classes }: any): any => <ConnectedNumberInput classes={classes} source={source} />
) as React.StatelessComponent<IProps>;

const calcAmountTotal = (values, ...sources: HoursSource[]) => {
  let sum = 0;
  sources.map(source => {
    sum += calcAmount(values, source);
  });
  return sum;
};
const calcAmount = (values, source: HoursSource): number => {
  let hours = 0,
    hoursTarget = 0;
  switch (source) {
    case "hours_target":
      hours = +values["hours_mechanic"];
      hoursTarget = +values["hours_target"];
      break;
    case "hours_mechanic":
      hours = +values["hours_mechanic"];
      hoursTarget = +values["hours_target"];
      break;
    default:
      hours = +values[source];
      break;
  }
  const totalHours = hours - hoursTarget;
  const rate = +values[getRateSource(source)];
  const amount = totalHours * rate;
  return amount;
};
