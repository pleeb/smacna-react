import React, { Fragment } from "react";
import { connect } from "react-redux";
import Drawer from "@material-ui/core/Drawer";
import { CardActions, withStyles } from "@material-ui/core";
import * as choices from "./choices";
import {
  ISort,
  Pagination,
  Button,
  Enum,
  Input,
  Field,
  sourceFieldFromMap,
  Alias,
  DatagridMapper,
  DatagridMapperX,
  ReferenceManyContainer
} from "../";
import { ReferenceManyPaymentX } from "../payments";

const Actions = ({
  permissions,
  bulkActions,
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
  total
}: any) => (
  <CardActions>
    {filters &&
      React.cloneElement(filters, {
        resource,
        showFilter,
        displayedFilters,
        filterValues,
        context: "button"
      })}
    <Button.ExportButton disabled={total === 0} resource={resource} sort={currentSort} filter={filterValues} exporter={exporter} />
    <Button.RefreshButton />
  </CardActions>
);
const Filters = ({ permissions, ...props }) => (
  <Field.Filter {...props}>
    <Input.DateRangeInput alwaysOn source="reporting_week_end" defaultOperator={Enum.NumberOperator.BETWEEN} />
    <Input.SelectInput alwaysOn source="status" label={Alias["status"]} choices={choices.status} allowEmpty={false} />
    {permissions >= Enum.Role.Admin ? <Input.StringSearchInput alwaysOn source="cid" /> : null}
  </Field.Filter>
);
const rowStyle = record => ({
  backgroundImage:
    record && isShortOrDelinquent(record.status)
      ? "linear-gradient(to right, tomato, transparent 10%)"
      : "linear-gradient(to right, LimeGreen, transparent 10%",
  padding: 0
  // borderLeft: record && record.status === Enum.Status.DELINQUENT ? "5px solid tomato" : "5px solid LimeGreen"
});

const defaultSort: ISort = { field: "reporting_week_end", order: "DESC" };
const isShortOrDelinquent = status => status === Enum.Status.DELINQUENT || status === Enum.Status.SHORT;
export const ReferenceManyContribution = ({ permissions, ...props }) => {
  return (
    <ReferenceManyContainer sort={defaultSort} reference="contributions" target="company_id" source="id" {...props}>
      <DatagridMapperX
        fixed
        expand={<ReferenceManyPaymentX permissions={permissions} />}
        rowStyle={rowStyle}
        permissions={permissions}
        context={Enum.Context.Show}
      />
    </ReferenceManyContainer>
  );
};
export const ContributionList = connect(
  ({ session: { role, company_id } }: any) => ({ role, company_id }),
  {}
)(({ push, role, company_id, permissions, ...props }: any) => {
  return (
    <Field.List
      {...props}
      pagination={<Pagination />}
      sort={defaultSort}
      filterDefaultValues={{ status: "_", pay_status: { $ne: Enum.Status.PENDING } }}
      filter={
        role === Enum.Role.User ? { company_id, pay_status: { $ne: Enum.Status.PENDING } } : { pay_status: { $ne: Enum.Status.PENDING } }
      }
      filters={<Filters permissions={permissions} />}
      actions={<Actions permissions={permissions} />}
      bulkActionButtons={false}
    >
      <DatagridMapperX
        expand={<ReferenceManyPaymentX permissions={permissions} />}
        rowStyle={rowStyle}
        permissions={permissions}
        context={Enum.Context.List}
      />
    </Field.List>
  );
});
