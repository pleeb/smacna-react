import React, { Children, cloneElement } from "react";
import { Grid, withStyles } from "@material-ui/core";
import { Role } from "../enums";

interface IProps {
  children: any;
  className?: any;
}
interface IItem extends IProps {
  xs?: true | number;
  sm?: true | number;
  md?: true | number;
}
interface IContainer extends IProps {
  withMargin?: boolean;
  direction?: "row" | "column" | "row-reverse" | "column-reverse";
  record?: any;
}
const enhance = withStyles({
  container: {
    width: "auto",
    flexWrap: "nowrap",
    padding: 0,
    margin: 0
  },
  item: {
    padding: 0,
    margin: 0
  },
  zeroMinWidth: {
    width: "100%",
    minWidth: "inherit",
    margin: 0,
    padding: "16px 24px"
  }
});
export const GridContainer = enhance(({ permissions, children, record, basePath, resource, withMargin, ...rest }: any) => (
  <Grid container justify="flex-start" spacing={16} zeroMinWidth={withMargin} {...rest}>
    {Children.map(children, child => child && cloneElement(child, { record, basePath, resource, permissions }))}
  </Grid>
)) as React.StatelessComponent<IContainer>;
export const GridItem = enhance(({ children, record, total, basePath, resource, permissions, ...rest }: any) => (
  <Grid item {...rest} style={{ display: "flex", flexDirection: "column" }}>
    {Children.map(children, (child, i) =>
      cloneElement(child, {
        record,
        basePath,
        resource,
        permissions
      })
    )}
  </Grid>
)) as React.StatelessComponent<IItem>;
