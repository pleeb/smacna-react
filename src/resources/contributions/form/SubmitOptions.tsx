import React from "react";
import BButton from "@material-ui/core/Button";
import Electronic from "@material-ui/icons/AccountBalance";
import Mail from "@material-ui/icons/Mail";
import { withStyles, FormLabel, Typography } from "@material-ui/core";
import { Grid, List, ListItem, ListItemText } from "@material-ui/core";
import { connect } from "react-redux";
import { Toolbar } from "react-admin";
import { isMaster, Enum, Input, Field, DialogWrapper, Button } from "../../";

const styles = withStyles({
  root: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  label: { fontWeight: "bold" }
}) as any;
export const SubmitOptions = styles(({ classes, invalid, ...props }: any) => (
  <div className={classes.root}>
    <Typography color={invalid ? "textSecondary" : "primary"} variant="subheading" className={classes.label}>
      Submit payment by:
    </Typography>
    <Spacer />
    <Button.SaveButton {...props} disabled={invalid} icon={<Mail />} label="Mail" />
    <Spacer />
    <DialogWrapper
      title="Congratulations! We are getting closer!"
      button={
        <BButton color="primary" disabled={invalid}>
          <Electronic /> <span style={{ marginLeft: 10 }}>Electronic</span>
        </BButton>
      }
    >
      This Electronic Payment function will send the payment, user and company details over the configured Transactis SSO endpoint.
      <br />
      <br />
      You (user) will be redirected to an external browser window or tab to Transactis/Signature Bank's secure electronic payment page.
      <br />
      The external payment page will be prefilled with the submitted payment data.
      <br />
      <br />
      Upon the external user electronic payment submission, Transactis will return the results to our server and we update our internal
      records automatically.
      <br />
      <br />
      The Portal will display the ( successful / failed ) Contribution record / payment with a ( "Success" / "Error" ) message.
      <br />
      We will also give the ability to print a payment receipt along with the status message.
    </DialogWrapper>
  </div>
));
const Spacer = props => <span style={{ marginLeft: 7, marginRight: 7 }} />;
export const SaveButton = ({ invalid, ...props }: any) => <Button.SaveButton {...props} disabled={invalid} />;
