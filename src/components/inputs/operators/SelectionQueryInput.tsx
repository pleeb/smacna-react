import React, { Fragment, Component } from "react";
import { change } from "redux-form";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { SearchInput } from "react-admin";
import {
  Button,
  withStyles,
  Select,
  FormControl,
  InputLabel,
  FormHelperText,
  MenuItem,
  Input,
  TextField
} from "@material-ui/core";
import { Enum, OperatorSelector, stringOperators as operators, Alias, required } from "./";

interface ISearchLike {
  alwaysOn?: boolean;
  placeholder: string;
  source: string;
  strategy?: "starts" | "ends" | "contains";
}
const parseStrategy = {
  [Enum.StringOperator.STARTS_WITH]: (v?: string) => (v ? `${v}%` : ""),
  [Enum.StringOperator.ENDS_WITH]: (v?: string) => (v ? `%${v}` : ""),
  [Enum.StringOperator.CONTAINS]: (v?: string) => (v ? `%${v}%` : "")
};
const defaultFormat = (v?: string) => (v ? v.replace(/%/g, "") : "");

type SourceField = {
  [props: string]: Object | string;
};
type TProps<Source extends "companies" | "contributions", Field = any> = {
  source: Source;
  field?: Field;
  dispatch: any;
  defaultOperator?: Enum.StringOperator;
  classes?: any;
  fieldMap?: any;
  type?: "filterForm" | "record-form";
  meta?: any;
};
class OperatorInput extends Component<any, any> {
  state = {
    operator: this.props.defaultOperator || Enum.StringOperator.STARTS_WITH
  };
  private join(...paths: string[]): string {
    return paths.filter(e => e).join(".");
  }
  private onChange = event => {
    const operator = event.target.value;
    if (operator === Enum.StringOperator.NOOP) {
      this.props.dispatch(change(this.props.meta.form, this.props.source + ".$like", ""));
    } else {
      this.props.dispatch(change(this.props.meta.form, this.props.source + ".$like", ""));
    }
    this.setState({ operator, operators });
  };
  render() {
    const { source, alwaysOn, classes } = this.props;
    const operator = this.state.operator;
    const disabled = operator === Enum.StringOperator.NOOP;
    return (
      <div className={classes.container}>
        <OperatorSelector readonly={false} value={operator} onChange={this.onChange} />
        <SearchInput
          source={`${source}.$like`}
          format={defaultFormat}
          parse={parseStrategy[operator]}
          placeholder={Alias[source]}
        />
      </div>
    );
  }
}
const styles = {
  container: { display: "flex", width: "100%", minWidth: 250, maxWidth: 500 },
  input: { width: "100%", marginTop: 16, marginBottom: 8, minWidth: 125 }
};
const enhance = compose(
  connect(
    null,
    dispatch => ({ dispatch })
  ),
  withStyles(styles)
);
export const SelectionQueryInput = enhance(OperatorInput);
