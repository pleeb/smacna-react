import React from "react";
import compose from "recompose/compose";
import { connect } from "react-redux";
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Button,
  withStyles,
  LinearProgress,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from "@material-ui/core";
import { IRequest, IReport } from "./";
import { NetStats } from "./NetStats";
import { DynamicTable } from "./DynamicTable";

const enhance = compose(
  withStyles({
    root: {
      minWidth: 600,
      width: "100%",
      display: "flex",
      margin: 0,
      padding: 0,
      marginTop: 14
    },
    content: {
      display: "flex",
      width: "100%",
      flexDirection: "column"
    },
    info: {
      display: "flex",
      width: "100%",
      flexDirection: "row"
    },
    xspacer: {
      width: 14,
      height: "100%",
      display: "flex"
    },
    yspacer: {
      height: 14,
      width: "100%",
      display: "flex"
    }
  }),
  connect((state: any) => ({
    record: state.report
  }))
);
export const ReportView = enhance((props: { record: IReport; classes: any }) => {
  const {
    classes,
    record: { fetching, error, datas, query, request, time }
  } = props;
  return (
    <Card className={classes.root}>
      <CardContent className={classes.content}>
        <div className={classes.info}>
          <NetStats {...{ ...time, fetching }} />
          <div className={classes.xspacer} />
        </div>
        <div className={classes.yspacer} />
      </CardContent>
    </Card>
  );
});
