import React from "react";

import {
  // buttons
  CloneButton,
  CreateButton,
  DeleteButton,
  EditButton,
  FilterButton,
  ListButton,
  RefreshButton,
  SaveButton,
  ShowButton,
  ExportButton,
  // view guessers
  ListGuesser,
  ShowGuesser,
  EditGuesser,
  // view components
  TabbedShowLayout,
  Tab,
  TabbedForm,
  FormTab,
  Datagrid,
  SimpleForm,
  Filter,
  // view containers
  Create,
  Toolbar,
  Show,
  Edit,
  List,
  // view actions
  CreateActions,
  EditActions,
  Actions
} from "react-admin";
import { withStyles } from "@material-ui/core";
import RunIcon from "@material-ui/icons/Send";
import { Link } from "react-router-dom";
import { withDataProvider } from "react-admin";
import * as choices from "./choices";
import * as company from "../companies/choices";
import * as contribution from "../contributions/choices";
import { Input, Button, Enum } from "../";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { RunButton } from "./RunButton";
import { DynamicTable, NetStats } from "./report";
import { TabbedInputForm } from "./inputs";

const CreateToolbar = props => {
  return (
    <Input.Toolbar {...props}>
      <Button.SaveButton label="Create" redirect="edit" />
      <RunButton />
    </Input.Toolbar>
  );
};

export const ReportCreate = withStyles({
  root: {
    display: "flex",
    flexDirection: "column"
  }
})(({ classes, ...props }: any) => {
  return (
    <div className={classes.root}>
      <Input.Create title="Reports (TEST)" resource={Enum.Resource.Reports} {...props} actions={null} aside={<NetStats />} >
        <TabbedInputForm toolbar={<CreateToolbar />} />
      </Input.Create>
      <DynamicTable />
    </div>
  );
});
