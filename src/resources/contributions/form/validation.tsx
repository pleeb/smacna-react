import { Validator } from "redux-form";
import { isMaster } from "../../";

export const validate = (record, ...props) => {
  if (isMaster(props[0].permissions)) return;
  const errors = { certified: [], amount_owed: [], cid: [] } as any;
  if (!record.certified) errors.certified.push("Must be certified to be true.");
  const owed = record.amount_paid - record.amount_total;
  if (owed > 0) errors.amount_owed.push("You may not perform a negative adjustment. Please call.");
  if (!record.cid) errors.cid.push("Reporting week end and number of weeks required.");
  for (let p in errors) errors[p].length === 0 && delete errors[p];
  return errors;
};
export const amountPaid: Validator = (value, values, props, name) => {
  if (!isMaster(props.permissions)) {
    if (+value < 0) {
      return "Negative adjustment. Please call.";
    }
  }
};
