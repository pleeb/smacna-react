import React, { Fragment } from "react";
import "react-day-picker/lib/style.css";
import Helmet from "react-helmet";
import { Enum } from "./";

export const CalendarStyles = props => (
  <Helmet>
    <style>{`
/*@media (max-width: 1080px) {
  .payment-calendar .DayPicker-Month:nth-child(1) {
    display: none;
  }  
}*/
.payment-calendar {
  display: block;
}
.payment-calendar .DayPicker-wrapper {
  padding: 14px 0;
  background-color: whitesmoke;
  font-size: 0.9rem;
  border-top: 1px solid lightgray;
  min-height: 240px;
}
.payment-calendar .DayPicker-Months {
  justify-content: space-evenly;
  flex-wrap: nowrap;
}
.payment-calendar .DayPicker-Month {
  margin: 0;
}
.payment-calendar .DayPicker-Day {
  outline: none !important;
  border-radius: 0;
  opacity: 0.8;
  color: blue;
}
/* DAY STYLES */
.payment-calendar .DayPicker-Day:hover:not(.DayPicker-Day--disabled) {
  background-color: lightsteelblue !important;
  color: black;
}
.payment-calendar .DayPicker-Day.DayPicker-Day--hovering {
  background-color: lightsteelblue !important;
}
.payment-calendar .DayPicker-Day--disabled {
  color: dimgray;
}
.payment-calendar .DayPicker-Day--wednesday {
  font-weight: bold !important;
}
.payment-calendar .DayPicker-Day--outside {
  opacity: 0.15 !important;
}
.payment-calendar .DayPicker-Day.DayPicker-Day--today {
  color: crimson !important;
  font-weight: bold;
}
/* SELECTED STYLES */
.payment-calendar .DayPicker-Day--selected {
  background-color: darkblue !important;
  color: white;
}
.payment-calendar .DayPicker-Day--selected.DayPicker-Day--selectStart {
  border-top-left-radius: 50% !important;
  border-bottom-left-radius: 50% !important;
  opacity: 0.6;
}
.payment-calendar .DayPicker-Day--selected.DayPicker-Day--selectEnd {
  border-top-right-radius: 50% !important;
  border-bottom-right-radius: 50% !important;
  opacity: 1;
}
.payment-calendar .DayPicker-Day--start {
  border-top-left-radius: 50% !important;
  border-bottom-left-radius: 50% !important;
  opacity: 0.6;
}
.payment-calendar .DayPicker-Day--end {
  border-top-right-radius: 50% !important;
  border-bottom-right-radius: 50% !important;
  opacity: 1;
}
/* STATUS STYLES */
.payment-calendar .DayPicker-Day--pending {
  color: white !important;
  background-color: ${Enum.StatusColorMap.PENDING} !important;
}
.payment-calendar .DayPicker-Day--paid {
  color: white !important;
  background-color: ${Enum.StatusColorMap.PAID} !important;
}
.payment-calendar .DayPicker-Day--delinquent {
  color: white !important;
  background-color: ${Enum.StatusColorMap.DELINQUENT} !important;
}
`}</style>
  </Helmet>
);
