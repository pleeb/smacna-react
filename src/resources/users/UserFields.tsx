import React, { Fragment } from "react";
import phone from "google-libphonenumber";
import get from "lodash/get";
import { withStyles, TextField as _TextField, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { DashPanel, Enum, Button, Input, Field, ReferenceManyContainer, Alias, sourceFieldFromMap, sourceField } from "../";

const TextField = withStyles({
  root: {
    // marginTop: 16,
    // marginBottom: 8
  },
  InputProps: {
    fontSize: "0.875rem",
    fontWeight: 400,
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    lineHeight: "1.46429em",
    margin: 0,
    display: "block"
  }
})(({ classes, ...props }: any) => (
  <_TextField margin="normal" fullWidth InputProps={{ readOnly: true, disableUnderline: true, className: classes.InputProps }} {...props} />
));

export const UserFields = connect(({ session: user }: any) => ({ user }))(({ user }: any) => {
  return (
    <Fragment>
      <TextField value={user.full_name} label={Alias["full_name"]} />
      <TextField value={user.title} label={Alias["title"]} />
      <TextField value={user.email} label={Alias["email"]} />
      <PhoneField value={user.phone} label={Alias["phone"]} />
    </Fragment>
  );
});
const PNF = phone.PhoneNumberFormat;
const phoneUtil = phone.PhoneNumberUtil.getInstance();
const PhoneField = ({ value, label, ...props }: any) => {
  if (value) {
    const number = phoneUtil.parseAndKeepRawInput(value, "US");
    value = phoneUtil.format(number, PNF.NATIONAL);
  } else {
    value = "----";
  }
  return <TextField value={value} label={label} {...props} />;
};
