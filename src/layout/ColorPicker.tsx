import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button, Card, CardContent, CardHeader, Typography } from "@material-ui/core";

export class ColorPicker extends Component<any, any> {
  state = { background: this.props.background, foreground: this.props.foreground };
  selectBackground = (background: string) => {
    this.setState({ background });
    this.props.selectBackground(background);
  };
  selectForeground = (foreground: string) => {
    this.setState({ foreground });
    this.props.selectForeground(foreground);
  };
  render() {
    return (
      <Card style={{ position: "absolute", left: 25, top: 25, maxWidth: 500 }}>
        <CardContent>
          <Typography paragraph>
            Click a color to choose, <br />
            let me know of the color scheme you decide on.
          </Typography>
          <Typography paragraph variant="subheading">
            Login Color: <strong>{this.state.foreground}</strong>
          </Typography>
          <Palette current={this.state.foreground} onClick={this.selectForeground} />
          <br />
          <Typography paragraph variant="subheading">
            Background Color: <strong>{this.state.background}</strong>
          </Typography>
          <Palette current={this.state.background} onClick={this.selectBackground} />
        </CardContent>
      </Card>
    );
  }
}
const Palette = ({ current, onClick }) => (
  <div style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}>
    {colors.map((e, i) => (
      <Color disabled={current === e} color={e} key={i} onClick={onClick} />
    ))}
  </div>
);
const Color = ({ disabled, color, onClick }) => (
  <Button
    disabled={disabled}
    style={{
      width: 28,
      height: 28,
      minWidth: 0,
      minHeight: 0,
      padding: 0,
      backgroundColor: color,
      border: disabled ? "3px solid black" : "1px solid lightgray"
    }}
    onClick={() => onClick(color)}
  >
    {" "}
  </Button>
);
const colors = [
  "AliceBlue",
  "AntiqueWhite",
  "Aqua",
  "Aquamarine",
  "Azure",
  "Beige",
  "Bisque",
  "Black",
  "BlanchedAlmond",
  "Blue",
  "BlueViolet",
  "Brown",
  "BurlyWood",
  "CadetBlue",
  "Chartreuse",
  "Chocolate",
  "Coral",
  "CornflowerBlue",
  "Cornsilk",
  "Crimson",
  "Cyan",
  "DarkBlue",
  "DarkCyan",
  "DarkGoldenRod",
  "DarkGray",
  "DarkGrey",
  "DarkGreen",
  "DarkKhaki",
  "DarkMagenta",
  "DarkOliveGreen",
  "DarkOrange",
  "DarkOrchid",
  "DarkRed",
  "DarkSalmon",
  "DarkSeaGreen",
  "DarkSlateBlue",
  "DarkSlateGray",
  "DarkSlateGrey",
  "DarkTurquoise",
  "DarkViolet",
  "DeepPink",
  "DeepSkyBlue",
  "DimGray",
  "DimGrey",
  "DodgerBlue",
  "FireBrick",
  "FloralWhite",
  "ForestGreen",
  "Fuchsia",
  "Gainsboro",
  "GhostWhite",
  "Gold",
  "GoldenRod",
  "Gray",
  "Grey",
  "Green",
  "GreenYellow",
  "HoneyDew",
  "HotPink",
  "IndianRed",
  "Indigo",
  "Ivory",
  "Khaki",
  "Lavender",
  "LavenderBlush",
  "LawnGreen",
  "LemonChiffon",
  "LightBlue",
  "LightCoral",
  "LightCyan",
  "LightGoldenRodYellow",
  "LightGray",
  "LightGrey",
  "LightGreen",
  "LightPink",
  "LightSalmon",
  "LightSeaGreen",
  "LightSkyBlue",
  "LightSlateGray",
  "LightSlateGrey",
  "LightSteelBlue",
  "LightYellow",
  "Lime",
  "LimeGreen",
  "Linen",
  "Magenta",
  "Maroon",
  "MediumAquaMarine",
  "MediumBlue",
  "MediumOrchid",
  "MediumPurple",
  "MediumSeaGreen",
  "MediumSlateBlue",
  "MediumSpringGreen",
  "MediumTurquoise",
  "MediumVioletRed",
  "MidnightBlue",
  "MintCream",
  "MistyRose",
  "Moccasin",
  "NavajoWhite",
  "Navy",
  "OldLace",
  "Olive",
  "OliveDrab",
  "Orange",
  "OrangeRed",
  "Orchid",
  "PaleGoldenRod",
  "PaleGreen",
  "PaleTurquoise",
  "PaleVioletRed",
  "PapayaWhip",
  "PeachPuff",
  "Peru",
  "Pink",
  "Plum",
  "PowderBlue",
  "Purple",
  "RebeccaPurple",
  "Red",
  "RosyBrown",
  "RoyalBlue",
  "SaddleBrown",
  "Salmon",
  "SandyBrown",
  "SeaGreen",
  "SeaShell",
  "Sienna",
  "Silver",
  "SkyBlue",
  "SlateBlue",
  "SlateGray",
  "SlateGrey",
  "Snow",
  "SpringGreen",
  "SteelBlue",
  "Tan",
  "Teal",
  "Thistle",
  "Tomato",
  "Turquoise",
  "Violet",
  "Wheat",
  "White",
  "WhiteSmoke",
  "Yellow",
  "YellowGreen"
];
