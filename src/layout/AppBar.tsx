import React from "react";
import { connect } from "react-redux";
import { AppBar, UserMenu, MenuItemLink, translate } from "react-admin";
import { withRouter } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import UserIcon from "@material-ui/icons/AccountBox";
import CompanyIcon from "@material-ui/icons/Work";
import { withStyles } from "@material-ui/core/styles";
import { ProfileIcon } from "../components";

const styles = {
  title: {
    flex: 1,
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    overflow: "hidden"
  },
  spacer: {
    flex: 1
  }
} as any;
const CustomUserMenu = withRouter(
  connect(({ session: { id: user_id, company_id, role } }: any) => {
    return {
      user_id,
      company_id,
      role
    };
  })(({ user_id, company_id, role, ...props }) => (
    <UserMenu icon={<ProfileIcon role={role} />} {...props}>
      <MenuItemLink to={`/users/${user_id}/show`} primaryText="View Profile" leftIcon={<UserIcon />} />
      <MenuItemLink to={`/companies/${company_id}/show`} primaryText="Company Profile" leftIcon={<CompanyIcon />} />
    </UserMenu>
  ))
);

const CustomAppBar = ({ classes, ...props }) => (
  <AppBar {...props} userMenu={<CustomUserMenu />}>
    <Typography variant="title" color="inherit" className={classes.title} id="react-admin-title" />
  </AppBar>
);

// <span className={classes.spacer} />
export default withStyles(styles)(CustomAppBar);
