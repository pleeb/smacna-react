export const FETCH_REPORT = "FETCH_REPORT";
export const FETCH_REPORT_SUCCESS = "FETCH_REPORT_SUCCESS";
export const FETCH_REPORT_ERROR = "FETCH_REPORT_ERROR";

export const fetchReport = (body: any) => {
  return {
    type: FETCH_REPORT
  };
};
