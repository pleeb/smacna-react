import React from "react";
import { ReadonlyTable } from "./table";
import {
  Title,
  DatagridMapper,
  DashPanel,
  Enum,
  Field,
  Button,
  ReferenceManyContainer,
  Alias,
  sourceFieldFromMap,
  sourceField,
  GridItem,
  GridContainer,
  SimpleShowContainer,
  Actionbar
} from "../";
import { ReferenceManyPayment } from "../payments";

const { Tab, TabbedShowLayout } = Field;

const ShowActions = ({ permissions, ...props }) => {
  const record = props.data || {};
  let button;
  switch (permissions) {
    case Enum.Role.Admin:
      button = null;
      break;
    case Enum.Role.Master:
      button = <Button.DashPaymentAdjustButton />;
      break;
    case Enum.Role.User:
      button = [
        record.status === Enum.Status.DELINQUENT ? <Button.DashPaymentPayButton key={1} /> : null,
        <Button.DashPaymentAdjustButton key={2} />
      ];
      break;
    default:
      button = null;
  }
  return (
    <Actionbar verb={props.dash ? "Viewing Contribution" : "Showing Contribution"} title={record["cid"]} {...props}>
      {button}
      <Button.ListButton />
    </Actionbar>
  );
};
const WrapTitle = ({ record }: any) => <Title title={`Contribution #${record.cid}`} />;
export const ContributionShow = ({ permissions, ...props }: any) => {
  return (
    <Field.Show {...props} title=" " actions={<ShowActions permissions={permissions} />}>
      <SimpleShowContainer>
        <GridContainer withMargin>
          <GridItem md={6} xs={5}>
            <WrapTitle />
            <ReadonlyTable />
          </GridItem>
          <GridItem xs={true}>
            <SimpleShowContainer>
              {sourceField("Company.company_name")}
              {sourceField("company_id")}
              {sourceField("cid")}
            </SimpleShowContainer>
          </GridItem>
          <GridItem xs={true}>
            <SimpleShowContainer>
              {sourceField("reporting_week")}
              {sourceField("reporting_week_start")}
              {sourceField("reporting_week_end")}
              {sourceField("reporting_week_count")}
            </SimpleShowContainer>
          </GridItem>
          <GridItem xs={true}>
            <SimpleShowContainer>
              {sourceField("amount_total")}
              {sourceField("amount_paid")}
              {sourceField("amount_owed")}
              {sourceField("status")}
            </SimpleShowContainer>
          </GridItem>
          {permissions >= Enum.Role.Admin && (
            <GridItem xs={true}>
              <SimpleShowContainer>
                {sourceField("pay_status")}
                {sourceField("pay_method")}
                {sourceField("receive_date")}
                {sourceField("deposit_date")}
                {sourceField("comment")}
              </SimpleShowContainer>
            </GridItem>
          )}
        </GridContainer>
        <TabbedShowLayout>
          <Tab label="Payment History">
            <ReferenceManyPayment permissions={permissions} target="contribution_id" />
          </Tab>{" "}
        </TabbedShowLayout>
      </SimpleShowContainer>
    </Field.Show>
  );
};
const Dash = ({ permissions, ...props }: any) => {
  return (
    <Field.Show {...props} basePath="/contributions" resource="contributions" actions={<ShowActions permissions={permissions} dash />}>
      <GridContainer withMargin>
        <GridItem xs={6}>
          <ReadonlyTable />
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("Company.company_name")}
            {sourceField("company_id")}
            {sourceField("cid")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("reporting_week_end")}
            {sourceField("reporting_week")}
            {sourceField("reporting_week_count")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("amount_total")}
            {sourceField("amount_paid")}
            {sourceField("amount_owed")}
            {permissions >= Enum.Role.Admin && sourceField("calculation")}
          </SimpleShowContainer>
        </GridItem>
        {permissions >= Enum.Role.Admin && (
          <GridItem xs={true}>
            <SimpleShowContainer>
              {sourceField("receive_date")}
              {sourceField("deposit_date")}
              {sourceField("comment")}
              {sourceField("status")}
            </SimpleShowContainer>
          </GridItem>
        )}
      </GridContainer>
    </Field.Show>
  );
};
export const ContributionDashShow = (props: any) => {
  return (
    <DashPanel resource={Enum.Resource.Contributions} anchor={Enum.Anchor.Bottom} crud={Enum.Crud.Read} {...props}>
      <Dash />
    </DashPanel>
  );
};
