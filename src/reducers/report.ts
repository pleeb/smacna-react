import { RESET_FORM } from "react-admin";

import { combineReducers } from "redux";

const init = {
  time: {
    start: 0,
    end: 0,
    total: 0,
    query: 0,
    latency: 0
  },
  query: "",
  request: {
    selects: [],
    groups: [],
    computes: [],
    companies: {},
    contributions: {}
  },
  datas: [],
  fetching: false
};
class Timer {
  get start() {
    return this._start;
  }
  get end() {
    return this._end;
  }
  get time() {
    return +this._end - +this._start;
  }
  private _start: Date;
  private _end: Date;
  constructor() {}
  startTimer(): this {
    this._start = new Date();
    this._end = this._start;
    return this;
  }
  endTimer(): this {
    if (this._start) {
      this._end = new Date();
      return this;
    }
    console.log("WARNING: endTimer() was called but timer was not started.");
    return this;
  }
  resetTimer(): this {
    this._start = null;
    this._end = null;
    return this;
  }
}
const time = new Timer();
export function report(state = init, action) {
  // console.log(action);
  if (action.type === RESET_FORM) return init;
  else if (action.meta && action.meta.resource === "reports/query") {
    switch (action.type) {
      case "CUSTOM_FETCH": {
        const start = new Date();
        const end = start;
        const total = +end - +start;
        const datas = [];
        const query = 0;
        const time = { start, end, total, query, latency: total - query };
        const selects = action.payload.data.selects || [];
        const groups = action.payload.data.groups || [];
        const computes = action.payload.data.computes || [];
        const companies = action.payload.data.companies || {};
        const contributions = action.payload.data.contributions || {};
        return {
          ...state,
          query: "",
          time,
          datas,
          request: { selects, groups, computes, companies, contributions }
        };
      }
      case "CUSTOM_FETCH_LOADING": {
        return { ...state, fetching: true };
      }
      case "CUSTOM_FETCH_SUCCESS": {
        const {
          time: { start }
        } = state;
        const end = new Date();
        const total = +end - +start;
        const report = action.payload.data.id;
        const datas = report.datas;
        const {
          time: { total: query }
        } = report;
        const time = { start, end, total, query, latency: total - query };
        return { ...state, fetching: false, time, query: report.query, datas };
      }
      case "CUSTOM_FETCH_FAILURE": {
        return state;
      }
      default:
        return state;
    }
  }
  return state;
}
