import { DateRange } from "./DateRange";
import moment, { Moment } from "moment";

const NUM_VISIBLE_MONTHS: number = 4;
const NUM_SELECTABLE_WEEKS: number = 4;
const NUM_CURRENT_MONTH: number = 1;

export class CalendarConfig {
  readonly PERIOD_DAY_START: number = moment()
    .day("thursday")
    .day();
  readonly PERIOD_DAY_END: number = moment()
    .day("wednesday")
    .day();
  private _today: Moment;
  private _selectable: DateRange;
  private _traversable: DateRange;
  get today() {
    return this._today.clone();
  }
  get selectable() {
    return this._selectable;
  }
  get traversable() {
    return this._traversable;
  }
  constructor(from?: Date, to?: Date, private padding: number = 2) {
    if (from && to) {
      this.initializeFromTo(from, to);
    } else {
      this.initializeFromToday(from);
    }
  }
  private initializeToday(today?: Date): Moment {
    this._today = moment(today || new Date());

    return this.today;
  }
  private initializeFromTo(from: Date, to: Date): void {
    const today = this.initializeToday();
    const start = this.getPeriodStartingDay(from);
    const end = this.getPeriodEndingDay(to);
    this.initialize(start.toDate(), end.toDate());
  }
  private initializeFromToday(date?: Date): void {
    const today = this.initializeToday(date);
    const start = this.getPeriodStartingDay(today);
    const end = this.getPeriodEndingDay(start.clone().add(NUM_SELECTABLE_WEEKS, "weeks"));
    this.initialize(start.toDate(), end.toDate());
  }
  private initialize(from: Date, to: Date): void {
    this._selectable = new DateRange(from, to);
    const start = this.selectable.from.startOf("month").subtract(this.padding, "months");
    const end = this.selectable.to.endOf("month").add(this.padding, "months");
    this._traversable = new DateRange(start.toDate(), end.toDate());
  }
  isSelectable(target: Date | Moment): boolean {
    return moment(target).isBetween(this.selectable.from, this.selectable.to.add(1, "day"));
  }
  getPeriodStartingDay(target: Date | Moment, inclusive: boolean = false): Moment {
    const date: Moment = moment(target);
    const curb = date.clone().day(this.PERIOD_DAY_START);
    if (date.isBefore(curb) || (inclusive && date.isSame(curb))) return curb.subtract(1, "week");
    else return curb;
  }
  getPeriodEndingDay(target: Date | Moment, inclusive: boolean = false): Moment {
    const date: Moment = moment(target);
    const curb = date.clone().day(this.PERIOD_DAY_END);
    if (date.isAfter(curb) || (inclusive && date.isSame(curb))) return curb.add(1, "week");
    else return curb;
  }
}
