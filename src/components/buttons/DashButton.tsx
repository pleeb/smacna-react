import React, { ComponentType } from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
import Button from "./Button";
// import { Button } from "react-admin";
// import { Button } from "@material-ui/core";
import { dashAction, DashAction, closeAllDash } from "../../actions/dash";
import { withStyles } from "@material-ui/core";
import { Enum, isMaster } from "../";
import { destroy } from "redux-form";

export const stylize = withStyles(theme => ({
  button: {
    // minWidth: 80,
    margin: "0 4px"
  }
}));
const _stylize = withStyles(theme => ({
  button: {
    // minWidth: 80,
    margin: "0 4px"
  },
  large: {
    margin: "0 8px",
    width: "50%",
    padding: "24px 28px",
    color: theme.palette.error.main,
    border: "1px solid red",
    "&:hover": {
      color: theme.palette.error.main
    }
  },
  medium: {
    margin: "0 4px"
  },
  small: {
    margin: "0 4px"
  }
}));
interface IProps {
  label?: string;
  record?: { id: number };
  Icon: any;
  crud: Enum.Crud;
  form?: boolean;
  size?: "small" | "medium" | "large";
  color?: "primary" | "secondary" | "default" | "inherit";
  variant?: "text" | "flat" | "outlined" | "raised" | "fab" | "extendedFab";
  openDash?: DashAction;
  resource?: string;
}
const mapStateToProps = ({ session: { role: permissions } }: any, { label }: any) => {
  return { label: isMaster(permissions) ? `Master ${label}` : label };
};
const enhance = compose(
  _stylize,
  connect<any, any, IProps>(
    mapStateToProps,
    (dispatch, { resource, crud, form }: any) => ({
      openDash: record => dispatch(dashAction[resource][crud].open(record)),
      closeDash: record => {
        if (resource && crud) dispatch(dashAction[resource][crud].close());
        else dispatch(closeAllDash());
        if (form || crud === Enum.Crud.Update || crud === Enum.Crud.Create) dispatch(destroy("record-form"));
      }
    })
  )
);
export const DashButton = enhance(
  ({
    style,
    classes,
    record,
    openDash,
    closeDash,
    close,
    onClick,
    label,
    variant = "outlined",
    size = "small",
    color = "primary",
    Icon,
    crud
  }: any) => {
    const dashAction = close ? closeDash : openDash;
    return (
      <Button
        className={classes[size]}
        style={style}
        size={size}
        color={color}
        variant={variant}
        onClick={e => {
          e.stopPropagation();
          dashAction(record);
          if (onClick) onClick();
        }}
        label={label}
      >
        <Icon />
      </Button>
    );
  }
);
