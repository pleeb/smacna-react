import { React, parse, TSourceRecord, HoursSource } from "../";

export const FieldNumHours = ({ record, source }: TSourceRecord<HoursSource>) => <span>{parse.number(record[source])}</span>;
