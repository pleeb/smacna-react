import React, { Children, Fragment, cloneElement } from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";
import LiveHelp from "@material-ui/icons/LiveHelp";
import Close from "@material-ui/icons/Close";
import Send from "@material-ui/icons/Send";
import { Button, TextField } from "@material-ui/core";
import {
  CircularProgress,
  withStyles,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  DialogContentText
} from "@material-ui/core";

const CloseButton = props => (
  <Button
    disabled={props.disabled}
    color="secondary"
    onClick={props.onClick}
    style={{ width: 36, height: 36, borderRadius: 5 }}
    variant="fab"
  >
    <Close />
  </Button>
);
const SendButton = props => (
  <Button color="secondary" onClick={props.onClick} disabled={props.disabled} variant="contained">
    {props.sending ? <CircularProgress size={20} /> : <Send />}
    <span style={{ marginLeft: 8 }}>Send</span>
  </Button>
);
interface IProps {
  button?: React.ReactElement;
  open?: boolean;
  title: string;
  children?: any;
}
class DialogWrapper extends React.Component<any, any> {
  state = {
    open: this.props.open || false,
    content: "",
    sending: false,
    sent: false
  };
  handleClickOpen = () => {
    this.setState({ open: true, sent: false });
  };
  handleClose = () => {
    !this.sending && this.setState({ open: false });
  };
  handleChange = event => {
    this.setState({ content: event.target.value });
  };
  get invalid() {
    return this.state.content.length <= 0;
  }
  get sending() {
    return this.state.sending;
  }
  get disabled() {
    return this.invalid || this.sending;
  }
  get sent() {
    return this.state.sent;
  }
  handleSend = async () => {
    const { user_id, email } = this.props;
    const { content } = this.state;
    this.setState({ sending: true });
    const resp = await fetch("http://localhost:3000/v1/contact", {
      method: "POST",
      body: JSON.stringify({ user_id, content }) as any,
      headers: { "Content-Type": "application/json" }
    });
    this.setState({ sending: false, sent: true, content: "" });
    setTimeout(this.handleClose, 2000);
    console.log("fetched", resp);
  };
  render() {
    const { button, classes, user_id, email } = this.props;
    const { content } = this.state;
    return (
      <Fragment>
        <Button onClick={this.handleClickOpen} color="secondary" size="large" variant="fab" className={classes.button}>
          <LiveHelp />
        </Button>
        <Dialog
          open={this.state.open}
          classes={{ paper: classes.root }}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogActions style={{ justifyContent: "space-between", marginBottom: 28 }}>
            <Typography style={{ marginLeft: 24 }} variant="title">
              Contact Us at SMACNA.
            </Typography>
            <CloseButton onClick={this.handleClose} disabled={this.sending} />
          </DialogActions>
          <DialogContent style={{ display: "flex" }}>
            {this.sent ? (
              <Typography style={{ marginLeft: 24 }} variant="subheading">
                Thank you. Your message has been sent.
              </Typography>
            ) : (
              <TextField
                autoFocus
                disabled={this.sending}
                className={classes.textfield}
                onChange={this.handleChange}
                value={content}
                rows={5}
                fullWidth
                multiline
                placeholder={`Contact us with questions or comments.\nA confirmation of receipt will be sent to your email at ${email}.`}
              />
            )}
          </DialogContent>
          <DialogActions>
            {this.sent ? null : <SendButton onClick={this.handleSend} disabled={this.disabled} sending={this.sending} />}
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}
const enhance = compose(
  withStyles(theme => ({
    root: {
      width: 500
    },
    button: {
      position: "absolute" as any,
      bottom: 100,
      right: 100,
      minWidth: 0,
      zIndex: 5
    },
    textfield: {
      border: "1px solid lightgray",
      borderRadius: 5,
      paddingLeft: 10,
      paddingRight: 10
    }
  })),
  connect(({ session: { id: user_id, email } }: any) => ({ user_id, email })),
  reduxForm({
    form: "simple"
  })
);
export const ContactUsDialog = enhance(DialogWrapper);
