import { USER_CHECK, USER_LOGIN_SUCCESS, USER_LOGOUT, REGISTER_RESOURCE } from "react-admin";
import { getLocalAuth, setLocalAuth } from "../data/authProvider";

export function session(state = {} as any, { type, payload, requestPayload, meta }) {
  // console.log(type, payload, requestPayload, meta);
  switch (type) {
    case "RA/CRUD_UPDATE_SUCCESS": {
      if (isSessionUpdate(state, payload.data, meta)) {
        setLocalAuth(payload.data);
        return { ...payload.data };
      }
      return state;
    }
    case REGISTER_RESOURCE:
    case USER_CHECK: {
      const auth = getLocalAuth();
      if (auth && auth.user) {
        if (!state.id) return { ...auth.user };
        return state;
      }
      return {};
    }
    case USER_LOGIN_SUCCESS: {
      return { ...payload };
    }
    case USER_LOGOUT:
      return {};
    default:
      return state;
  }
}
const isSessionUpdate = (session, record, { resource, fetchResponse }) =>
  resource === "users" && fetchResponse === "UPDATE" && session.id === record.id ? true : false;
