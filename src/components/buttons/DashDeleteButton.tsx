import React from "react";
import Icon from "@material-ui/icons/Edit";
import { DashButton, Enum } from "./";

export const DashDeleteButton = props => (
  <DashButton
    Icon={Icon}
    crud={Enum.Crud.Delete}
    label="Delete"
    record={props.record}
    resource={props.resource}
    variant="flat"
  />
);
