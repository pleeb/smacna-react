import React from "react";

import * as component from "./components";

export { connect } from "react-redux";
export { parse } from "../../../utils";
import { Enum } from "../../";
export { React, Enum, component };

const hoursRateMapping = {
  hours_light_commercial: "rate_light_commercial",
  hours_mechanic: "rate_mechanic",
  hours_target: "rate_mechanic",
  hours_apprentice: "rate_apprentice"
};
const hoursAmountMapping = {
  hours_light_commercial: "amount_light_commercial",
  hours_mechanic: "amount_mechanic",
  hours_target: "amount_mechanic",
  hours_apprentice: "amount_apprentice"
};
export const getRateSource = (source: HoursSource): RateSource => {
  return hoursRateMapping[source];
};
export const getAmountSource = (source: HoursSource): AmountSource => {
  return hoursAmountMapping[source];
};

export { InputTable } from "./input";
export { ReadonlyTable } from "./readonly";

export type SourceNum = "num_light_commercial" | "num_mechanic" | "num_apprentice" | "num_total";
export type HoursSource = "hours_light_commercial" | "hours_mechanic" | "hours_apprentice" | "hours_target" | "hours_total";
export type AmountSource = "amount_light_commercial" | "amount_mechanic" | "amount_apprentice" | "amount_total";
export type RateSource = "rate_light_commercial" | "rate_mechanic" | "rate_apprentice";

export type SourceTypes = HoursSource | AmountSource | RateSource | SourceNum;

export interface TSourceRecord<T extends SourceTypes> {
  record?: any;
  source?: T;
}
export interface IState {
  form: {
    "record-form": {
      values: any;
      syncErrors?: any;
    };
  };
}
