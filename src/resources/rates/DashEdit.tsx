import React from "react";
import { Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@material-ui/core";
import { connect } from "react-redux";
import { ReadonlyTable } from "../contributions/table";
import {
  parse,
  Field,
  Enum,
  DashPanel,
  Input,
  Button,
  Alias,
  GridContainer,
  GridItem,
  sourceFieldFromMap,
  sourceField,
  SimpleShowContainer,
  SimpleFormContainer,
  Actionbar,
  mui
} from "../";
import { closeAllDash } from "../../actions";
import { refreshView } from "react-admin";
import { InputMoney } from "./InputRateTable";

const Actions = ({ permissions, ...props }) => <Actionbar verb="Update Payment" title={(props.data || {})["cid"]} {...props} />;
const Dash = ({ permissions, anchor, record, ...props }: any) => {
  return (
    <Input.Edit id={record.id} {...props} title={` Update Rate`} resource="rates" basePath="/rates" actions={null}>
      <WrapForm permissions={permissions} value={record} />
    </Input.Edit>
  );
};
const WrapForm = connect(
  null,
  dispatch => ({
    redirect: (basePath, id, data) => {
      dispatch(closeAllDash());
      dispatch(refreshView());
      return false;
    }
  })
)(({ value: record, ...props }: any) => {
  return (
    <SimpleFormContainer toolbar={<Toolbar />} record={record} {...props}>
      <Wrap />
    </SimpleFormContainer>
  );
}) as any;
const Wrap = ({ record }: any) => {
  return (
    <Table padding="checkbox">
      <TableHead>
        <TableRow>
          <TableCell numeric>
            <strong>Building Trade Mechanics</strong>
          </TableCell>
          <TableCell numeric>
            <strong>Apprentice Workers</strong>
          </TableCell>
          <TableCell numeric>
            <strong>Light Commercial Workers</strong>
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell numeric>
            <InputMoney source="rate_mechanic" />
          </TableCell>
          <TableCell numeric>
            <InputMoney source="rate_apprentice" />
          </TableCell>
          <TableCell numeric>
            <InputMoney source="rate_light_commercial" />
          </TableCell>
        </TableRow>
      </TableBody>
    </Table>
  );
};
const Toolbar = (props: any) => (
  <Input.Toolbar {...props} style={{ backgroundColor: "lavender" }}>
    <Button.SaveButton color="primary" disabled={props.invalid} />
  </Input.Toolbar>
);
export const RateDashEdit = (props: any) => {
  return (
    <DashPanel dialog resource={Enum.Resource.Rates} anchor={Enum.Anchor.Right} crud={Enum.Crud.Update} {...props}>
      <Dash />
    </DashPanel>
  );
};
