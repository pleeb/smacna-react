import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { withStyles } from "@material-ui/core";
import LabelIcon from "@material-ui/icons/Label";
import { withRouter } from "react-router-dom";
import { translate, MenuItemLink, Responsive } from "react-admin";
import DashboardMenuItem from "./DashboardMenuItem";
import SubMenu from "./SubMenu";
import { Enum } from "../";
import { ProfileIcon, DialogWrapper } from "../components";
import { ColorLegend } from "./ColorLegend";

class Menu extends Component<any, any> {
  state = {
    menuCatalog: true,
    menuSales: true,
    menuCustomers: true
  };

  static propTypes = {
    onMenuClick: PropTypes.func,
    logout: PropTypes.object
  };

  handleToggle = menu => {
    this.setState(state => ({ [menu]: !state[menu] }));
  };

  render() {
    const { classes, onMenuClick, open, logout, translate, user_id, company_id, permissions, currentPath } = this.props;
    return (
      <div>
        {" "}
        <DashboardMenuItem classes={classes} onClick={onMenuClick} />
        <SubMenu
          handleToggle={() => this.handleToggle("menuSales")}
          isOpen={this.state.menuSales}
          sidebarIsOpen={open}
          name="Resources"
          icon={<Enum.Icon.Home />}
        >
          <MenuItemLink
            classes={classes}
            to={`/contributions`}
            primaryText="Contributions"
            leftIcon={<Enum.Icon.Contributions />}
            onClick={onMenuClick}
          />
          {permissions >= Enum.Role.Admin ? (
            <MenuItemLink
              classes={classes}
              to={`/payments`}
              primaryText="Payments"
              leftIcon={<Enum.Icon.Payments />}
              onClick={onMenuClick}
            />
          ) : null}
          <MenuItemLink classes={classes} to={`/users`} primaryText="Users" leftIcon={<Enum.Icon.Users />} onClick={onMenuClick} />
          {permissions >= Enum.Role.Admin ? (
            <MenuItemLink
              classes={classes}
              to={`/companies`}
              primaryText="Companies"
              leftIcon={<Enum.Icon.Companies />}
              onClick={onMenuClick}
            />
          ) : null}
          {permissions >= Enum.Role.Admin ? (
            <MenuItemLink classes={classes} to={"/rates/1/show"} primaryText="Rates" leftIcon={<Enum.Icon.Rates />} onClick={onMenuClick} />
          ) : null}
          {permissions >= Enum.Role.Admin ? (
            <MenuItemLink
              classes={classes}
              to="/reports"
              primaryText="Reports (TEST)"
              leftIcon={<Enum.Icon.Reports />}
              onClick={onMenuClick}
            />
          ) : null}
        </SubMenu>
        <SubMenu
          handleToggle={() => this.handleToggle("menuCatalog")}
          isOpen={this.state.menuCatalog}
          sidebarIsOpen={open}
          name="Account"
          icon={<Enum.Icon.Settings />}
        >
          <MenuItemLink
            classes={classes}
            to={{ pathname: `/users/${user_id}/show`, state: { fromProfileLink: true } }}
            primaryText="Profile"
            leftIcon={<ProfileIcon role={permissions} />}
            onClick={onMenuClick}
          />
          <MenuItemLink
            classes={classes}
            to={{ pathname: `/companies/${company_id}/show`, state: { fromProfileLink: true } }}
            primaryText="Company"
            leftIcon={<Enum.Icon.Company />}
            onClick={onMenuClick}
          />
        </SubMenu>
        <MenuItemLink classes={classes} to={`/login`} primaryText="Log Out" leftIcon={<Enum.Icon.Logout />} onClick={onMenuClick} />
        <ColorLegend />
      </div>
    );
  }
}

const mapStateToProps = ({
  session: { id: user_id, company_id, role: permissions },
  admin,
  theme,
  i18n,
  router: {
    location: { basePath: currentPath }
  }
}) => {
  return {
    open: admin.ui.sidebarOpen,
    theme: theme,
    locale: i18n.locale,
    user_id,
    company_id,
    permissions,
    currentPath
  };
};
const styles = withStyles(theme => ({
  root: {
    color: theme.palette.text.secondary,
    display: "flex",
    alignItems: "flex-start"
  },
  active: {
    color: theme.palette.primary.main,
    fontWeight: "bold"
  }
}));
const enhance = compose(
  withRouter,
  styles,
  connect(
    mapStateToProps,
    {}
  ),
  translate
);

export default enhance(Menu);
