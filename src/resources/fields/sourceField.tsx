import React, { Fragment } from "react";
import phone from "google-libphonenumber";
import {
  ArrayField,
  BooleanField,
  DateField,
  EmailField,
  FileField,
  FunctionField,
  ImageField,
  NumberField,
  ReferenceArrayField,
  ReferenceField,
  ReferenceManyField,
  RichTextField,
  SelectField,
  SingleFieldList,
  TextField,
  UrlField
} from "../../components/fields";
import { withStyles, Typography } from "@material-ui/core";
import { Person, DateRange } from "@material-ui/icons";
import { Alias, Enum, FieldType, numericFields, Button, OwnerIcon, Chip } from "../";
import get from "lodash/get";
const isShortOrDelinquent = status => status === Enum.Status.DELINQUENT || status === Enum.Status.SHORT;
const enhanceMoney = withStyles(theme => ({
  BAD_VALUE: { fontWeight: "bold", color: Enum.ColorMap.BAD_VALUE },
  Amount: { fontWeight: "bold", color: Enum.ColorMap.Money.Amount },
  AmountTotal: { fontWeight: "bold", color: Enum.ColorMap.Money.AmountTotal },
  AmountPaid: { fontWeight: "bold", color: Enum.ColorMap.Money.AmountPaid },
  Zero: { fontWeight: "bold", color: Enum.ColorMap.Money.Zero },
  UnderPay: { fontWeight: "bold", color: Enum.ColorMap.Money.UnderPay },
  OverPay: { fontWeight: "bold", color: Enum.ColorMap.Money.OverPay }
}));
const getMoneyClass = (classes, amount: number) => {
  if (typeof amount !== "number") return classes.BAD_VALUE;
  if (amount === 0) return classes.Zero;
  if (amount > 0) return classes.OverPay;
  if (amount < 0) return classes.UnderPay;
};
const enhanceChip = withStyles(theme => ({
  status: {
    display: "flex",
    flexDirection: "column"
  },
  Default: { fontWeight: 500, color: Enum.ColorMap.Status.Default },
  MAIL: { fontWeight: 500, color: Enum.ColorMap.Status.Default },
  ELECTRONIC: { fontWeight: 500, color: Enum.ColorMap.Status.Default },
  PENDING: { fontWeight: 500, color: Enum.ColorMap.Status.PENDING },
  SUCCESS: { fontWeight: 500, color: Enum.ColorMap.Status.SUCCESS },
  FAILED: { fontWeight: 500, color: Enum.ColorMap.Status.FAILED },
  PAID: { fontWeight: 500, color: Enum.ColorMap.Status.PAID },
  DELINQUENT: { fontWeight: 500, color: Enum.ColorMap.Status.DELINQUENT },
  OVERPAY: { fontWeight: 500, color: Enum.ColorMap.Status.PAID },
  SHORT: { fontWeight: 500, color: Enum.ColorMap.Status.DELINQUENT }
}));
const enhanceDate = withStyles({
  root: { display: "flex", flexDirection: "row", alignItems: "center" },
  count: {
    marginRight: 4,
    textAlign: "center",
    color: "inherit"
  },
  start: { color: "inherit" },
  end: { fontWeight: "bold", color: "inherit" }
});
const PNF = phone.PhoneNumberFormat;
const phoneUtil = phone.PhoneNumberUtil.getInstance();
const NullablePhoneField = ({ record, source, ...props }: any) => {
  const value = get(record, source);
  let sub;
  if (value) {
    const number = phoneUtil.parseAndKeepRawInput(value, "US");
    sub = { [source]: phoneUtil.format(number, PNF.NATIONAL) };
  } else {
    sub = { [source]: "----" };
  }
  return <TextField record={sub} {...props} source={source} />;
};
const NullableTextField = ({ record, source, ...props }: any) => {
  const value = get(record, source);
  const sub = value ? record : { [source]: "----" };
  return <TextField record={sub} {...props} source={source} />;
};
const NullableDateField = ({ record, source, ...props }: any) => {
  const value = get(record, source);
  const sub = value ? record : { [source]: "----" };
  return value ? <DateField {...props} record={sub} source={source} /> : <TextField source={source} {...props} record={sub} />;
};
const NullableNumberField = ({ record, source, ...props }: any) => {
  const value = get(record, source);
  const sub = value ? record : { [source]: "----" };
  return <NumberField record={sub} {...props} source={source} />;
};
const ChipField = ({ record, source, ...props }) => {
  const value = get(record, source);
  const label = value ? value : "----";
  return <Chip value={label} {...props} clickable />;
};
const ChipDateField = ({ record, source, ...props }) => {
  const value = get(record, source);
  const label = value ? new Date(value).toLocaleDateString() : "----";
  return <Chip {...props} value={label} />;
};
const ReportingDateChip = ({ record, source, clickable, ...props }: any) => {
  const count = get(record, "reporting_week_count");
  const _start = get(record, "reporting_week_start");
  const start = _start ? new Date(_start).toLocaleDateString() : "----";
  const _end = get(record, "reporting_week_end");
  const end = _end ? new Date(_end).toLocaleDateString() : "----";
  // console.log(props);
  return <Chip icon={count} clickable={clickable} value={<StartEndCount start={start} end={end} count={count} />} />;
};
const StartEndCount = enhanceDate(({ classes, start, end, count }: any) => (
  <span className={classes.root}>
    <Typography component="span" variant="headline" className={classes.count}>
      {count}
    </Typography>
    <span>
      <Typography component="span" variant="caption" className={classes.start}>
        {start}
      </Typography>
      <Typography component="span" variant="caption" className={classes.end}>
        {end}
      </Typography>
    </span>
  </span>
));
const RenderStatus = ({ a, b, classes, ...props }) => (
  <div className={classes.status}>
    <Typography variant="body1" className={classes[a]}>
      {Alias[a]}
    </Typography>
    <Typography variant="body1" className={classes[b]}>
      {Alias[b]}
    </Typography>
  </div>
);
const localeCurrency = { style: "currency", currency: "USD" };
const FieldMap = {
  nullCell: props => <TextField record={{ null: "" }} source="null" />,
  nullCellA: props => <TextField record={{ null: "" }} source="null" />,
  nullCellB: props => <TextField record={{ null: "" }} source="null" />,
  nullCellC: props => <TextField record={{ null: "" }} source="null" />,
  id: (props: any) => <NullableTextField {...props} source="id" />,
  cid: (props: any) => <NullableTextField {...props} source="cid" />,
  company_name: (props: any) => <NullableTextField {...props} source="company_name" />,
  address: (props: any) => <NullableTextField {...props} source="address" />,
  city: (props: any) => <NullableTextField {...props} source="city" />,
  state: (props: any) => <NullableTextField {...props} source="state" />,
  county: (props: any) => <NullableTextField {...props} source="county" />,
  phone: (props: any) => <NullablePhoneField {...props} source="phone" />,
  fax: (props: any) => <NullablePhoneField {...props} source="fax" />,
  contact_name: (props: any) => <NullableTextField {...props} source="contact_name" />,
  work_code: (props: any) => <FunctionField {...props} source="work_code" render={v => Alias[v.work_code]} />,
  association_code: (props: any) => <FunctionField {...props} source="association_code" render={v => Alias[v.association_code]} />,
  croup: (props: any) => <FunctionField {...props} source="croup" render={v => Alias[v.croup]} />,
  association: (props: any) => <BooleanField {...props} source="association" />,
  permanent: (props: any) => <BooleanField {...props} source="permanent" />,
  ifus: (props: any) => <BooleanField {...props} source="ifus" />,
  start_date: (props: any) => <NullableDateField {...props} source="start_date" />,
  outbus_date: (props: any) => <NullableDateField {...props} source="outbus_date" />,
  inactive_date: (props: any) => <NullableDateField {...props} source="inactive_date" />,
  chap11_date: (props: any) => <NullableDateField {...props} source="chap11_date" />,
  created_at: (props: any) => <NullableDateField {...props} source="created_at" />,
  updated_at: (props: any) => <NullableDateField {...props} source="updated_at" />,
  num_apprentice: (props: any) => <NumberField {...props} source="num_apprentice" />,
  num_mechanic: (props: any) => <NumberField {...props} source="num_mechanic" />,
  num_light_commercial: (props: any) => <NumberField {...props} source="num_light_commercial" />,
  num_total: (props: any) => <NumberField {...props} source="num_total" />,
  hours_apprentice: (props: any) => <NumberField {...props} source="hours_apprentice" />,
  hours_mechanic: (props: any) => <NumberField {...props} source="hours_mechanic" />,
  hours_light_commercial: (props: any) => <NumberField {...props} source="hours_light_commercial" />,
  hours_target: (props: any) => <NumberField {...props} source="hours_target" />,
  hours_total: (props: any) => <NumberField {...props} source="hours_total" />,
  pay_status: enhanceChip(({ classes, ...props }: any) => (
    <FunctionField {...props} className={classes[props.record.pay_status]} source="pay_status" render={v => Alias[v.pay_status]} />
  )),
  pay_method: enhanceChip(({ classes, ...props }: any) => (
    <FunctionField {...props} className={classes.Default} source="pay_method" render={v => Alias[v.pay_method]} />
  )),
  status: enhanceChip(({ classes, ...props }: any) => (
    <FunctionField {...props} className={classes[props.record.status]} source="status" render={v => Alias[v.status]} />
  )),
  calculation: enhanceChip(({ classes, ...props }: any) => (
    <FunctionField {...props} className={classes.Default} source="calculation" render={v => Alias[v.calculation]} />
  )),
  comment: (props: any) => <NullableTextField {...props} source="comment" />,
  reporting_week_count: (props: any) => <NumberField {...props} source="reporting_week_count" />,
  reporting_week: (props: any) => <NumberField {...props} source="reporting_week" />,
  reporting_year: (props: any) => <NumberField {...props} source="reporting_year" />,
  reporting_week_start: enhanceDate(({ classes, ...props }: any) => (
    <NullableDateField {...props} className={classes.start} source="reporting_week_start" />
  )),
  reporting_week_end: enhanceDate(({ classes, ...props }: any) => (
    <NullableDateField {...props} className={classes.end} source="reporting_week_end" />
  )),
  receive_date: (props: any) => <NullableDateField {...props} source="receive_date" />,
  deposit_date: (props: any) => <NullableDateField {...props} source="deposit_date" />,
  amount_total: enhanceMoney(({ classes, ...props }: any) => (
    <NumberField {...props} className={classes.AmountTotal} source="amount_total" options={localeCurrency} />
  )),
  amount_paid: enhanceMoney(({ classes, ...props }: any) => (
    <NumberField {...props} className={classes.AmountPaid} source="amount_paid" options={localeCurrency} />
  )),
  amount_owed: enhanceMoney(({ classes, ...props }: any) => (
    <NumberField {...props} className={getMoneyClass(classes, props.record.amount_owed)} source="amount_owed" options={localeCurrency} />
  )),
  rate_apprentice: (props: any) => <NumberField {...props} source="rate_apprentice" options={localeCurrency} />,
  rate_mechanic: (props: any) => <NumberField {...props} source="rate_mechanic" options={localeCurrency} />,
  rate_light_commercial: (props: any) => <NumberField {...props} source="rate_light_commercial" options={localeCurrency} />,
  amount_apprentice: enhanceMoney(({ classes, ...props }: any) => (
    <NumberField {...props} className={classes.Amount} source="amount_apprentice" options={localeCurrency} />
  )),
  amount_mechanic: enhanceMoney(({ classes, ...props }: any) => (
    <NumberField {...props} className={classes.Amount} source="amount_mechanic" options={localeCurrency} />
  )),
  amount_light_commercial: enhanceMoney(({ classes, ...props }: any) => (
    <NumberField {...props} className={classes.Amount} source="amount_light_commercial" options={localeCurrency} />
  )),
  zipcode: (props: any) => <FunctionField {...props} source="zipcode" render={record => record.zipcode.slice(0, 5)} />,
  company_id: props => <NullableTextField {...props} source="company_id" />,
  contribution_id: props => <NullableTextField {...props} source="contribution_id" />,
  payment_id: props => <NullableTextField {...props} source="payment_id" />,
  user_id: props => <NullableTextField {...props} source="user_id" />,
  title: (props: any) => <NullableTextField {...props} source="title" />,
  username: (props: any) => <NullableTextField {...props} source="username" />,
  full_name: (props: any) => <NullableTextField {...props} source="full_name" />,
  last_name: (props: any) => <NullableTextField {...props} source="last_name" />,
  first_name: (props: any) => <NullableTextField {...props} source="first_name" />,
  role: enhanceChip(({ classes, ...props }: any) => (
    <FunctionField {...props} className={classes.Default} source="role" render={v => Alias[Enum.Role[v.role]]} />
  )),
  email: (props: any) => <EmailField {...props} source="email" />,
  "User.email": (props: any) => <EmailField {...props} source="User.email" />,
  "User.title": (props: any) => <NullableTextField {...props} source="User.title" />,
  "User.full_name": (props: any) => <NullableTextField {...props} source="User.full_name" />,
  "User.phone": (props: any) => <NullablePhoneField {...props} source="User.phone" />,
  "User.first_name": (props: any) => (
    <ReferenceField {...props} source="user_id" linkType="show" reference="users">
      <TextField source="first_name" />
    </ReferenceField>
  ),
  "User.last_name": (props: any) => (
    <ReferenceField {...props} source="user_id" linkType="show" reference="users">
      <TextField source="last_name" />
    </ReferenceField>
  ),
  CreateButton: ({ addLabel, textAlign, ...props }) => <Button.CreateButton {...props} label={Alias[props.source]} />,
  ShowButton: ({ addLabel, textAlign, ...props }) => <Button.ShowButton {...props} label={Alias[props.source]} />,
  EditButton: ({ addLabel, textAlign, ...props }) => <Button.EditButton {...props} label={Alias[props.source]} />,
  ListButton: ({ addLabel, textAlign, ...props }) => <Button.ListButton {...props} label={Alias[props.source]} />,
  DeleteButton: ({ addLabel, textAlign, ...props }) => <Button.DeleteButton {...props} label={Alias[props.source]} />,
  RefreshButton: ({ addLabel, textAlign, ...props }) => <Button.RefreshButton {...props} label={Alias[props.source]} />,
  ExportButton: ({ addLabel, textAlign, ...props }) => <Button.ExportButton {...props} label={Alias[props.source]} />,
  SaveButton: ({ addLabel, textAlign, ...props }) => <Button.SaveButton {...props} label={Alias[props.source]} />,
  DashButton: ({ addLabel, textAlign, ...props }) => <Button.DashButton {...props} label={Alias[props.source]} />,
  DashPaymentPayButton: ({ addLabel, textAlign, ...props }) => <Button.DashPaymentPayButton {...props} label={Alias[props.source]} />,
  DashPaymentReportButton: ({ addLabel, textAlign, ...props }) => <Button.DashPaymentReportButton {...props} label={Alias[props.source]} />,
  DashPaymentAdjustButton: ({ addLabel, textAlign, ...props }) => <Button.DashPaymentAdjustButton {...props} label={Alias[props.source]} />,
  DashPaymentEditButton: ({ addLabel, textAlign, ...props }) => <Button.DashPaymentAdjustButton {...props} label="Edit" />,
  DashShowButton: ({ addLabel, textAlign, ...props }) => <Button.DashShowButton {...props} label={Alias[props.source]} />,
  DashEditButton: ({ addLabel, textAlign, ...props }) => <Button.DashEditButton {...props} label={Alias[props.source]} />,
  DashDeleteButton: ({ addLabel, textAlign, ...props }) => <Button.DashDeleteButton {...props} label={Alias[props.source]} />,
  DashPaymentUpdateButton: ({ addLabel, ...props }) => <Button.DashPaymentUpdateButton {...props} label={Alias[props.source]} />,
  DashPaymentPayOrNullButton: ({ addLabel, textAlign, ...props }) =>
    isShortOrDelinquent(props.record.status) ? <Button.DashPaymentPayButton {...props} label={Alias[props.source]} /> : null,
  DashPaymentUpdateOrNullButton: ({ addLabel, textAlign, ...props }) =>
    props.permissions >= Enum.Role.Admin ? <Button.DashPaymentUpdateButton {...props} label={Alias[props.source]} /> : null,
  PrintButton: ({ addLabel, textAlign, ...props }) => <Button.PrintButton {...props} label={Alias[props.source]} />,
  status__chip: enhanceChip(({ addLabel, textAlign, record = {}, ...props }: any) => (
    <RenderStatus a={record.status} b={record.pay_method} classes={props.classes} />
  )),
  pay_status__chip: enhanceChip(({ addLabel, textAlign, record = {}, ...props }: any) => (
    <RenderStatus a={record.pay_status} b={record.pay_method} classes={props.classes} />
  )),
  // OwnerIcon: props => <OwnerIcon color="secondary" record={props.record} />,
  payment_date: (props: any) => <NullableDateField {...props} source="payment_date" />,
  payment_date__chip: ({ label, source, ...props }: any) => <ChipDateField {...props} source="payment_date" />,
  "Company.company_name": ({ source, ...props }: any) => (
    <ReferenceField {...props} source="company_id" linkType="show" reference="companies">
      <TextField source="company_name" />
    </ReferenceField>
  ),
  payment_id__ref: (props: any) => (
    <ReferenceField {...props} source="payment_id" linkType="show" reference="payments">
      <TextField source="id" />
    </ReferenceField>
  ),
  cid__ref: (props: any) => (
    <ReferenceField {...props} source="cid" linkType="show" reference="contributions">
      <TextField source="cid" />
    </ReferenceField>
  ),
  user_id__ref: (props: any) => (
    <ReferenceField {...props} source="user_id" linkType="show" reference="users">
      <TextField source="id" />
    </ReferenceField>
  ),
  company_id__ref: (props: any) => (
    <ReferenceField {...props} source="company_id" linkType="show" reference="companies">
      <TextField source="id" />
    </ReferenceField>
  ),
  company_name__ref: (props: any) => (
    <ReferenceField {...props} source="company_id" linkType="show" reference="companies">
      <TextField source="company_name" />
    </ReferenceField>
  ),
  "Contribution.reporting_week_end": (props: any) => (
    <ReferenceField {...props} reference="contributions" source="contribution_id" linkType="show">
      <FieldMap.reporting_week_end />
    </ReferenceField>
  ),
  reporting_week_end__ref_chip: (props: any) => {
    return (
      <ReferenceField {...props} reference="contributions" source="contribution_id" linkType="show">
        <ReportingDateChip clickable />
      </ReferenceField>
    );
  },
  reporting_week_end__chip: (props: any) => {
    return <ReportingDateChip {...props} />;
  },
  full_name__ref_chip: ({ label, source, ...props }: any) => (
    <ReferenceField {...props} source="user_id" linkType="show" reference="users">
      <ChipField {...props} source="full_name" />
    </ReferenceField>
  ),
  full_name__ref_no_sort: ({ label, source, ...props }: any) => (
    <ReferenceField {...props} source="user_id" linkType="show" reference="users">
      <TextField {...props} source="full_name" />
    </ReferenceField>
  ),
  user_id__test: (props: any) => (
    <React.Fragment>
      <ReferenceField {...props} source="user_id" linkType="show" reference="users">
        <TextField source="full_name" />
      </ReferenceField>
      <ReferenceField {...props} source="user_id" linkType="show" reference="users">
        <TextField source="title" />
      </ReferenceField>
      <ReferenceField {...props} source="user_id" linkType="show" reference="users">
        <TextField source="email" />
      </ReferenceField>
      <ReferenceField {...props} source="user_id" linkType="show" reference="users">
        <TextField source="phone" />
      </ReferenceField>
    </React.Fragment>
  )
};
// const RenderChip = ({ source, record }: any) => <Chip label={record[source]} color="primary" />;
const getChips = (...attributes: FieldType[]) => record => attributes.map(e => ({ full_name: record[e] })).filter(e => e.full_name);
const getChipsSource = (...attributes: FieldType[]) => record =>
  attributes.map(e => ({ full_name: record[e] && e })).filter(e => e.full_name);
const getField = (source: FieldType) => {
  const Field = FieldMap[source];
  if (!Field) throw "INTERNAL ERROR: no field found for " + source;
  return Field;
};
const getTextAlign = (source: FieldType) => (numericFields.has(source) ? "right" : "left");
export const sourceField = (source: FieldType, addLabel: boolean = true) => {
  const Field = getField(source);
  const _source = parseSourceData(source);
  const align = getTextAlign(_source.value);
  const label = source.includes("Button") ? "" : Alias[_source.value];
  return <Field source={_source.value} label={label} addLabel={label && addLabel} textAlign={align} />;
};

export const sourceFieldFromMap = (source: FieldType, i: number, permissions?: Enum.Role) => {
  const Field = getField(source);
  const _source = parseSourceData(source);
  const align = getTextAlign(_source.value);
  const isButton = source.includes("Button");
  const label = isButton ? "" : Alias[_source.value];
  switch (_source.type) {
    case Source.Association:
      return <Field source={_source.value} label={label} textAlign={align} key={i} permissions={permissions} sortBy={_source.initial} />;
    case Source.Alternate:
      return (
        <Field
          source={_source.value}
          label={label}
          textAlign={align}
          key={i}
          permissions={permissions}
          sortable={!source.includes("no_sort")}
        />
      );
    default:
      return <Field source={_source.value} label={label} textAlign={align} key={i} permissions={permissions} />;
  }
};

enum Source {
  Field,
  Association = ".",
  Alternate = "__"
}
interface ISourceData {
  initial: FieldType;
  type: Source;
  value: FieldType;
  associations?: string[];
}
const parseSourceData = (initial: FieldType): ISourceData => {
  const alt = initial.indexOf("__");
  if (alt === -1) {
    const associations = initial.split(".");
    const value = associations.pop() as FieldType;
    const type = associations.length === 0 ? Source.Field : Source.Association;
    return { initial, associations, value, type };
  } else {
    const value = initial.slice(0, alt) as FieldType;
    const type = Source.Alternate;
    return { initial, type, value };
  }
};
