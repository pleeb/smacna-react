import React from "react";
import { connect } from "react-redux";
import { CardActions, withStyles, Button as B } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import * as choices from "./choices";
import {
  ISort,
  Enum,
  Input,
  Alias,
  Button,
  sourceFieldFromMap,
  sourceField,
  GridItem,
  GridContainer,
  DatagridMapper,
  ReferenceManyContainer,
  Actionbar,
  Field,
  Pagination,
  DialogWrapper
} from "../";

const Filters = ({ permissions, ...props }) => (
  <Field.Filter {...props}>
    <Input.StringSearchInput source="first_name" alwaysOn noOptions />
    <Input.StringSearchInput source="last_name" alwaysOn noOptions />
  </Field.Filter>
);
const Actions = ({
  bulkActions,
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
  total,
  permissions
}: any) => (
  <CardActions>
    {filters &&
      React.cloneElement(filters, {
        resource,
        showFilter,
        displayedFilters,
        filterValues,
        context: "button"
      })}
    <DialogWrapper
      title="User creation panel."
      button={
        <B color="primary" variant="text">
          <Add /> <span style={{ marginLeft: 10 }}>Create User</span>
        </B>
      }
    >
      A user creation panel will appear here.
    </DialogWrapper>
    <Button.ExportButton disabled={total === 0} resource={resource} sort={currentSort} filter={filterValues} exporter={exporter} />
    <Button.RefreshButton />
  </CardActions>
);
interface IRefMany {
  permissions: Enum.Role;
  // target: "contribution_id" | "user_id" | "company_id";
  label?: string;
}
const defaultSort: ISort = { field: "Company.company_name", order: "ASC" };
// sort={defaultSort}
export const ReferenceManyUser = ({ permissions, ...props }: IRefMany) => (
  <ReferenceManyContainer {...props} sort={defaultSort} reference="users" source="id" target="company_id">
    <DatagridMapper context={Enum.Context.Show} permissions={permissions} />
  </ReferenceManyContainer>
);
export const UserList = connect(
  ({ session: { role: permissions, company_id } }: any) => ({ role: permissions, company_id }),
  {}
)(({ company_id, permissions, ...props }: any) => {
  return (
    <Field.List
      {...props}
      sort={defaultSort}
      pagination={<Pagination />}
      filter={permissions === Enum.Role.User ? { company_id } : null}
      filters={<Filters permissions={permissions} />}
      actions={<Actions permissions={permissions} />}
      bulkActionButtons={false}
    >
      <DatagridMapper permissions={permissions} context={Enum.Context.List} />
    </Field.List>
  );
});
