// const React = require("react");
// const { renderToString } = require("react-dom/server");
import React from "react";
import { renderToString } from "react-dom/server";

const HSpacer = props => (
  <td
    {...props}
    width="14"
    style={{
      width: "14px"
    }}
  />
);
const ttableStyles = {
  borderCollapse: "collapse",
  msoTableRspace: "0pt",
  msoTableLspace: "0pt",
  fontSize: 0,
  display: "table"
} as any;
const tableStyles = {
  borderCollapse: "collapse",
  msoTableRspace: "0pt",
  msoTableLspace: "0pt",
  fontSize: 0,
  display: "table"
} as any;
// fontFamily: "Montserrat, Proxima Nova, arial, sans-serif, helvetica",
const ttdStyles = {
  fontFamily: "sans-serif, arial",
  fontSize: "10px",
  height: "20px",
  color: "#000",
  letterSpacing: "0.9px",
  lineHeight: "20px"
} as any;
const tdStyles = {
  fontFamily: "sans-serif, arial",
  fontSize: "10px",
  height: "15px",
  color: "#000",
  letterSpacing: "0.6px",
  lineHeight: "15px"
} as any;
const aStyles = {
  ...tdStyles,
  padding: "0px",
  margin: "0px",
  textDecoration: "none",
  color: "#000"
} as any;
const imgStyles = {
  display: "block",
  verticalAlign: "middle",
  // width: 64,
  // height: 64
} as any;
// mso-line-height-rule:exactly;
const people = [
  { name: "SARAH BERMAN", title: "PRESIDENT", cell: "917.880.5596", email: "SBERMAN@BERMANGRP.COM" },
  { name: "KATE HARRINGTON", title: "MANAGING DIRECTOR", cell: "413.358.5397", email: "KHARRINGTON@BERMANGRP.COM" },
  { name: "CHRISTINA HUANG", title: "GRAPHIC DESIGNER", email: "CHUANG@BERMANGRP.COM" },
  { name: "MIA JOHNSON", title: "ACCOUNT EXECUTIVE", email: "MJOHNSON@BERMANGRP.COM" },
  {
    name: <span>BONNIE O&apos;CALLAGHAN</span>,
    title: "DIRECTOR OF CLIENT ACCOUNTS",

    cell: "914.473.7269",
    email: "BOCALLAGHAN@BERMANGRP.COM"
  },
  { name: "CAROLINE SZUCH", title: "ACCOUNT EXECUTIVE", email: "CSZUCH@BERMANGRP.COM" },
  { name: "REGAN HECKL", title: "GRAPHIC DESIGNER", email: "RHECKL@BERMANGRP.COM" },
  { name: "SHARISSE GUISAO", title: "OPERATIONS MANAGER", email: "SGUISAO@BERMANGRP.COM" },
  { name: "BLAKE MEYER", title: "DIRECTOR OF ACCOUNTING AND HUMAN RESOURCES", email: "BMEYER@BERMANGRP.COM" },
  { name: "KAROLINA BRODKA", title: "ACCOUNT EXECUTIVE", email: "KBRODKA@BERMANGRP.COM" },
  { name: "SHERRI STAALESEN", title: "ACCOUNT EXECUTIVE", email: "SSTAALESEN@BERMANGRP.COM" },
  { name: "DANE WISHER", title: "EDITORIAL DIRECTOR", email: "DWISHER@BERMANGRP.COM" },
  { name: "MARISSA FLEISCHHACKER", title: "ACCOUNT EXECUTIVE", email: "MFLEISCHHACKER@BERMANGRP.COM" },
  { name: "EMILY NISSIM", title: "SENIOR ACCOUNT EXECUTIVE", email: "ENISSIM@BERMANGRP.COM" },
  { name: "LOGAN ELLIS", title: "ACCOUNT EXECUTIVE", email: "LELLIS@BERMANGRP.COM" },
  { name: "MICHELLE LAROCCA", title: "SENIOR GRAPHIC DESIGNER", email: "MLAROCCA@BERMANGRP.COM" },
  { name: "PETER LEE", title: "WEB DEVELOPER", email: "PLEE@BERMANGRP.com" },
  { name: "SAMANTHA KAYNE", title: "ACCOUNT EXECUTIVE", email: "SKAYNE@BERMANGRP.COM" },
  { name: "EMILY CARTY", title: "ACCOUNT EXECUTIVE", email: "ECARTY@BERMANGRP.COM" },
  { name: "KAITLIN KILIAN", title: "DIRECTOR OF CREATIVE SERVICES", email: "KKILIAN@BERMANGRP.COM" },
  { name: "KRISTEN CAPOZZI", title: "MARKETING ASSOCIATE", email: "KCAPOZZI@BERMANGRP.COM" },
  { name: "HANNAH FRIEDMAN", title: "MARKETING ASSOCIATE", email: "HFRIEDMAN@BERMANGRP.COM" },
  { name: "MARIA MULLER", title: "MARKETING ASSOCIATE", email: "MMULLER@BERMANGRP.COM" }
];
const TDWrap = props => <td {...props} />;
const Signature = ({ name, email, title, tel = "212.450.7300", cell = null, ...props }) => {
  return (
    <div style={{ width: "100%", userSelect: "all", cursor: "pointer" }}>
      <table cellPadding="0" cellSpacing="0" style={ttableStyles}>
        <tbody>
          <tr>
            <TDWrap valign="middle" style={{ /*width: 64, height: 64, minWidth: 64, minHeight: 64*/ }}>
              <img
                src="http://bermangrp.com/wp-content/uploads/2019/05/TBG_Mark_Sig.png"
                alt="The Berman Group"
                // width="64"
                // height="64"
                style={imgStyles}
              />
            </TDWrap>
            <TDWrap
              width="14"
              style={{
                width: "14px",
                // content: " "
              }}
            />
            <TDWrap>
              <table cellPadding="0" cellSpacing="0" style={tableStyles}>
                <tbody>
                  <tr>
                    <TDWrap valign="middle" style={tdStyles}>
                      <span style={ttdStyles}>
                        <strong>{name}</strong> {" // "}
                        {title.toUpperCase()}
                      </span>
                      <br />
                      <span style={tdStyles}>
                        THE BERMAN GROUP {" // "} {tel}
                        {cell ? ` // C ${cell}` : null}
                      </span>
                      <br />
                      <a href={`mailto:${email}`} target="_blank" style={aStyles}>
                        {email.toUpperCase()}
                      </a>
                      <br />
                      <a href="http://www.bermangrp.com/" target="_blank" style={aStyles}>
                        BERMANGRP.COM
                      </a>
                      {" // "}
                      <a href="https://www.instagram.com/thebermangrp/" target="_blank" style={aStyles}>
                        @THEBERMANGRP
                      </a>
                    </TDWrap>
                  </tr>
                </tbody>
              </table>
            </TDWrap>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export const Signatures = props => {
  return (
    <div style={{ position: "absolute", top: 0, left: 0, width: "100%", marginTop: 50 }}>
      <div style={{ fontFamily: "sans-serif", textAlign: "center" }}>
        <h2>
          Outlook email signature instructions: <br />
        </h2>
        <h3>
          <small style={{ color: "red" }}>
            <em>This must be set in your Outlook client. Gmail only users must to wait for an alternate design.</em>
          </small>
        </h3>
        <h4>1) Hover your mouse over your signature.</h4>
        <h4>2) Click and copy ( instructions will appear on mouse hover ).</h4>
        <h4>3) Simply paste into your Outlook signature creation section.</h4>
        <h4>
          4) Save and test it by sending an email to yourself and to <a href="mailto:plee@bermangrp.com">plee@bermangrp.com</a>. I will come
          by to troubleshoot if the signature doesn't appear to be correct.
        </h4>
      </div>
      <div style={{ width: 600, margin: "50px auto", position: "relative" }}>
        <style
          dangerouslySetInnerHTML={{
            __html: `
.button {
  display: flex;
  flex-direction: row;
  user-select: none;
  border: 2px solid lightsteelblue;
  border-radius: 5px;
  margin: 6px;
  user-select: none;
}
.button:hover { 
  border: 2px solid orangered;
}
.button::before, .button::after {
  content: "";
  padding: 6px;
  position: absolute;
  text-align: center;
  user-select: none;
  height: fit-content;
  display: none;
  border: 2px solid orangered;
  border-radius: 5px;
}
.button::before {
  width: 150px;
  left: -175px;
  content: "Click to select your signature html code.";
}
.button::after {
  width: 250px;
  right: -275px;
  content: "Then press the control + c copy shortcut on keyboard - or - right click on the selection and select copy.";
}
.button:hover::before, .button:hover::after {
  display: block;
}
`
          }}
        />
        {people.map((e, i) => {
          const signature = <Signature {...e} key={i} />;
          return (
            <div className="button" key={i}>
              {signature}
            </div>
          );
        })}
      </div>
    </div>
  );
};
console.log(renderToString(<Signatures />));
