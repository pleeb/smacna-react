import React, { Fragment, Component } from "react";
import { BaseRangeInput } from "./BaseRangeInput";
import { NumberInput } from "../";
import { Enum } from "./";

interface IProps {
  source: string;
  field?: string;
  defaultOperator?: Enum.NumberOperator;
  alwaysOn?: boolean;
  label?: string;
  adornStart?: string;
  adornEnd?: string;
}
export const NumberRangeInput = (props: IProps) => <BaseRangeInput {...props} InputComponent={NumberInput} />;
