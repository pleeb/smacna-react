import { fetchUtils } from "react-admin";
import simpleRest from "ra-data-simple-rest";
import simpleRestProvider from "ra-data-simple-rest";

import { API_URL } from "../actions/consts";

const httpClient = (url, options: RequestInit & { headers?: Headers } = {}) => {
  if (!options.headers) {
    options.headers = new Headers({ Accept: "application/json" });
  }
  const token = localStorage.getItem("token");
  if (token) options.headers.set("Authorization", `Bearer ${token}`);
  return fetchUtils.fetchJson(url, options);
};
// export const dataProvider = simpleRest(API_URL, httpClient) as (action, resource, params?) => any;
const rest = simpleRest(API_URL, httpClient);
export const dataProvider = (request, resource, params?) => {
  const defaults = {
    filter: {},
    pagination: { page: 1, perPage: 10 },
    sort: { field: "id", order: "DESC" }
  };
  return rest(request, resource, params ? { ...defaults, ...params } : defaults);
};
