import React from "react";
import { withStyles, FormLabel, Typography } from "@material-ui/core";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { Enum, Input } from "../../";

const styles = withStyles({
  root: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
    // backgroundColor: theme.palette.primary.main
  },
  label: { marginRight: "1em", fontWeight: "bold" }
}) as any;
const enhance = compose(
  styles,
  connect(({ form: { "record-form": { syncErrors = {} } }, ...state }: any) => ({
    invalid: Boolean(syncErrors.certified)
  }))
);
export const Certification = enhance(({ classes, invalid, ...props }: any) => (
  <FormLabel className={classes.root}>
    <Typography color={invalid ? "error" : "primary"} variant="subheading" className={classes.label}>
      Certification:
    </Typography>
    <Input.BooleanInput
      required
      defaultValue={false}
      source="certified"
      label="I certify that the information provided in this report is true and correct."
    />
  </FormLabel>
));
