import { ResourceType } from "../components";
export const PRINT_FETCH = "PRINT_FETCH";
export const PRINT_FETCH_SUCCESS = "PRINT_FETCH_SUCCESS";
export const PRINT_FETCH_ERROR = "PRINT_FETCH_ERROR";

interface IRecord {
  id: number;
}
export interface IPrintAction {
  type: typeof PRINT_FETCH | typeof PRINT_FETCH_SUCCESS | typeof PRINT_FETCH_ERROR;
  record: IRecord;
  resource: ResourceType;
}
export type PrintActionCreator = (record: IRecord, resource: ResourceType) => IPrintAction;

export const printRecord: PrintActionCreator = (record, resource) => {
  console.log("action print record", resource, record);
  return {
    type: PRINT_FETCH,
    record,
    resource
  };
};
export const printRecordSuccess = (record: IRecord, resource: ResourceType) => {
  console.log("action print record success", resource, record);
  return {
    type: PRINT_FETCH_SUCCESS,
    record,
    resource
  };
};
