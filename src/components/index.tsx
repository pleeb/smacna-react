import * as Enum from "../enums";

export type EnumType =
  | Enum.PayStatus
  | Enum.PayMethod
  | Enum.Calculation
  | Enum.Status
  | Enum.AssociationCode
  | Enum.Group
  | Enum.WorkCode
  | Enum.Anchor
  | Enum.Resource
  | Enum.Crud
  | Enum.Role
  | Enum.SelectionOperator
  | Enum.NumberOperator
  | Enum.StringOperator
  | Enum.Context;
export type ResourceType =
  | Enum.Resource.Companies
  | Enum.Resource.Contributions
  | Enum.Resource.Payments
  | Enum.Resource.Users
  | Enum.Resource.Reports
  | Enum.Resource.Rates
  | "companies"
  | "contributions"
  | "payments"
  | "users"
  | "reports"
  | "rates";
export type BasePathType =
  | Enum.BasePath.Dashboard
  | Enum.BasePath.Contributions
  | Enum.BasePath.Companies
  | Enum.BasePath.Payments
  | Enum.BasePath.Users
  | Enum.BasePath.Reports
  | Enum.BasePath.Rates
  | "/dashboard"
  | "/contributions"
  | "/companies"
  | "/payments"
  | "/users"
  | "/reports"
  | "/rates";
export type ContributionEnum = Enum.Calculation | Enum.Status;
export type PaymentEnum = Enum.PayStatus | Enum.PayMethod;
export type CompanyEnum = Enum.AssociationCode | Enum.WorkCode | Enum.Group;
export type Enumerable = ContributionEnum | PaymentEnum | CompanyEnum;
// fields

export type RecordFieldType =
  | "id"
  | "created_at"
  | "updated_at"
  | "nullCell"
  | "nullCellA"
  | "nullCellB"
  | "nullCellC"
  | "ShowButton"
  | "EditButton"
  | "ListButton"
  | "DeleteButton"
  | "DashPaymentEditButton"
  | "DashPaymentAdjustButton"
  | "DashPaymentUpdateButton"
  | "DashPaymentReportButton"
  | "DashPaymentPayButton"
  | "DashPaymentPayOrNullButton"
  | "DashDeleteButton"
  | "DashCreateButton"
  | "DashEditButton"
  | "DashShowButton"
  | "PrintButton";
export type ContributionFieldType =
  | RecordFieldType
  | "cid"
  | "company_id"
  | "reporting_week_start"
  | "reporting_week_end"
  | "reporting_week"
  | "reporting_week_count"
  | "reporting_year"
  | "rate_apprentice"
  | "rate_mechanic"
  | "rate_light_commercial"
  | "num_apprentice"
  | "num_mechanic"
  | "num_light_commercial"
  | "num_total"
  | "hours_apprentice"
  | "hours_mechanic"
  | "hours_light_commercial"
  | "hours_total"
  | "hours_target"
  | "amount_apprentice"
  | "amount_mechanic"
  | "amount_light_commercial"
  | "amount_total"
  | "amount_paid"
  | "amount_owed"
  | "calculation"
  | "status"
  | "pay_status"
  | "pay_method"
  | "receive_date"
  | "deposit_date"
  | "comment";
export type CompanyFieldType =
  | RecordFieldType
  | "company_name"
  | "address"
  | "city"
  | "state"
  | "zipcode"
  | "croup"
  | "county"
  | "phone"
  | "fax"
  | "contact_name"
  | "ifus"
  | "association"
  | "permanent"
  | "work_code"
  | "association_code"
  | "start_date"
  | "outbus_date"
  | "inactive_date"
  | "chap11_date";
export type PaymentFieldType =
  | RecordFieldType
  | "user_id"
  | "company_id"
  | "contribution_id"
  | "payment_date"
  | "reporting_week_end"
  | "reporting_week"
  | "reporting_week_count"
  | "reporting_year"
  | "amount_total"
  | "amount_paid"
  | "pay_status"
  | "pay_method"
  | "comment";
export type UserFieldType =
  | RecordFieldType
  | "company_id"
  | "first_name"
  | "last_name"
  | "username"
  | "role"
  | "full_name"
  | "title"
  | "email"
  | "phone";
export type FieldType =
  | ContributionFieldType
  | PaymentFieldType
  | CompanyFieldType
  | UserFieldType
  | ReportableFieldType
  | "User.title"
  | "User.email"
  | "User.full_name"
  | "User.username"
  | "User.phone"
  | "User.first_name"
  | "User.last_name"
  | "Company.company_name"
  | "Contribution.cid"
  | "Contribution.reporting_week_end"
  | "Contribution.id"
  | "Payment.id"
  | "Company.company_name"
  | "Company.id"
  | "payments_count"
  | "contributions_count"
  | "companies_count"
  | "Company.company_name"
  | "User.first_name"
  | "full_name__ref_chip"
  | "payment_id__ref"
  | "cid__ref"
  | "company_name__ref"
  | "Contribution.reporting_week_end"
  | "Contribution.reporting_week_end_chip"
  | "full_name__ref_chip"
  | "full_name__ref_no_sort"
  | "payment_date__chip";

// enumable fields are fields with known values that can be iterated against.
// these include enum or boolean datatype fields.
// enum datatype fields will iterate against the known enum values.
// boolean datatype fields will iterate against true or false.
export type ReportableFieldType = EnumerableFieldType | CompanyFieldType | NumericFieldType;
export type DateFieldType =
  | "reporting_week_end"
  | "receive_date"
  | "deposit_date"
  | "start_date"
  | "outbus_date"
  | "inactive_date"
  | "chap11_date"
  | "created_at"
  | "updated_at";
export type EnumerableFieldType =
  | "calculation"
  | "status"
  | "croup"
  | "work_code"
  | "association"
  | "permanent"
  | "association_code"
  | "ifus"
  | "payment_type"
  | "pay_status"
  | "pay_method";
export type QueryFieldType =
  | "$and"
  | "$or"
  | "$gt"
  | "$gte"
  | "$lt"
  | "$lte"
  | "$ne"
  | "$between"
  | "$notBetween"
  | "$in"
  | "$notIn"
  | "$like"
  | "$notLike";
export type SearchableFieldType =
  | "company_name"
  | "city"
  | "state"
  | "zipcode"
  | "phone"
  | "contact_name"
  | "remunerator_name"
  | "remunerator_email"
  | "remunerator_title";
export type NumericFieldType =
  | "num_apprentice"
  | "num_mechanic"
  | "num_light_commercial"
  | "num_total"
  | "hours_apprentice"
  | "hours_mechanic"
  | "hours_light_commercial"
  | "hours_total"
  | "hours_target"
  | "amount_apprentice"
  | "amount_mechanic"
  | "amount_light_commercial"
  | "amount_total"
  | "amount_paid"
  | "amount_owed"
  | "payments_count"
  | "contributions_count"
  | "companies_count";
export const enumerableFields = new Set<EnumerableFieldType>([
  "calculation",
  "status",
  "croup",
  "work_code",
  "association",
  "permanent",
  "association_code",
  "ifus",
  "payment_type",
  "pay_status",
  "pay_method"
]);
export const dateFields = new Set<DateFieldType>([
  "reporting_week_end",
  "receive_date",
  "deposit_date",
  "start_date",
  "outbus_date",
  "inactive_date",
  "chap11_date",
  "created_at",
  "updated_at"
]);
export const queryFields = new Set<QueryFieldType>([
  "$and",
  "$or",
  "$gt",
  "$gte",
  "$lt",
  "$lte",
  "$ne",
  "$between",
  "$notBetween",
  "$in",
  "$notIn",
  "$like",
  "$notLike"
]);
export const searchableFields = new Set<SearchableFieldType>([
  "company_name",
  "city",
  "state",
  "zipcode",
  "phone",
  "contact_name",
  "remunerator_name",
  "remunerator_email",
  "remunerator_title"
]);
export const numericFields = new Set<FieldType>([
  "chips" as any,
  "role",
  "rate_apprentice",
  "rate_mechanic",
  "rate_light_commercial",
  "num_apprentice",
  "num_mechanic",
  "num_light_commercial",
  "num_total",
  "hours_apprentice",
  "hours_mechanic",
  "hours_light_commercial",
  "hours_target",
  "hours_total",
  "reporting_week_count",
  "reporting_week",
  "reporting_year",
  "amount_apprentice",
  "amount_mechanic",
  "amount_light_commercial",
  "amount_total",
  "amount_paid",
  "amount_owed",
  "payments_count",
  "contributions_count",
  "companies_count",
  "pay_status",
  "DashPaymentPayOrNullButton",
  "DashPaymentAdjustButton",
  "DashPaymentUpdateButton",
  "DashPaymentEditButton",
  "status"
]);
export const integerFields = new Set<NumericFieldType>([
  "num_apprentice",
  "num_mechanic",
  "num_light_commercial",
  "num_total",
  "hours_apprentice",
  "hours_mechanic",
  "hours_light_commercial",
  "hours_total",
  "hours_target",
  "payments_count",
  "contributions_count",
  "companies_count"
]);
export const currencyFields = new Set<NumericFieldType>([
  "amount_apprentice",
  "amount_mechanic",
  "amount_light_commercial",
  "amount_total",
  "amount_paid",
  "amount_owed"
]);
export const enumerableHash = {
  ["association"]: [1, 0, null],
  ["permanent"]: [1, 0, null],
  ["ifus"]: [1, 0, null],
  ["association_code"]: ["NON_ASSOCIATION_NY", "NON_ASSOCIATION_LI", "ASSOCIATION_NY", "ASSOCIATION_LI", "OTHER", null],
  ["croup"]: ["IND"],
  ["work_code"]: ["VENTILATION", "SPECIALTY", "ROOFING", "MANUFACTURING", "TESTING_AND_BALANCING", null],
  ["calculation"]: ["ACTUAL", "ESTIMATE"],
  ["status"]: ["PAID", "DELINQUENT"],
  ["payment_type"]: ["SUBMISSION", "ADJUSTMENT", "PAYMENT"],
  ["pay_status"]: ["PENDING", "FAILED", "SUCCESS"],
  ["pay_method"]: ["MAIL", "ELECTRONIC"]
};

export * from "../utils";
import * as mui from "./mui";
export { Enum, Alias, Const } from "../";
export { OwnerIcon, ProfileIcon } from "./OwnerIcon";
import * as Button from "./buttons";
import * as Input from "./inputs";
import * as Field from "./fields";

export { Button, Input, Field, mui };
export * from "./Actionbar";
export * from "./Pagination";
export * from "./DashPanel";
export * from "./DialogWrapper";
export * from "./GridContainer";
export * from "./Permission";
export * from "./Chip";
export * from "./SimpleContainer";
