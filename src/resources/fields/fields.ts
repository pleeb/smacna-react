import compose from "recompose/compose";
import {
  ResourceType,
  BasePathType,
  Alias,
  Enum,
  FieldType,
  UserFieldType,
  ContributionFieldType,
  CompanyFieldType,
  PaymentFieldType,
  ReportableFieldType
} from "../";

const { Role, Resource, BasePath, Context } = Enum;

const filter = (fields: FieldType[]) => (e, i, a) => !fields.some(ee => ee.includes(e));

export const unref = (..._fields: FieldType[]) => (fields: FieldType[]) => fields.filter(filter(_fields));
export const unshift = (..._fields: FieldType[]) => (fields: FieldType[]) => fields.unshift(..._fields) && fields;
export const push = (..._fields: FieldType[]) => (fields: FieldType[]) => fields.push(..._fields) && fields;
export const splice = (..._fields: (FieldType | [FieldType, FieldType?])[]) => (fields: FieldType[]) => {
  // no mutate
  const clone = [...fields];

  for (let _field of _fields) {
    if (typeof _field === "string") {
      const index = clone.indexOf(_field);
      if (index !== -1) clone.splice(index, 1);
    } else {
      const [field, replace] = _field;
      const index = clone.indexOf(field);
      if (index !== -1) clone.splice(index, 1, replace);
    }
  }
  return clone;
};

export const contributionX = [
  "reporting_week_end__chip",
  "Company.company_name",
  "amount_total",
  "amount_paid",
  "amount_owed",
  "status__chip",
  "DashPaymentPayOrNullButton",
  "DashPaymentAdjustButton",
  "ShowButton"
] as ContributionFieldType[];
export const paymentXX = [
  "payment_date__chip",
  "full_name__ref_chip",
  "amount_total",
  "amount_paid",
  "nullCellA",
  "pay_status__chip",
  "nullCellB",
  "DashPaymentUpdateButton",
  "PrintButton"
] as PaymentFieldType[];
export const payment = [
  "payment_date__chip",
  "reporting_week_end__ref_chip",
  "Company.company_name",
  "full_name__ref_no_sort",
  "amount_total",
  "amount_paid",
  "pay_status__chip",
  "DashPaymentUpdateButton",
  "PrintButton"
] as PaymentFieldType[];
export const user = ["Company.company_name", "first_name", "last_name", "title", "email", "phone", "ShowButton"] as UserFieldType[];
export const company = [
  "company_name",
  "phone",
  "fax",
  "ifus",
  "association",
  "permanent",
  "start_date",
  "outbus_date",
  "inactive_date",
  "chap11_date",
  "ShowButton"
] as CompanyFieldType[];
