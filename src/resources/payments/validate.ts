import { Validator } from "redux-form";

const floor = new Date("1995/01/01");
const validateDate = (value, values, props, name) => {
  if (!values.amount_paid) return;
  const date = new Date(value);
  const today = new Date();
  let earliest, label;
  if (name === "deposit_date") {
    const receive = values["receive_date"];
    if (!receive || !value) return;
    earliest = new Date(values["receive_date"]);
    earliest.setUTCHours(0, 0, 0, 0);
  } else {
    earliest = floor;
  }
  label = earliest.toLocaleDateString();
  if (date > today) return "Date cannot be after today.";
  if (date < earliest) return `Date cannot be on or before ${label}.`;
};
export const receiveDate: Validator = (value, values, props, name) => {
  if (!values.amount_paid && value) return "Receive date cannot exist if no amount was paid.";
  if (values.amount_paid && !value) return "Receive date must be recorded if an amount was paid.";
  return validateDate(value, values, props, name);
};
export const depositDate: Validator = (value, values, props, name) => {
  if (!values.amount_paid && value) return "Deposit date cannot exist if no amount was paid.";
  return validateDate(value, values, props, name);
};
export const amountPaid: Validator = (value, values, props, name) => {
  if (value < 0) return "Amount paid cannot be negative.";
};
