import { React, connect, IState, SourceNum, TSourceRecord, component } from "../";
import { compose, change } from "./";
// import { NumberInput } from "react-admin";
import { Field } from "redux-form";
import { mui } from "../../../";
import { validationMap } from "./validation";

const { inputStyles, createOptions } = component;

const selectValue = e => e.target.select();
const toNum = (value, name?) => (isNaN(value) ? 0 : +value);
// const Field = _Field as any;
const NumberField = ({ name, input, label, type, meta: { touched, error, warning, dispatch }, ...props }) => {
  return (
    <mui.TextField
      type="number"
      fullWidth
      {...input}
      InputLabelProps={{ shrink: true }}
      inputProps={{
        min: 0,
        style: { textAlign: "right", width: "100%" },
        onFocus: selectValue
      }}
      InputProps={{ margin: "none" as any, disableUnderline: true, style: { marginTop: 0 }, classes: props.classes }}
      FormHelperTextProps={{ style: { display: "flex", flexDirection: "column" } }}
      label={label}
      error={Boolean(error)}
      helperText={error}
    />
  );
};
const ConnectedNumberInput = (({ source, classes, ...props }: any) => {
  return (
    <Field<any>
      component={NumberField}
      name={source}
      label=""
      type="number"
      validate={validationMap[source]}
      normalize={toNum}
      props={{ classes }}
    />
  );
}) as any;

export const InputNumWorkers = inputStyles(({ source, classes }: any | TSourceRecord<SourceNum>) => (
  <ConnectedNumberInput classes={classes} source={source} />
));
