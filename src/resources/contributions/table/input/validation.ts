import _ from "lodash";
import { Validator } from "redux-form";

const getFirstError = (rules: Validator[], value, values, props, name) => {
  let error;
  for (let rule of rules) if ((error = rule(value, values, props, name))) return error;
};

const noNegative = (value, values, props, name) => {
  if (+value < 0) return "Value may not be negative.";
  return undefined;
};
const isNumber = (value, values, props, name) => {
  if (isNaN(value)) return "Value must be a number.";
  return undefined;
};

const basic = [isNumber, noNegative];

type Source = "mechanic" | "apprentice" | "light_commercial";
type Type = "hours" | "num";

const toLabel = (source: Source) => source.split("_").join(" ");
// NUM WORKERS
const ff_numMatchesHours = (source: Source) => (value, values) => {
  const num = value;
  const hours = values["hours_" + source];
  if (!num && hours) return `Num ${toLabel(source)} cannot be zero with hours worked.`;
};
const mechanicNum = ff_numMatchesHours("mechanic");
const apprenticeNum = ff_numMatchesHours("apprentice");
const light_commercialNum = ff_numMatchesHours("light_commercial");
const num_mechanic = (value, values, props, name) => {
  const rules = [...basic, mechanicNum];
  return getFirstError(rules, value, values, props, name);
};
const num_apprentice = (value, values, props, name) => {
  const rules = [...basic, apprenticeNum];
  return getFirstError(rules, value, values, props, name);
};
const num_light_commercial = (value, values, props, name) => {
  const rules = [...basic, light_commercialNum];
  return getFirstError(rules, value, values, props, name);
};
// WORKER HOURS
const ff_hoursMatchesNum = (source: Source) => (value, values) => {
  const hours = value;
  const num = values["num_" + source];
  if (!hours && num) return `${_.capitalize(toLabel(source))} hours cannot be zero.`;
};
const mechanicHours = ff_hoursMatchesNum("mechanic");
const apprenticeHours = ff_hoursMatchesNum("apprentice");
const light_commercialHours = ff_hoursMatchesNum("light_commercial");
const hours_mechanic = (value, values, props, name) => {
  const rules = [...basic, mechanicHours];
  return getFirstError(rules, value, values, props, name);
};
const hours_apprentice = (value, values, props, name) => {
  const rules = [...basic, apprenticeHours];
  return getFirstError(rules, value, values, props, name);
};
const hours_light_commercial = (value, values, props, name) => {
  const rules = [...basic, light_commercialHours];
  return getFirstError(rules, value, values, props, name);
};
const targetHours = (value, values) => {
  if (value > values["hours_mechanic"]) return "Target hours may not exceed mechanic hours.";
};
const hours_target = (value, values, props, name) => {
  const rules = [...basic, targetHours];
  return getFirstError(rules, value, values, props, name);
};
export const validationMap = {
  num_mechanic,
  num_apprentice,
  num_light_commercial,
  hours_mechanic,
  hours_apprentice,
  hours_light_commercial,
  hours_target
};
