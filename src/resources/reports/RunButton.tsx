import React from "react";
import {
  // buttons
  CloneButton,
  CreateButton,
  DeleteButton,
  EditButton,
  FilterButton,
  ListButton,
  RefreshButton,
  SaveButton,
  ShowButton,
  ExportButton,
  // view guessers
  withDataProvider,
  CREATE
} from "react-admin";
import { Button, withStyles, LinearProgress, CircularProgress } from "@material-ui/core";
import RunIcon from "@material-ui/icons/Send";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import compose from "recompose/compose";

const enhance = compose(
  withStyles(theme => ({
    button: { margin: 0, marginLeft: theme.spacing.unit, position: "relative", minWidth: 64, minHeight: 36 },
    rightIcon: { marginLeft: theme.spacing.unit }
  })),
  connect(({ report: { fetching }, form: { "record-form": recordForm } }: any) => {
    const valid = recordForm.syncErrors ? Object.keys(recordForm.syncErrors).length === 0 : true;
    // console.log(recordForm);
    return {
      data: recordForm ? recordForm.values : {},
      fetching,
      valid
    };
  }),
  withDataProvider
);
class _RunButton extends React.Component<{ classes; dataProvider; data; fetching; valid }> {
  private fetchReport = () => {
    const { classes, dataProvider, data } = this.props;
    dataProvider(
      CREATE,
      "reports/query",
      { data },
      {
        onSuccess: {
          notification: {
            body: "Fetch report success.",
            level: "info"
          }
        },
        onError: {
          notification: {
            body: "Fetch report error.",
            level: "warning"
          }
        },
        onFailure: {
          notification: {
            body: "Fetch report failure.",
            level: "warning"
          }
        }
      }
    );
  };
  render() {
    const { classes, fetching, valid } = this.props;
    return (
      <Button color="primary" variant="contained" className={classes.button} onClick={this.fetchReport} disabled={fetching || !valid}>
        {fetching ? <CircularProgress size={24} variant="indeterminate" /> : "Run"}
        <RunIcon className={classes.rightIcon} />
      </Button>
    );
  }
}

export const RunButton = enhance(_RunButton);

