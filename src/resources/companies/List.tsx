import React from "react";
import { CardActions, withStyles, Button as B } from "@material-ui/core";
import * as choices from "./choices";
import { isMaster, DialogWrapper, ISort, Enum, Field, Input, Alias, Button, DatagridMapper, Pagination } from "../";
import { Add } from "@material-ui/icons";

const Actions = ({
  permissions,
  bulkActions,
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
  total
}: any) => (
  <CardActions>
    {filters &&
      React.cloneElement(filters, {
        resource,
        showFilter,
        displayedFilters,
        filterValues,
        context: "button"
      })}
    {isMaster(permissions) && (
      <DialogWrapper
        title="Company creation panel."
        button={
          <B color="primary" variant="text">
            <Add /> <span style={{ marginLeft: 10 }}>Create Company</span>
          </B>
        }
      >
        A company creation panel will appear here.
      </DialogWrapper>
    )}
    <Button.ExportButton disabled={total === 0} resource={resource} sort={currentSort} filter={filterValues} exporter={exporter} />
    <Button.RefreshButton />
  </CardActions>
);
const Filters = props => (
  <Field.Filter {...props}>
    <Input.StringSearchInput source="company_name" alwaysOn />
    <Input.DateRangeInput source="start_date" />
    <Input.StringSearchInput source="city" />
    <Input.StringSearchInput source="state" />
    <Input.StringSearchInput source="phone" />
    <Input.StringSearchInput source="contact_name" />
    <Input.StringSearchInput source="zipcode" />
    <Input.BooleanInput label={Alias["ifus"]} source="ifus" />
    <Input.BooleanInput label={Alias["permanent"]} source="permanent" />
    <Input.BooleanInput label={Alias["association"]} source="association" />
    <Input.SelectInput label={Alias["association_code"]} source="association_code" choices={choices.associationCode} />
    <Input.SelectInput label={Alias["work_code"]} source="work_code" choices={choices.workCode} />
  </Field.Filter>
);
const defaultSort: ISort = { field: "company_name", order: "ASC" };
// sort={defaultSort}
export const CompanyList = ({ role, company_id, permissions, ...props }: any) => {
  return (
    <Field.List
      {...props}
      pagination={<Pagination />}
      sort={defaultSort}
      filters={<Filters permissions={permissions} />}
      actions={<Actions permissions={permissions} />}
      bulkActionButtons={false}
    >
      <DatagridMapper permissions={permissions} context={Enum.Context.List} />
    </Field.List>
  );
};
