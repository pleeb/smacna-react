import * as React from "react";
import { withStyles } from "@material-ui/core";
import { Enum } from "../../";
export { Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@material-ui/core";

export const Mdash = props => <span style={{ marginRight: 5, marginLeft: 5 }}>&mdash;</span>;

export const inputStyles = withStyles(theme => ({
  root: {
    padding: 0,
    "label + &": {
      marginTop: theme.spacing.unit * 3
    }
  },
  input: {
    borderRadius: 4,
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "4px 8px",
    // width: "calc(100% - 24px)",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.25rem rgba(0,123,255,0.3)!important"
    },
    "&:hover": {
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.1rem rgba(0,123,255,0.3)"
    }
  },
  error: {
    color: theme.palette.error.main
  }
}));
const selectValue = e => e.target.select();
const isInvalid = e => e.target.value === "" || isNaN(e.target.value);
export const createOptions = (source, change, classes, syncErrors = {}) => {
  const error = syncErrors[source];
  return {
    inputProps: {
      min: 0,
      style: { textAlign: "right", width: "100%" },
      onBlur: e => isInvalid(e) && change("record-form", source, 0),
      onFocus: selectValue
    },
    label: null,
    error: Boolean(error),
    helperText: error,
    InputProps: {
      margin: "none",
      disableUnderline: true,
      classes,
      style: { marginTop: 0 }
    }
  };
};
export const tableStyles = withStyles(({ palette: { primary } }) => ({
  card: {
    marginBottom: 14,
    marginLeft: "auto",
    marginRight: "auto",
    maxWidth: 700
  },
  table: {
    tableLayout: "fixed"
  },
  head: {
    backgroundColor: primary.dark
  },
  headCell: {
    color: primary.contrastText,
    fontWeight: "bold"
  },
  amountRow: {
    backgroundColor: "lemonchiffon"
  },
  amountCell: {
    fontWeight: "bold",
    fontSize: 14,
    color: Enum.ColorMap.Money.Amount
  },
  amountTotalRow: {
    backgroundColor: "moccasin"
  },
  totalCell: {
    fontWeight: "bold",
    fontSize: 15,
    color: Enum.ColorMap.Money.AmountTotal
  }
}));
