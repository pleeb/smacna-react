import React from "react";
import { Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@material-ui/core";
import compose from "recompose/compose";
import { NumberInput } from "react-admin";
import { connect } from "react-redux";
import { change } from "redux-form";
import { withStyles } from "@material-ui/core";
import { Enum } from "../../";

const Mdash = props => <span style={{ marginRight: 5, marginLeft: 5 }}>&mdash;</span>;

const inputStyles = withStyles(theme => ({
  root: {
    padding: 0,
    "label + &": {
      marginTop: theme.spacing.unit * 3
    }
  },
  input: {
    borderRadius: 4,
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "4px 8px",
    // width: "calc(100% - 24px)",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.25rem rgba(0,123,255,0.3)!important"
    },
    "&:hover": {
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.1rem rgba(0,123,255,0.3)"
    }
  },
  error: {
    color: theme.palette.error.main
  }
}));
const connection = connect(
  (
    {
      form: {
        "record-form": { values = {} }
      }
    }: any,
    { source }: any
  ) => {
    return { value: values[source] || 0 };
  },
  { change }
);
const selectValue = e => e.target.select();
const isInvalid = e => e.target.value === "" || isNaN(e.target.value);
const createOptions = (source, change, classes, syncErrors = {}) => {
  const error = syncErrors[source];
  return {
    inputProps: {
      min: 0,
      style: { textAlign: "right" },
      onBlur: e => isInvalid(e) && change("record-form", source, 0),
      onFocus: selectValue
    },
    label: null,
    error: Boolean(error),
    helperText: error,
    InputProps: {
      margin: "none",
      disableUnderline: true,
      classes,
      style: { marginTop: 0 }
    }
  };
};
export const tableStyles = withStyles(({ palette: { primary } }) => ({
  //
}));
export const InputMoney = compose(
  connection,
  inputStyles
)(({ source, value, change, classes }: any) => (
  <TextField
    type="number"
    value={value}
    InputProps={{
      style: { textAlign: "right" },
      onChange: e => change("record-form", source, e.target.value),
      onBlur: e => isInvalid(e) && change("record-form", source, 0),
      onFocus: selectValue,
      disableUnderline: true,
      classes
    }}
  />
));
