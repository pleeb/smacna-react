import React from "react";
import Icon from "@material-ui/icons/MonetizationOn";
import { DashButton, Enum } from "./";

export const DashPaymentUpdateButton = props => (
    <DashButton
        color="primary"
        Icon={Icon}
        crud={Enum.Crud.Update}
        label="Update"
        record={props.record}
        resource={Enum.Resource.Payments}
        variant="outlined"
    />
);
