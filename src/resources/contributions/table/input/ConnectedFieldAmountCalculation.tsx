import { React, connect, IState } from "../";
import { FieldAmountCalculation, ComponentSignature } from "../readonly/FieldAmountCalculation";

export const ConnectedFieldAmountCalculation = connect<any, any, ComponentSignature>(
  ({
    form: {
      "record-form": { values: record = {} }
    }
  }: IState) => {
    return { record };
  }
)(FieldAmountCalculation);
