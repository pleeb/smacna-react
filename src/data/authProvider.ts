import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, AUTH_GET_PERMISSIONS } from "react-admin";

import { dataProvider } from "./";

import { API_URL } from "../actions/consts";
import { Enum } from "../";

// import decodeJwt from 'jwt-decode';

export const authProvider = async (type, params) => {
  try {
    if (type === AUTH_LOGIN) {
      let { username, password } = params;

      const request = new Request(API_URL + "/signin", {
        method: "POST",
        body: JSON.stringify({ username, password }),
        headers: new Headers({ "Content-Type": "application/json" })
      });
      const response = await fetch(request);
      if (response.status < 200 || response.status >= 300) {
        throw new Error(response.statusText);
      }
      const user = await response.json();
      // console.log("user login success", user);
      const { role, id, token, company_id, accessToken, company_name } = user;
      setLocalAuth({
        ...user,
        id,
        role,
        token: token || "token placeholder",
        username,
        company_id,
        accessToken: accessToken || "access token placeholder",
        company_name
      });
      return user;
    }
    if (type === AUTH_LOGOUT) {
      setLocalAuth();
      return;
    }
    if (type === AUTH_ERROR) {
      const status = params.status;
      if (status === 401 || status === 403) {
        setLocalAuth();
        throw "auth error";
      }
      return params;
    }
    if (type === AUTH_CHECK) {
      if (!localStorage.getItem("localAuth.token")) throw "auth check failed";
      const { record, resource, crud, user_id, company_id, role } = params;
      return;
    }
    if (type === AUTH_GET_PERMISSIONS) {
      const role = localStorage.getItem("localAuth.role");
      if (!role) throw "auth get permission failed: " + role;
      return +role;
    }
    throw "Unknown method";
  } catch (err) {
    throw err;
  }
};
interface IAuthParams {
  id: number;
  username: string;
  company_name: string;
  role: Enum.Role;
  user?: any;
  token: string;
  accessToken: string;
  company_id: number;
  Company?: any;
}
export const setLocalAuth = (user?: IAuthParams): IAuthParams | undefined => {
  if (user) {
    const { id, company_id, company_name, role, username, token, accessToken } = user;
    switch (user.role) {
      case Enum.Role.User:
      case Enum.Role.Admin:
      case Enum.Role.Master:
        localStorage.setItem("localAuth.user", JSON.stringify(user || null));
        // localStorage.setItem("localAuth.company", JSON.stringify(user.Company || null));
        localStorage.setItem("localAuth.id", String(id));
        localStorage.setItem("localAuth.role", String(role));
        localStorage.setItem("localAuth.token", token);
        localStorage.setItem("localAuth.accessToken", accessToken);
        localStorage.setItem("localAuth.username", username);
        localStorage.setItem("localAuth.company_id", String(company_id));
        break;
      default:
        clearLocalAuth();
        throw `invalid user.role ${user.role} for auth.setLocalAuth`;
    }
  } else {
    clearLocalAuth();
  }
  return user;
};
export const getLocalAuth = () => {
  try {
    return {
      user: JSON.parse(localStorage.getItem("localAuth.user") || null),
      company: JSON.parse(localStorage.getItem("localAuth.company") || null),
      id: +localStorage.getItem("localAuth.id"),
      role: +localStorage.getItem("localAuth.role"),
      token: localStorage.getItem("localAuth.token"),
      accessToken: localStorage.getItem("localAuth.accessToken"),
      username: localStorage.getItem("localAuth.username"),
      company_id: +localStorage.getItem("localAuth.company_id"),
      company_name: localStorage.getItem("localAuth.company_name")
    };
  } catch (err) {
    console.log(err, localStorage.getItem("localAuth.company"), localStorage.getItem("localAuth.user"));
  }
};
const clearLocalAuth = () => {
  localStorage.removeItem("localAuth.user");
  localStorage.removeItem("localAuth.company");
  localStorage.removeItem("localAuth.id");
  localStorage.removeItem("localAuth.role");
  localStorage.removeItem("localAuth.token");
  localStorage.removeItem("localAuth.accessToken");
  localStorage.removeItem("localAuth.username");
  localStorage.removeItem("localAuth.company_id");
  localStorage.removeItem("localAuth.company_name");
};
