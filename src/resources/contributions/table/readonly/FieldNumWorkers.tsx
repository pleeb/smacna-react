import { React, parse, TSourceRecord, SourceNum } from "../";

export const FieldNumWorkers = ({ record, source }: TSourceRecord<SourceNum>) => <span>{parse.number(record[source])}</span>;
