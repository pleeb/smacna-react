import React from "react";
import { withRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { Chip, CardActions, withStyles } from "@material-ui/core";
import Run from "@material-ui/icons/ChevronRight";
import * as choices from "./choices";
import * as company from "../companies/choices";
import * as contribution from "../contributions/choices";
import { ReportView } from "./report";
import { Input, Field, sourceField, Alias, Button } from "../";

const Actions = props => {
  return (
    <CardActions>
      <Button.CreateButton resource="reports" basePath="/reports/query" />
    </CardActions>
  );
};
const Tools = props => {
  return (
    <Input.Toolbar {...props}>
      <Button.SaveButton label="Run" icon={<Run />} />
    </Input.Toolbar>
  );
};
const Form = props => {
  let showCompany = false;
  // console.log(props);
  return (
    <Field.Show
      // aside={<ReportView />}
      style={{ flexDirection: "column" }}
      {...props}
      // actions={<Actions permissions={permissions} />}
    >
      <Field.TabbedShowLayout
      // defaultValue={query || defaults}
      // toolbar={<Tools />}
      // redirect={false}2
      >
        <Field.Tab label="run">
          <Field.TextField source="companies.state" label="State" />
          <Field.DateField source="companies.start_date" />
        </Field.Tab>
        <Field.Tab label="date">
          <Field.DateField source="contributions.reporting_week_end" />
          <Field.DateField source="contributions.receive_date" />
          <Field.DateField source="contributions.deposit_date" />
          <Field.DateField source="companies.outbus_date" />
          <Field.DateField source="companies.inactive_date" />
          <Field.DateField source="companies.chap11_date" />
        </Field.Tab>
        <Field.Tab label="companies">
          <Field.BooleanField label={Alias["ifus"]} source="companies.ifus" />
          <Field.BooleanField label={Alias["permanent"]} source="companies.permanent" />
          <Field.BooleanField label={Alias["association"]} source="companies.association" />
        </Field.Tab>
        <Field.Tab label="attributes">
          <Tests source="groups" />
        </Field.Tab>
      </Field.TabbedShowLayout>
    </Field.Show>
  );
};
const Tests = ({ source, record }: any) => {
  return (
    <React.Fragment>
      {record[source].map((e, i) => (
        <Chip
          key={i}
          // avatar={avatar}
          label={e}
          // onDelete={this.handleDelete(data)}
        />
      ))}
    </React.Fragment>
  );
};
const enhance = compose(
  withRouter,
  withStyles({
    card: {
      overflow: "visible"
    }
  }),
  connect(
    (state: any) => {
      const form = state.form["record-form"];
      const query = form ? form.values : null;
      return {
        query
      };
    },
    {}
  )
);
export const ReportShow = enhance(Form);
