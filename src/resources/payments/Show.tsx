import React from "react";
import { ReadonlyTable } from "../contributions/table";
import { DashPanel, Field, Alias, Enum, Button, sourceField, GridItem, GridContainer, SimpleShowContainer, Actionbar } from "../";
const Actions = ({ permissions, ...props }) => (
  <Actionbar verb="View Payment" title={(props.data || {})["cid"]} {...props}>
    {permissions >= Enum.Role.Admin ? <Button.DashPaymentUpdateButton /> : null}
    <Button.ListButton />
    <Button.PrintButton />
  </Actionbar>
);
const Dash = ({ permissions, ...props }: any) => {
  return (
    <Field.Show basePath="/payments" resource="payments" id={0} {...props} actions={<Actions dash permissions={permissions} />}>
      <GridContainer withMargin>
        <GridItem xs={5}>
          <ReadonlyTable />
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("Company.company_name")}
            {sourceField("company_id")}
            {sourceField("contribution_id")}
            {sourceField("cid")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("reporting_week_end")}
            {sourceField("reporting_week")}
            {sourceField("reporting_week_count")}
            {sourceField("created_at")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("amount_total")}
            {sourceField("amount_paid")}
            {sourceField("pay_status")}
            {sourceField("pay_method")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("user_id")}
            {sourceField("User.title")}
            {sourceField("User.email")}
            {sourceField("User.phone")}
          </SimpleShowContainer>
        </GridItem>
        {permissions >= Enum.Role.Admin && (
          <GridItem xs={true}>
            <SimpleShowContainer>
              {sourceField("receive_date")}
              {sourceField("deposit_date")}
              {sourceField("updated_at")}
              {sourceField("comment")}
            </SimpleShowContainer>
          </GridItem>
        )}
      </GridContainer>
    </Field.Show>
  );
};
export const PaymentDashShow = (props: any) => {
  return (
    <DashPanel resource={Enum.Resource.Payments} anchor={Enum.Anchor.Bottom} crud={Enum.Crud.Read} {...props}>
      <Dash />
    </DashPanel>
  );
};
