I certify that the information contained in this report is true and correct.

Please return this shape for `GET /payments/:contribution_id?`

```
export interface IRecord {
  payment_id: number;
  contribution_id: number;
  company_id: number;
  parent_payment_id: number;

  payment_created: Date;

  // NUM_{WORKER}
  num_apprentice?: number;
  num_mechanic?: number;
  num_light_commercial?: number;
  /*
    diff_num_{worker} is the difference between the parent_payment and child payment num_{worker} values:
    if (parent_payment_id) {
      diff_num_{worker} = payments[parent_payment_id][num_{worker}] - payments[payment_id][num_{worker}]
    }
  */
  diff_num_apprentice?: number;
  diff_num_mechanic?: number;
  diff_num_light_commercial?: number;

  // HOURS_{WORKER}
  hours_apprentice?: number;
  hours_mechanic?: number;
  hours_light_commercial?: number;
  /*
    diff_hours_{worker} is the difference between the parent_payment and child payment hours_{worker} values:
    if (parent_payment_id) {
      diff_hours_{worker} = payments[parent_payment_id][hours_{worker}] - payments[payment_id][hours_{worker}]
    }
  */
  diff_hours_apprentice?: number;
  diff_hours_mechanic?: number;
  diff_hours_light_commercial?: number;

  payment_type: "Submission" | "Adjustment" | "Payment";
  payment_status: "Pending" | "Success" | "Failed";
  payment_method: "Mail" | "Electronic";

  payment_due: number;
  payment_paid: number;
  payment_received: number;

  remunerator_name: string;
  remunerator_email: string;
  remunerator_title: string;

  payment_comment?: string;
}
```

Please return this shape for `GET /payments/:contribution_id/items/:payment_id?`

```
export interface IRecord {
  contribution_id: number;
  company_id: number;

  num_apprentice: number;
  num_mechanic: number;
  num_light_commercial: number;

  hours_apprentice: number;
  hours_mechanic: number;
  hours_light_commercial: number;

  dues_apprentice: number;
  dues_mechanic: number;
  dues_light_commercial: number;

  rate_apprentice: number;
  rate_mechanic: number;
  rate_light_commercial: number;

  hours_total: number;

  contribution_due: number;
  contribution_received: number;
  contribution_owed: number;

  week_start: Date;
  week_end: Date;
  num_weeks: number;
  week_num: number;

  contribution_calculation: "User" | "Estimate";
  contribution_status: "Paid" | "Delinquent";
  contribution_comment?: string;

  date_received?: Date;
  date_deposited?: Date;
}
```

```
/**
 * @Entity @Table(name="company")
 **/
class Company extends Model {

    /** @Id @GeneratedValue @Column(type="integer") **/
    protected $id;

    /**
     * @Column(type="string")
     * @Column(name="name")
     **/
    protected $name;

    /**
     * @Column(type="string")
     * @Column(name="address")
     **/
    protected $address;

    /**
     * @Column(type="string")
     * @Column(name="city")
     **/
    protected $city;

    /**
     * @Column(type="string")
     * @Column(name="state")
     **/
    protected $state;

    /**
     * @Column(type="string")
     * @Column(name="zipcode")
     **/
    protected $zipcode;

    /**
     * @Column(type="string")
     * @Column(name="croup")
     **/
    protected $croup;

    /**
     * @Column(type="string")
     * @Column(name="county")
     **/
    protected $county;

    /**
     * @Column(type="string")
     * @Column(name="voicephone")
     **/
    protected $voicephone;

    /**
     * @Column(type="string")
     * @Column(name="fax")
     **/
    protected $fax;

    /**
     * @Column(type="string")
     * @Column(name="workcode")
     **/
    protected $workcode;

    /**
     * @Column(type="string")
     * @Column(name="assoc")
     **/
    protected $assoc;

    /**
     * @Column(type="string")
     * @Column(name="permanent")
     **/
    protected $permanent;

    /**
     * @Column(type="datetime")
     * @Column(name="start_date")
     **/
    protected $start_date;

    /**
     * @Column(type="datetime")
     * @Column(name="outbus_date")
     **/
    protected $outbus_date;

    /**
     * @Column(type="datetime")
     * @Column(name="inactive_date")
     **/
    protected $inactive_date;

    /**
     * @Column(type="string")
     * @Column(name="asscode")
     **/
    protected $asscode;

    /**
     * @Column(type="string")
     * @Column(name="contact")
     **/
    protected $contact;

    /**
     * @Column(type="boolean")
     * @Column(name="ifus")
     **/
    protected $ifus;

    /**
     * @Column(type="datetime")
     * @Column(name="chap11")
     **/
    protected $chap11;
}

```

```
<COMPANY>
<COMPNUM>953</COMPNUM>
<CONAME>A. Munder</CONAME>
<ADDR1>28-10 38th Avenue</ADDR1>
<CITY>LIC</CITY>
<STATE>NY</STATE>
<ZIP>11101</ZIP>
<CROUP>IND</CROUP>
<COUNTY>QU</COUNTY>
<VOICEPHONE>7187293338</VOICEPHONE>
<FAX>7187293352</FAX>
<WORKCODE>S</WORKCODE>
<ASSOC>0</ASSOC>
<PERMANENT>1</PERMANENT>
<START>2007-05-01T00:00:00</START>
<OUTBUS>2010-12-15T00:00:00</OUTBUS>
<INACTDATE>2010-11-01T00:00:00</INACTDATE>
<ASSCODE>AN</ASSCODE>
</COMPANY>
```

```
COMPNUM > company_id: int; autoincrement
CONAME > company_name: varchar(255); not null
ADDR1 > address: varchar(255); not null
CITY > city: varchar(255); not null
STATE > state: varchar(255); not null
ZIP > zipcode: int; not null
CROUP > croup: varchar(255); not null
COUNTY > county: varchar(255); not null
CONTACT > contact_name: varchar(255) null
VOICEPHONE > voicephone: int; null
FAX > fax: int; null
WORKCODE > workcode: varchar(255); null
ASSOC > association: boolean; not null
PERMANENT > permanent: boolean; not null
START > start_date: datetime; null
OUTBUS > outbus_date: datetime; null
INACTDATE > inactive_date: datetime; null
CHAP11 > chap11_date: datetime; null
ASSCODE > association_code: varchar(255); not null
IFUS > ifus: boolean; not null
```
