import React from "react";
import { Card, CardHeader, CardContent, CardActions, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import * as choices from "./choices";
import { required } from "../../components/inputs";
import { Enum, Input, Alias, GridContainer, GridItem, DashPanel } from "../";

const CreateAside = props => {
  return (
    <div style={{ width: "200px", margin: "1em" }}>
      <CardHeader title="Aside header" />
      <CardContent>aside content</CardContent>
    </div>
  );
};
const Dash = ({ ...props }) => (
  <Input.Create {...props}>
    <Input.SimpleForm redirect="show">
      <GridContainer>
        <GridItem xs={3}>
          <Input.TextInput fullWidth source="company_name" label={Alias["company_name"]} validate={required} required />
          <Input.TextInput fullWidth source="contact_name" label={Alias["contact_name"]} />
          <Input.TextInput fullWidth source="phone" label={Alias["phone"]} validate={required} required />
          <Input.TextInput fullWidth source="fax" label={Alias["fax"]} />
        </GridItem>
        <GridItem xs={3}>
          <Input.TextInput fullWidth source="address" label={Alias["address"]} />
          <Input.TextInput fullWidth source="city" label={Alias["city"]} />
          <Input.SelectInput fullWidth source="state" label={Alias["state"]} choices={choices.state} allowEmpty />
          <Input.TextInput fullWidth source="zipcode" label={Alias["zipcode"]} />
          <Input.TextInput fullWidth source="county" label={Alias["county"]} />
        </GridItem>
        <GridItem xs={3}>
          <Input.SelectInput fullWidth source="croup" label={Alias["croup"]} choices={choices.croup} allowEmpty />
          <Input.SelectInput
            fullWidth
            source="association_code"
            label={Alias["association_code"]}
            choices={choices.associationCode}
            allowEmpty
          />
          <Input.SelectInput fullWidth source="work_code" label={Alias["work_code"]} choices={choices.workCode} allowEmpty />
          <Input.BooleanInput fullWidth source="permanent" label={Alias["permanent"]} />
          <Input.BooleanInput fullWidth source="ifus" label={Alias["ifus"]} />
        </GridItem>
        <GridItem xs={3}>
          <Input.DateInput fullWidth source="start_date" label={Alias["start_date"]} />
          <Input.DateInput fullWidth source="outbus_date" label={Alias["outbus_date"]} />
          <Input.DateInput fullWidth source="inactive_date" label={Alias["inactive_date"]} />
          <Input.DateInput fullWidth source="chap11_date" label={Alias["chap11_date"]} />
        </GridItem>
      </GridContainer>
    </Input.SimpleForm>
  </Input.Create>
);

export const CompanyDashCreate = ({}) => {
  return (
    <DashPanel anchor={Enum.Anchor.Center} crud={Enum.Crud.Create} resource={Enum.Resource.Companies}>
      <Dash />
    </DashPanel>
  );
};
