import { React, component, Enum } from "../";
import { FieldAmountCalculation } from "./FieldAmountCalculation";
import { FieldAmountDue } from "./FieldAmountDue";
import { FieldNumHours } from "./FieldNumHours";
import { FieldNumWorkers } from "./FieldNumWorkers";

const { tableStyles, Mdash, Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow } = component;

export const ReadonlyTable = tableStyles(({ classes, record, resource, ...rest }: any) => {
  switch (resource) {
    case Enum.Resource.Contributions:
      record = record || {};
      break;
    case Enum.Resource.Payments:
      record = record.Contribution || {};
      break;
    default:
      console.log("WARNING, INVALID RESOURCE FOR READ ONLY TABLE", resource);
      return null;
  }
  return (
    <Card className={classes.card}>
      <Table padding="checkbox" className={classes.table}>
        <TableHead className={classes.head}>
          <TableRow>
            <TableCell>{null}</TableCell>
            <TableCell className={classes.headCell} numeric>
              <strong>Building Trade Mechanics</strong>
            </TableCell>
            <TableCell className={classes.headCell} numeric>
              <strong>Apprentice Workers</strong>
            </TableCell>
            <TableCell className={classes.headCell} numeric>
              <strong>Light Commercial Workers</strong>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>Number of Employees</TableCell>
            <TableCell numeric>
              <FieldNumWorkers record={record} source="num_mechanic" />
            </TableCell>
            <TableCell numeric>
              <FieldNumWorkers record={record} source="num_apprentice" />
            </TableCell>
            <TableCell numeric>
              <FieldNumWorkers record={record} source="num_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>+ Total Hours Paid</TableCell>
            <TableCell numeric>
              <FieldNumHours record={record} source="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <FieldNumHours record={record} source="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <FieldNumHours record={record} source="hours_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>&ndash; Target Hours</TableCell>
            <TableCell numeric>
              <FieldNumHours record={record} source="hours_target" />
            </TableCell>
            <TableCell numeric>
              <Mdash />
            </TableCell>
            <TableCell numeric>
              <Mdash />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Contribution Hour</TableCell>
            <TableCell numeric>
              <FieldAmountCalculation record={record} source="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <FieldAmountCalculation record={record} source="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <FieldAmountCalculation record={record} source="hours_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow className={classes.amountRow}>
            <TableCell>Contribution Due</TableCell>
            <TableCell numeric className={classes.amountCell}>
              <FieldAmountDue record={record} source="amount_mechanic" />
            </TableCell>
            <TableCell numeric className={classes.amountCell}>
              <FieldAmountDue record={record} source="amount_apprentice" />
            </TableCell>
            <TableCell numeric className={classes.amountCell}>
              <FieldAmountDue record={record} source="amount_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow className={classes.amountTotalRow}>
            <TableCell>
              <strong>Contribution Total</strong>
            </TableCell>
            <TableCell numeric className={classes.totalCell}>
              <FieldAmountDue record={record} source="amount_total" />
            </TableCell>
            <TableCell numeric />
            <TableCell numeric />
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  );
});
