import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import compose from "recompose/compose";
import { BasePathType, ResourceType, Field, Enum } from "../";
import { getFields } from "./getFields";
import { sourceField, sourceFieldFromMap } from "./sourceField";

import ExpansionDatagrid from "./ExpansionDatagrid";

interface IProps {
  // location?: BasePathType;
  basePath?: BasePathType;
  resource?: ResourceType;
  context?: Enum.Context;
  permissions: Enum.Role;
  rowStyle?: ({ id: number }) => any;
  children?: never;
  classes?: any;
  fixed?: boolean;
}
const connection = connect(
  ({
    router: {
      location: { pathname }
    }
  }: any) => ({
    location: pathname
      .split("/")
      .slice(0, 2)
      .join("/")
  }),
  {}
);
const styles = withStyles({
  table: {
    width: "100%",
    display: "inline-table"
  },
  tableFixed: {
    width: "100%",
    display: "inline-table",
    tableLayout: "fixed"
  }
});
export const DatagridMapper = compose(
  styles,
  connection
)(({ fixed, classes, ...props }: IProps) => {
  return (
    <ExpansionDatagrid {...props} classes={{ table: fixed ? classes.tableFixed : classes.table }}>
      {getFields(props).map((e, i) => sourceFieldFromMap(e, i, props.permissions))}
    </ExpansionDatagrid>
  );
}) as React.StatelessComponent<IProps>;

interface IPropsX extends IProps {
  expand: React.ReactNode;
}
const stylesX = withStyles({
  table: {
    width: "100%",
    tableLayout: "fixed",
    display: "inline-table"
  }
});
export const DatagridMapperX = compose(
  stylesX,
  connection
)(({ expand, fixed, ...props }: any) => {
  return (
    <ExpansionDatagrid {...props} rowClick="expand" expand={expand}>
      {getFields(props).map((e, i) => sourceFieldFromMap(e, i, props.permissions))}
    </ExpansionDatagrid>
  );
}) as React.StatelessComponent<IPropsX>;

interface IPropsXX extends IProps {}
const stylesXX = withStyles({
  table: {
    width: "100%",
    tableLayout: "fixed"
  }
});
export const DatagridMapperXX = compose(
  stylesXX,
  connection
)(({ fixed, ...props }: any) => {
  return (
    <ExpansionDatagrid {...props} expand={true}>
      {getFields(props).map((e, i) => sourceFieldFromMap(e, i, props.permissions))}
    </ExpansionDatagrid>
  );
}) as React.StatelessComponent<IPropsXX>;
