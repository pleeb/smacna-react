import React, { Fragment } from "react";
import { withRouter, Route, Redirect } from "react-router-dom";
import { DashboardComponent } from "./Dashboard";

export const routes = [<Route path="/dashboard" component={DashboardComponent} />];
