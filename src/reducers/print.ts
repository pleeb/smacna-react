import { PRINT_FETCH, PRINT_FETCH_ERROR, PRINT_FETCH_SUCCESS } from "../actions/print";

export function print(state = { fetching: false, record: null, resource: null }, { type, record, resource }) {
  switch (type) {
    case PRINT_FETCH:
      return { fetching: true, record, resource };
    case PRINT_FETCH_SUCCESS:
      return { fetching: false, record, resource };
    case PRINT_FETCH_ERROR:
      return { fetching: false, record: null, resource: null };
    default:
      return state;
  }
}
