import * as a from "../actions";

export function focus(state = { id: undefined }, { type, record }) {
  // console.log(type);
  switch (type) {
    case a.OPEN_DASH:
      const id = record ? record.id : 0;
      return { id };
    case "@@router/LOCATION_CHANGE":
    case a.CLOSE_DASH:
    case a.CLOSE_ALL_DASH:
      return { id: undefined };
    default:
      return state;
  }
}
