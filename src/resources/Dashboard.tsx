import React, { Fragment } from "react";
import { withRouter, Route, Redirect } from "react-router-dom";
import { push } from "react-router-redux";
import { connect } from "react-redux";
import { Title, refreshView } from "react-admin";
import compose from "recompose/compose";

import { withStyles, Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import { Field, SimpleShowContainer, ReferenceManyContainer, Enum, DatagridMapper, GridContainer, Button, Actionbar } from "./";
import { ReferenceManyContribution } from "./contributions";
import { ReferenceManyPayment, ReferenceManyPaymentX } from "./payments";
import { CONTENT_MAX_WIDTH as maxWidth, CONTENT_MIN_WIDTH as minWidth } from "../consts";

const { Tab, TabbedShowLayout } = Field;

const Wrap = ({
  permissions,
  classes,
  first_name,
  last_name,
  company_name,
  company_id,
  record,
  authParams,
  push,
  refreshView,
  ...rest
}) => {
  const isAdmin = permissions >= Enum.Role.Admin;
  const specific = isAdmin ? "All" : "Your";
  const title = `NYC SMACNA ${Enum.Role[permissions]} Portal`;
  const verb = permissions >= Enum.Role.Admin ? "Dashboard" : `Welcome ${Enum.Role[permissions]} ${first_name}`;
  const header = permissions >= Enum.Role.Admin ? "NYC SMACNA" : `${company_name}`;
  return (
    <Fragment>
      <CardContent>
        <Actions verb={verb} title={header} resource={Enum.Resource.Dashboard} permissions={permissions} />
        <div style={{ textAlign: "center", marginTop: 28 }}>
          {permissions === Enum.Role.User ? <Button.DashPaymentReportButton /> : null}
        </div>
      </CardContent>
      <Title title={title} />
      <SimpleShowContainer resource="contributions" basePath="/contributions" record={permissions >= Enum.Role.Admin ? {} : record}>
        <GridContainer>
          {isAdmin ? (
            <TabbedShowLayout>
              <Tab className={classes.tab} label={`${specific} Pending Payments`}>
                <ReferenceManyPayment permissions={permissions} filter={{ pay_status: Enum.PayStatus.PENDING }} target="company_id" />
              </Tab>
              <Tab className={classes.tab} label={`${specific} Delinquent Contributions`}>
                <ReferenceManyContribution
                  permissions={permissions}
                  filter={{ status: Enum.Status.DELINQUENT, pay_status: { $ne: Enum.Status.PENDING } }}
                />
              </Tab>
            </TabbedShowLayout>
          ) : (
            <TabbedShowLayout>
              <Tab className={classes.tab} label={`${specific} Delinquent Contributions`}>
                <ReferenceManyContribution
                  permissions={permissions}
                  filter={{ status: Enum.Status.DELINQUENT, pay_status: { $ne: Enum.Status.PENDING } }}
                  source="company_id"
                />
              </Tab>
              <Tab className={classes.tab} label={`${specific} Pending Payments`}>
                <ReferenceManyPayment
                  permissions={permissions}
                  filter={{ pay_status: Enum.PayStatus.PENDING }}
                  source="company_id"
                  target="company_id"
                />
              </Tab>
            </TabbedShowLayout>
          )}
        </GridContainer>
      </SimpleShowContainer>
    </Fragment>
  );
};

const Dash = ({
  permissions,
  classes,
  first_name,
  last_name,
  company_name,
  company_id,
  record,
  authParams,
  push,
  refreshView,
  staticContext,
  ...rest
}) => {
  const isAdmin = permissions >= Enum.Role.Admin;
  return (
    <Field.List
      className={classes.card}
      id={0}
      title=" "
      {...rest}
      pagination={null}
      resource="companies"
      basePath="/companies"
      record={permissions >= Enum.Role.Admin ? {} : record}
      filters={null}
      actions={null}
      hasList
      hasCreate
      hasEdit
      hasShow
      bulkActionButtons={false}
    >
      <Wrap
        permissions={permissions}
        classes={classes}
        first_name={first_name}
        last_name={last_name}
        company_name={company_name}
        company_id={company_id}
        record={record}
        authParams={authParams}
        push={push}
        refreshView={refreshView}
      />
    </Field.List>
  );
};

const Actions = props => <Actionbar {...props} />;
const styles = withStyles(theme => ({
  card: { maxWidth, minWidth },
  tab: {
    maxWidth: "none"
  }
}));
const connection = connect(
  ({ session: { id: user_id, role: permissions, company_id, company_name, first_name, last_name, Company, company }, ...state }: any) => {
    const record = Company || company || ({ company_id, company_name } as any);
    return {
      record,
      permissions,
      first_name,
      last_name,
      company_name: record.company_name,
      company_id
    };
  },
  { push, refreshView }
);
const enhance = compose(
  withRouter,
  styles,
  connection
);
export const DashboardComponent = enhance(Dash);
export const Dashboard = props => <Redirect to="/dashboard" />;
