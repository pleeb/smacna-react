import React, { Fragment } from "react";

import { connect } from "react-redux";
import { Typography } from "@material-ui/core";
import { change } from "redux-form";
import moment from "moment";

import { CalendarStyles, CalendarConfig, DayPicker, DateUtils, dataProvider, Enum } from "./";

const { Status } = Enum;

console.log("moment", moment);
interface IFromTo {
  readonly from: moment.Moment;
  readonly to: moment.Moment;
}
const mapFromTo = ({ reporting_week_end, reporting_week_count }, start, end) => {
  const to = moment(reporting_week_end);
  const map = {
    from: moment(reporting_week_end)
      .subtract(reporting_week_count, "weeks")
      .add(1, "day")
      .toDate(),
    to: to.toDate()
  };
  start.push(map.from);
  end.push(map.to);
  return map;
};
class _Calendar extends React.Component<any, any> {
  private date: CalendarConfig = new CalendarConfig(new Date("10/15/2013"));
  state = {
    pending: [],
    delinquent: [],
    paid: [],
    start: [],
    end: [],
    hovering: null,
    today: this.date.today.toDate(),
    from: null,
    to: null,
    enteredTo: null // Keep track of the last day for mouseEnter.
  };
  constructor(props) {
    super(props);
  }
  selectFrom(day: Date): void {
    if (this.date.selectable.to.isSame(this.date.getPeriodEndingDay(day), "date")) {
      this.selectTo(this.date.selectable.to.toDate(), this.date.getPeriodStartingDay(day).toDate());
    } else {
      const from = this.date.getPeriodStartingDay(day).toDate();
      const enteredTo = this.date.getPeriodEndingDay(day).toDate();
      this.updateForm(from);
      this.setState({
        from,
        to: null,
        enteredTo
      });
    }
  }
  selectTo(day: Date, from: Date): void {
    let to = this.date.getPeriodEndingDay(day).toDate();
    if (moment(from).isSame(to, "date")) to = this.date.getPeriodEndingDay(day, true).toDate();
    this.updateForm(from, to);
    this.setState({
      from,
      to,
      enteredTo: to
    });
  }
  selectEnteredTo(day: Date): void {
    this.setState({
      hovering: day,
      enteredTo: this.date.getPeriodEndingDay(day).toDate()
    });
  }
  handleDayClick = (day: Date): void => {
    if (!this.isSelectable(day)) return;
    const { from, to } = this.state;
    if (from && to && day >= from && day <= to) return this.handleResetClick();
    if (this.isSelectingFirstDay(from, to, day)) this.selectFrom(day);
    else this.selectTo(day, from);
  };
  handleDayMouseEnter = (day: Date): void => {
    const { from, to } = this.state;
    if (!this.isSelectable(day))
      return this.setState({
        hovering: null
      });
    if (!this.isSelectingFirstDay(from, to, day)) return this.selectEnteredTo(day);
    else
      return this.setState({
        hovering: day
      });
  };
  handleResetClick = (): void => {
    this.resetForm();
    this.setState({
      from: null,
      to: null,
      enteredTo: null
    });
  };
  changeForm(prop: ValidProp, value: any = ""): void {
    this.props.change("record-form", prop, value);
  }
  resetForm() {
    this.changeForm("cid");
    this.changeForm("reporting_week_start");
    this.changeForm("reporting_week_end");
    this.changeForm("reporting_week");
    this.changeForm("reporting_week_count");
  }
  updateForm(from: Date, to?: Date): void {
    if (from && to) {
      const reporting_week = moment(to).week();
      const days = Math.abs(moment(from).diff(to, "days", true));
      const weeks = Math.ceil(days / 7);
      const cid: string = `${this.props.company_id}${from.getUTCFullYear()}${reporting_week}`;
      this.changeForm("cid", cid);
      this.changeForm("reporting_week_start", from);
      this.changeForm("reporting_week_end", to);
      this.changeForm("reporting_week", reporting_week);
      this.changeForm("reporting_week_count", weeks);
      return;
    } else {
      this.resetForm();
      this.changeForm("reporting_week_start", from);
    }
  }
  isSelectingFirstDay(from, to, day): boolean {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }
  isSelectable(day: Date): boolean {
    if (!this.date.isSelectable(day)) return false;
    return !this.unselectables.some(e => moment(day).isBetween(e.from, e.to));
  }
  private unselectables: IFromTo[] = [];
  private lastSelectable: moment.Moment;
  addUnselectable({ reporting_week_end, reporting_week_count }: any) {
    if (this.isSelectable(reporting_week_end)) {
      const to = this.date.getPeriodEndingDay(reporting_week_end).add(1, "day");
      const from = to.clone().subtract(reporting_week_count, "weeks");
      this.unselectables.push({ from, to });
    }
  }
  // getLastSelectable(arr: IFromTo[]): moment.Moment {
  // switch (arr.length) {
  //   case 0:
  //     return this.date.selectable.to;
  //   case 1:
  //     return Math.max(arr.pop().to as any, this.date.selectable.to as any) as any;
  //   default:
  //     arr.sort((a: any, b: any) => b.from - a.from);
  //     return arr.find((e, i) => {
  //       const a = e,
  //         b = arr[i + 1];
  //       if (a && b && a.from.week() - b.to.week() > 0) return a;
  //     }) as any;
  //     break;
  // }
  // return;
  // }
  updateState(datas: any[]) {
    for (let data of datas) this.addUnselectable(data);
    // this.lastSelectable = this.getLastSelectable(this.unselectables);
    // console.log(this.lastSelectable);
    const [start, end] = [[], []];
    const delinquent = datas.filter(e => e.status === Status.DELINQUENT).map(e => mapFromTo(e, start, end));
    const paid = datas.filter(e => e.status === Status.PAID).map(e => mapFromTo(e, start, end));
    const pending = datas.filter(e => e.pay_status === Status.PENDING).map(e => mapFromTo(e, start, end));
    const next = {
      delinquent,
      paid,
      pending,
      start,
      end
    };
    this.setState(next);
  }
  async fetchContributions(from?: Date, to?: Date): Promise<any[]> {
    const { company_id, user_id } = this.props;
    const response = await dataProvider("GET_LIST", "contributions", {
      filter: {
        company_id,
        reporting_week_end: {
          $gte: from,
          $lt: to
        }
      },
      pagination: { page: 1, perPage: 52 }
    });
    return response.data;
  }
  async componentDidMount() {
    const from = this.date.traversable.date.from;
    const to = this.date.traversable.date.to;
    this.updateState(await this.fetchContributions(from, to));
  }
  render() {
    const { from, to, enteredTo, ...rest } = this.state;
    const modifiers = { selectStart: from, selectEnd: enteredTo, wednesday: { daysOfWeek: [3] }, ...rest };
    const disabledDays = { before: this.date.selectable.date.from, after: this.date.selectable.date.to };
    const selectedDays = [from, { from, to: enteredTo }];
    return (
      <Fragment>
        <div style={{ textAlign: "center" }}>
          <DayPicker
            showWeekNumbers
            className="payment-calendar"
            fromMonth={this.date.traversable.date.from}
            initialMonth={this.date.selectable.date.from}
            toMonth={this.date.traversable.date.to}
            numberOfMonths={4}
            enableOutsideDaysClick
            showOutsideDays
            modifiers={modifiers}
            disabledDays={disabledDays}
            selectedDays={selectedDays}
            onDayClick={this.handleDayClick}
            onDayMouseEnter={this.handleDayMouseEnter}
          />
          <CalendarStyles />
        </div>
      </Fragment>
    );
  }
}
export const Calendar = connect(
  ({ session: { id: user_id, company_id }, form }: any) => ({
    user_id,
    company_id
  }),
  { change }
)(_Calendar);
export { moment };
type ValidProp = "cid" | "reporting_week_start" | "reporting_week_end" | "reporting_week" | "reporting_week_count";
