import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import { InputTable } from "./table";
import { UserFields } from "../users/UserFields";
import { PaymentFields, Toolbar, validate } from "./form";
import { closeDash, openDash } from "../../actions";
import compose from "recompose/compose";
import {
  DashPanel,
  Enum,
  Input,
  Field,
  Alias,
  sourceField,
  GridItem,
  GridContainer,
  SimpleShowContainer,
  SimpleFormContainer,
  Actionbar,
  isMaster,
  dataProvider
} from "../";
import { refreshView } from "react-admin";

const Actions = ({ permissions, ...props }) => <Actionbar verb="Adjust Contribution" title={(props.data || {})["cid"]} {...props} />;
const enhance = compose(
  connect(
    ({ session: { role: permissions } }: any) => ({ permissions }),
    {}
  ),
  withStyles(theme => {
    return {
      xsHide: {
        [theme.breakpoints.down("sm")]: {
          display: "none!important"
        }
      },
      smHide: {
        [theme.breakpoints.down("md")]: {
          display: "none!important"
        }
      }
    };
  })
);
const Wrap = connect(
  null,
  (dispatch, { permissions, ...props }: any) => ({
    redirect: (basePath, id, data) => {
      dispatch(refreshView());
      dispatch(closeDash(Enum.Resource.Contributions, Enum.Crud.Update));
      if (!isMaster(permissions)) {
        dataProvider("GET_ONE", "contributions", { id }).then(({ data }) => {
          dispatch(openDash(Enum.Resource.Payments, Enum.Crud.None, { id: data.payment_id }));
        });
      }
      return `/contributions/${id}/show`;
    }
  })
)(({ classes, value: { user_id, contribution_id, Company, ...record } = {} as any, redirect, permissions, ...props }: any) => {
  return (
    <SimpleFormContainer
      {...props}
      record={record}
      className={classes.root}
      redirect={redirect}
      toolbar={<Toolbar permissions={permissions} />}
      validate={validate}
    >
      <GridContainer>
        <GridItem md={6} xs={9}>
          <InputTable />
        </GridItem>
        <GridItem className={classes.xsHide} xs={true}>
          <SimpleShowContainer>
            {sourceField("reporting_week")}
            {sourceField("reporting_week_count")}
            {sourceField("reporting_week_start")}
            {sourceField("reporting_week_end")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem className={classes.smHide} xs={true}>
          <SimpleShowContainer>
            <UserFields />
          </SimpleShowContainer>
        </GridItem>
        <GridItem sm={true} xs={3}>
          <PaymentFields />
        </GridItem>
      </GridContainer>
    </SimpleFormContainer>
  );
}) as any;
const Dash = enhance(({ classes, record, permissions, ...props }: any) =>
  isMaster(permissions) ? (
    <Input.Edit
      {...props}
      title={` Edit Contribution`}
      resource="contributions"
      basePath="/contributions"
      actions={<Actions dash permissions={permissions} data={record} />}
    >
      <Wrap classes={classes} value={record} permissions={permissions} />
    </Input.Edit>
  ) : (
    <Input.Create
      {...props}
      title={` Adjust Contribution`}
      resource="contributions"
      basePath="/contributions"
      actions={<Actions dash permissions={permissions} data={record} />}
    >
      <Wrap classes={classes} value={record} permissions={permissions} />
    </Input.Create>
  )
);
export const AdjustContributionDashEdit = props => (
  <DashPanel resource={Enum.Resource.Contributions} anchor={Enum.Anchor.Bottom} crud={Enum.Crud.Update} {...props}>
    <Dash />
  </DashPanel>
);
