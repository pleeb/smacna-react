import React from "react";
import {
  ResourceType,
  BasePathType,
  Alias,
  Enum,
  FieldType,
  UserFieldType,
  ContributionFieldType,
  CompanyFieldType,
  PaymentFieldType,
  ReportableFieldType
} from "../components";
import { company, contributionX, payment, paymentXX, user, splice } from "./fields";

const { Role, Resource, BasePath, Context } = Enum;

type Mask = -1 | 0 | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 | 512 | 1024 | 2048 | 4096 | 8192 | 16384 | 32768 | 65536;

interface IBitmaskMap {
  undefined?: Mask;
  null?: Mask;
  [BasePath.Dashboard]?: Mask;
  [BasePath.Companies]?: Mask;
  [BasePath.Contributions]?: Mask;
  [BasePath.Payments]?: Mask;
  [BasePath.Users]?: Mask;
  [BasePath.Reports]?: Mask;

  [Resource.Companies]?: Mask;
  [Resource.Contributions]?: Mask;
  [Resource.Payments]?: Mask;
  [Resource.Users]?: Mask;
  [Resource.Reports]?: Mask;

  [Context.List]?: Mask;
  [Context.Show]?: Mask;
  [Context.ListExpand]?: Mask;

  [Role.None]?: -1;
  [Role.User]?: Mask;
  [Role.Admin]?: Mask;
}
// TODO: Automate bit assignment.
const BitLocation: IBitmaskMap = {
  [BasePath.Dashboard]: 32768,
  [BasePath.Companies]: 1,
  [BasePath.Contributions]: 2,
  [BasePath.Payments]: 4,
  [BasePath.Users]: 8,
  [BasePath.Reports]: 16
};
const BitResource: IBitmaskMap = {
  [Resource.Companies]: 32,
  [Resource.Contributions]: 64,
  [Resource.Payments]: 128,
  [Resource.Users]: 256,
  [Resource.Reports]: 512
};
const BitContext: IBitmaskMap = {
  undefined: 1024,
  null: 1024,
  [Context.List]: 1024,
  [Context.Show]: 2048,
  [Context.ListExpand]: 4096
};
const BitRole: IBitmaskMap = {
  undefined: -1,
  null: -1,
  [Role.None]: -1,
  [Role.User]: 8192,
  [Role.Admin]: 16384,
  [Role.Master]: 65536
};
interface IMaskFieldMap {
  [mask: number]: FieldType[];
}
enum Bit {
  _Dashboard = BitLocation[BasePath.Dashboard],
  _Companies = BitLocation[BasePath.Companies],
  _Contributions = BitLocation[BasePath.Contributions],
  _Payments = BitLocation[BasePath.Payments],
  _Users = BitLocation[BasePath.Users],
  _Reports = BitLocation[BasePath.Reports],
  Companies = BitResource[Resource.Companies],
  Contributions = BitResource[Resource.Contributions],
  Payments = BitResource[Resource.Payments],
  Users = BitResource[Resource.Users],
  Reports = BitResource[Resource.Reports],
  List = BitContext[Context.List],
  Show = BitContext[Context.Show],
  ListExpand = BitContext[Context.ListExpand],
  None = BitRole[Role.None],
  User = BitRole[Role.User],
  Admin = BitRole[Role.Admin],
  Master = BitRole[Role.Master]
}
const fieldMap: IMaskFieldMap = {
  [-1]: [],

  // USER

  [Bit.User | Bit._Dashboard | Bit.Show | Bit.Contributions]: splice(["Company.company_name", "nullCell"])(contributionX),
  [Bit.User | Bit._Dashboard | Bit.ListExpand | Bit.Payments]: splice(["DashPaymentUpdateButton", "nullCell"])(paymentXX),
  [Bit.User | Bit._Dashboard | Bit.Show | Bit.Payments]: splice("Company.company_name", ["DashPaymentUpdateButton", "nullCell"])(payment),

  [Bit.User | Bit._Contributions | Bit.List | Bit.Contributions]: splice(["Company.company_name", "nullCell"])(contributionX),
  [Bit.User | Bit._Contributions | Bit.ListExpand | Bit.Payments]: splice(["DashPaymentUpdateButton", "nullCell"])(paymentXX),
  [Bit.User | Bit._Contributions | Bit.Show | Bit.Payments]: splice("Company.company_name", "Contribution.reporting_week_end", [
    "DashPaymentUpdateButton",
    "nullCell"
  ])(payment),

  [Bit.User | Bit._Users | Bit.List | Bit.Users]: splice("Company.company_name")(user),
  [Bit.User | Bit._Users | Bit.Show | Bit.Payments]: splice("Company.company_name", "full_name__ref_no_sort", [
    "DashPaymentUpdateButton",
    "nullCell"
  ])(payment),

  [Bit.User | Bit._Companies | Bit.Show | Bit.Users]: splice("Company.company_name")(user),
  [Bit.User | Bit._Companies | Bit.Show | Bit.Contributions]: splice(["Company.company_name", "nullCell"])(contributionX),
  [Bit.User | Bit._Companies | Bit.ListExpand | Bit.Payments]: splice(["DashPaymentUpdateButton", "nullCell"])(paymentXX),
  [Bit.User | Bit._Companies | Bit.Show | Bit.Payments]: splice("Company.company_name", ["DashPaymentUpdateButton", "nullCell"])(payment),

  // ADMIN

  [Bit.Admin | Bit._Dashboard | Bit.Show | Bit.Contributions]: splice("DashPaymentPayOrNullButton", [
    "DashPaymentAdjustButton",
    "nullCell"
  ])(contributionX),
  [Bit.Admin | Bit._Dashboard | Bit.ListExpand | Bit.Payments]: splice("nullCellB")(paymentXX),
  [Bit.Admin | Bit._Dashboard | Bit.Show | Bit.Payments]: splice()(payment),

  [Bit.Admin | Bit._Contributions | Bit.List | Bit.Contributions]: splice("DashPaymentPayOrNullButton", [
    "DashPaymentAdjustButton",
    "nullCell"
  ])(contributionX),
  [Bit.Admin | Bit._Contributions | Bit.ListExpand | Bit.Payments]: splice("nullCellB")(paymentXX),
  [Bit.Admin | Bit._Contributions | Bit.Show | Bit.Payments]: splice("Company.company_name", "Contribution.reporting_week_end")(payment),

  [Bit.Admin | Bit._Payments | Bit.List | Bit.Payments]: splice()(payment),

  [Bit.Admin | Bit._Users | Bit.List | Bit.Users]: splice()(user),
  [Bit.Admin | Bit._Users | Bit.Show | Bit.Payments]: splice("Company.company_name", "full_name__ref_no_sort")(payment),

  [Bit.Admin | Bit._Companies | Bit.List | Bit.Companies]: splice()(company),
  [Bit.Admin | Bit._Companies | Bit.Show | Bit.Payments]: splice("Company.company_name")(payment),
  [Bit.Admin | Bit._Companies | Bit.Show | Bit.Users]: splice("Company.company_name")(user),
  [Bit.Admin | Bit._Companies | Bit.Show | Bit.Contributions]: splice(["Company.company_name", "nullCell"], "DashPaymentPayOrNullButton", [
    "DashPaymentAdjustButton",
    "nullCell"
  ])(contributionX),
  [Bit.Admin | Bit._Companies | Bit.ListExpand | Bit.Payments]: splice("nullCellB")(paymentXX),

  // MASTER

  [Bit.Master | Bit._Dashboard | Bit.Show | Bit.Contributions]: splice("DashPaymentPayOrNullButton", [
    "DashPaymentAdjustButton",
    "DashPaymentEditButton"
  ])(contributionX),
  [Bit.Master | Bit._Dashboard | Bit.ListExpand | Bit.Payments]: splice("nullCellB")(paymentXX),
  [Bit.Master | Bit._Dashboard | Bit.Show | Bit.Payments]: splice()(payment),

  [Bit.Master | Bit._Contributions | Bit.List | Bit.Contributions]: splice("DashPaymentPayOrNullButton", [
    "DashPaymentAdjustButton",
    "DashPaymentEditButton"
  ])(contributionX),
  [Bit.Master | Bit._Contributions | Bit.ListExpand | Bit.Payments]: splice("nullCellB")(paymentXX),
  [Bit.Master | Bit._Contributions | Bit.Show | Bit.Payments]: splice("Company.company_name", "Contribution.reporting_week_end")(payment),

  [Bit.Master | Bit._Payments | Bit.List | Bit.Payments]: splice()(payment),

  [Bit.Master | Bit._Users | Bit.List | Bit.Users]: splice()(user),
  [Bit.Master | Bit._Users | Bit.Show | Bit.Payments]: splice("Company.company_name", "full_name__ref_no_sort")(payment),

  [Bit.Master | Bit._Companies | Bit.List | Bit.Companies]: splice()(company),
  [Bit.Master | Bit._Companies | Bit.Show | Bit.Payments]: splice("Company.company_name")(payment),
  [Bit.Master | Bit._Companies | Bit.Show | Bit.Users]: splice("Company.company_name")(user),
  [Bit.Master | Bit._Companies | Bit.Show | Bit.Contributions]: splice(["Company.company_name", "nullCell"], "DashPaymentPayOrNullButton", [
    "DashPaymentAdjustButton",
    "DashPaymentEditButton"
  ])(contributionX),
  [Bit.Master | Bit._Companies | Bit.ListExpand | Bit.Payments]: splice("nullCellB")(paymentXX)
};
interface IParams {
  basePath?: BasePathType;
  resource?: ResourceType;
  context?: Enum.Context;
  permissions?: Enum.Role;
  location?: BasePathType;
}
export const getFields = ({ location, basePath, context, resource, permissions }: IParams): FieldType[] => {
  const mask = BitRole[permissions] | BitLocation[location] | BitContext[context] | BitResource[resource];
  const fields = fieldMap[mask] || [];
  return fields;
};
