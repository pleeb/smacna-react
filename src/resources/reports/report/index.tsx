import { CompanyFieldType, EnumerableFieldType, NumericFieldType } from "../../";
export interface ITime {
  start: Date;
  end: Date;
  total: number;
  query: number;
  latency: number;
  fetching?: boolean;
}
export interface IRequest {
  groups: EnumerableFieldType[];
  computes: NumericFieldType[];
  selects: CompanyFieldType[];
  companies: any;
  contributions: any;
}
export interface IReport {
  query?: string;
  time: ITime;
  request: IRequest;
  datas?: any;
  fetching: boolean;
  error?: string | false;
}
export * from "./ReportView";
export * from "./DynamicTable";
export * from "./NetStats";
