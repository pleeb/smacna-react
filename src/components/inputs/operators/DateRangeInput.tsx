import React, { Fragment, Component } from "react";
import { BaseRangeInput } from "./BaseRangeInput";
import { DateInput } from "../";
import { Enum } from "./";

interface IProps {
  source: string;
  field?: string;
  defaultOperator?: Enum.NumberOperator;
  alwaysOn?: boolean;
  label?: string;
  defaultStart?: string;
  defaultEnd?: string;
}
export const DateRangeInput = (props: IProps) => (
  <BaseRangeInput meta={{ form: "record-form" }} InputComponent={DateInput} {...props} defaultStart="1995-01-01" defaultEnd="2017-01-01" />
);
