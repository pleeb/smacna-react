import moment, { Moment } from "moment";

interface TFromTo<T = any> {
  readonly from: T;
  readonly to: T;
}
export class DateRange implements TFromTo<Moment> {
  private _from: Moment = moment(this.__from);
  private _to: Moment = moment(this.__to);
  get from(): Moment {
    return this._from.clone();
  }
  get to(): Moment {
    return this._to.clone();
  }
  readonly date: TFromTo<Date> = this.createFromTo("toDate");
  readonly month: TFromTo<number> = this.createFromTo("month");
  readonly week: TFromTo<number> = this.createFromTo("week");
  readonly day: TFromTo<number> = this.createFromTo("day");
  constructor(private __from: Date, private __to: Date) {}
  private createFromTo(method: ValidCall): TFromTo<any> {
    const self = this;
    return {
      get from() {
        return self._from[method]();
      },
      get to() {
        return self._to[method]();
      }
    };
  }
}
type ValidCall = "day" | "week" | "month" | "toDate";

const notes = [
  // Contributions Page - Added "All" to the Status filter
  // `Reporting Table - Changed "Hours Paid" to "Total Hours Paid".`,
  // `Centered and positioned Report Contribution button betweendashboard header and Delinquent Contribution listing.`,
  // `Increased the width of Report Contribution button to 50% of the container width to increae visibility.`,
  // `Fixed field label "Reporting Date" to "Week Ending Date".`,
  // `Hid the operator labels and options on the search function. Defaulted hint: Starts With.`,
  // `Opened User and Company profiles edit functionality to allow database modification. Users may now update thei user or company profile.`,
  // `Opened Report / Adjust / Pay Delinquency functionality to allow database modification. Users may now pay by mail.`,
  // `Removed Delete function on a user's profile to preserve database data integrity for payment records for now.`
];
