import React from "react";
import { connect } from "react-redux";
import { change } from "redux-form";
import { dataProvider } from "../../data";
import { FieldType } from "../";
import Icon from "@material-ui/icons/EventNote";
import { DashButton, Enum } from "./";

const fields: FieldType[] = ["rate_apprentice", "rate_mechanic", "rate_light_commercial"];

export const DashPaymentReportButton = connect(null)(({ dispatch }: any) => (
  <DashButton
    Icon={Icon}
    onClick={async () => {
      const response = await dataProvider("GET_ONE", "rates", { id: 1 });
      for (let field of fields) dispatch(change("record-form", field, +response.data[field]));
    }}
    crud={Enum.Crud.Create}
    label="Report Contribution"
    record={{ id: undefined, payment_id: undefined }}
    resource={Enum.Resource.Contributions}
    variant="outlined"
    size="large"
  />
)) as any;
