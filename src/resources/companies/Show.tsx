import React from "react";
import { connect } from "react-redux";
import { ShowGuesser, Show } from "react-admin";
import { CardActions, withStyles } from "@material-ui/core";
import * as choices from "./choices";
import {
  isMaster,
  Field,
  Enum,
  Alias,
  GridContainer,
  GridItem,
  sourceField,
  SimpleShowContainer,
  Actionbar,
  ReferenceManyContainer,
  DatagridMapper,
  DashPanel,
  Button
} from "../";
import { ReferenceManyContribution } from "../contributions";
import { ReferenceManyPayment } from "../payments";
import { ReferenceManyUser } from "../users";

const { Tab, TabbedShowLayout } = Field;

const connection = connect(
  ({ session: { company_id } }: any, { id }: any) => {
    return {
      isOwner: +company_id === +id
    };
  },
  {}
);
const Actions = connection(({ isOwner, permissions, ...props }: any) => {
  const verb = props.dash ? "Viewing Company" : "Showing Company";
  const title = (props.data || {})["company_name"];
  const editable = isOwner || isMaster(permissions);
  return (
    <Actionbar verb={verb} title={title} {...props}>
      {editable && <Button.DashEditButton label={isOwner ? "Update" : "Edit"} />}
      {permissions >= Enum.Role.Admin && <Button.ListButton />}
    </Actionbar>
  );
});
const Admin = props => (
  <GridContainer withMargin {...props}>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("company_name")}
        {sourceField("id")}
        {sourceField("phone")}
        {sourceField("fax")}
        {sourceField("contact_name")}
      </SimpleShowContainer>
    </GridItem>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("address")}
        {sourceField("city")}
        {sourceField("state")}
        {sourceField("zipcode")}
        {sourceField("county")}
      </SimpleShowContainer>
    </GridItem>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("ifus")}
        {sourceField("association")}
        {sourceField("permanent")}
        {sourceField("work_code")}
        {sourceField("association_code")}
      </SimpleShowContainer>
    </GridItem>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("start_date")}
        {sourceField("outbus_date")}
        {sourceField("inactive_date")}
        {sourceField("chap11_date")}
      </SimpleShowContainer>
    </GridItem>
  </GridContainer>
);
const User = props => (
  <GridContainer withMargin {...props}>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("company_name")}
        {sourceField("id")}
      </SimpleShowContainer>
    </GridItem>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("phone")}
        {sourceField("fax")}
        {sourceField("contact_name")}
      </SimpleShowContainer>
    </GridItem>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("address")}
        {sourceField("city")}
        {sourceField("state")}
      </SimpleShowContainer>
    </GridItem>
    <GridItem xs={true}>
      <SimpleShowContainer>
        {sourceField("zipcode")}
        {sourceField("county")}
      </SimpleShowContainer>
    </GridItem>
  </GridContainer>
);
export const CompanyShow = ({ permissions, ...props }: any) => {
  const state = props.location.state || {};
  const isAdmin = permissions >= Enum.Role.Admin;
  const isAdminProfile = isAdmin && state.fromProfileLink;
  return (
    <Field.Show {...props} actions={<Actions permissions={permissions} id={props.id} />}>
      <GridContainer direction="column">
        {isAdmin ? <Admin /> : <User />}
        <GridContainer>
          {isAdminProfile ? (
            <TabbedShowLayout>
              <Tab label="Company Users">
                <ReferenceManyUser permissions={permissions} />
              </Tab>
            </TabbedShowLayout>
          ) : (
            <TabbedShowLayout>
              <Tab label="Paid Contributions">
                <ReferenceManyContribution
                  permissions={permissions}
                  filter={{ status: { $in: [Enum.Status.PAID, Enum.Status.OVERPAY] } }}
                />
              </Tab>
              <Tab label="Pending Payments" path="pending">
                <ReferenceManyPayment permissions={permissions} filter={{ pay_status: Enum.PayStatus.PENDING }} target="company_id" />
              </Tab>
              <Tab label="Delinquent Contributions" path="delinquent">
                <ReferenceManyContribution
                  permissions={permissions}
                  filter={{ status: Enum.Status.DELINQUENT, pay_status: { $ne: Enum.Status.PENDING } }}
                />
              </Tab>
              <Tab label="Company Users" path="users">
                <ReferenceManyUser permissions={permissions} />
              </Tab>
            </TabbedShowLayout>
          )}
        </GridContainer>
      </GridContainer>
    </Field.Show>
  );
};
const Dash = ({ permissions, ...props }: any) => {
  return (
    <Field.Show
      id={0}
      resource="companies"
      basePath="/companies"
      {...props}
      actions={<Actions anchor={props.anchor} id={props.id} permissions={permissions} dash />}
    >
      <GridContainer withMargin>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("company_name")}
            {sourceField("id")}
            {sourceField("phone")}
            {sourceField("fax")}
            {sourceField("contact_name")}
            {sourceField("address")}
            {sourceField("city")}
            {sourceField("state")}
            {sourceField("zipcode")}
            {sourceField("county")}
            {sourceField("created_at")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("croup")}
            {sourceField("ifus")}
            {sourceField("association")}
            {sourceField("permanent")}
            {sourceField("work_code")}
            {sourceField("association_code")}
            {sourceField("start_date")}
            {sourceField("outbus_date")}
            {sourceField("inactive_date")}
            {sourceField("chap11_date")}
            {sourceField("updated_at")}
          </SimpleShowContainer>
        </GridItem>
      </GridContainer>
    </Field.Show>
  );
};
export const CompanyDashShow = ({ classes, ...props }: any) => {
  return (
    <DashPanel resource={Enum.Resource.Companies} anchor={Enum.Anchor.Right} crud={Enum.Crud.Read} {...props}>
      <Dash />
    </DashPanel>
  );
};
