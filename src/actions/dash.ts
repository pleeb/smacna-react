import { Enum } from "../";

export const OPEN_DASH = "OPEN_DASH";
export const CLOSE_DASH = "CLOSE_DASH";
export const CLOSE_ALL_DASH = "CLOSE_ALL_DASH";

type Action = "CLOSE_DASH" | "OPEN_DASH" | "CLOSE_ALL_DASH";
interface IMeta {
  //
}
const ff_action = (type: Action, resource: Enum.Resource, crud: Enum.Crud) => (record?) => ({
  type,
  resource,
  crud,
  record
});
type ResourceCrudMap = {
  [Enum.Resource.Companies]: CrudActionMap;
  [Enum.Resource.Payments]: CrudActionMap;
  [Enum.Resource.Contributions]: CrudActionMap;
  [Enum.Resource.Users]: CrudActionMap;
  [Enum.Resource.Rates]: CrudActionMap;
  [Enum.Resource.Reports]: CrudActionMap;
};
type CrudActionMap = {
  [Enum.Crud.Create]: ActionMap;
  [Enum.Crud.Read]: ActionMap;
  [Enum.Crud.Update]: ActionMap;
  [Enum.Crud.Delete]: ActionMap;
};
type ActionMap = {
  open: (record: { id: number }) => any;
  close: () => any;
};
export type DashAction = (record: { id: number }) => any;
export const resources: Enum.Resource[] = [
  Enum.Resource.Companies,
  Enum.Resource.Payments,
  Enum.Resource.Contributions,
  Enum.Resource.Users,
  Enum.Resource.Rates,
  Enum.Resource.Reports
];
export const cruds: Enum.Crud[] = [Enum.Crud.Create, Enum.Crud.Update, Enum.Crud.None];
const _dashAction: ResourceCrudMap = {} as ResourceCrudMap;
resources.map(resource =>
  cruds.map(
    crud =>
      ((_dashAction[resource] ? _dashAction[resource] : (_dashAction[resource] = {} as CrudActionMap))[crud] = {
        open: ff_action(OPEN_DASH, resource, crud),
        close: ff_action(CLOSE_DASH, resource, crud)
      })
  )
);
export const dashAction: ResourceCrudMap = _dashAction;
export const closeAllDash = () => ({ type: CLOSE_ALL_DASH });
export const closeDash = (resource: Enum.Resource, crud: Enum.Crud) => {
  return {
    type: CLOSE_DASH,
    resource,
    crud
  };
};
export const openDash = (resource: Enum.Resource, crud: Enum.Crud, record) => {
  return {
    type: OPEN_DASH,
    resource,
    crud,
    record
  };
};
