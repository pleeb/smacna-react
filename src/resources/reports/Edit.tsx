import React from "react";
import { withStyles } from "@material-ui/core";
import RunIcon from "@material-ui/icons/Send";
import { Link } from "react-router-dom";
import { withDataProvider } from "react-admin";
import * as choices from "./choices";
import * as company from "../companies/choices";
import * as contribution from "../contributions/choices";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { RunButton } from "./RunButton";
import { Input, Button } from "../";
import { DynamicTable, NetStats } from "./report";
import { TabbedInputForm } from "./inputs";

const EditToolbar = props => {
  return (
    <Input.Toolbar {...props}>
      <SaveTouched redirect={false} />
      <RunButton />
      <Button.DeleteButton style={{ marginLeft: "auto" }} redirect="/reports" />
    </Input.Toolbar>
  );
};
export const ReportEdit = withStyles({
  root: {
    display: "flex",
    flexDirection: "column"
  }
})(({ classes, ...props }: any) => {
  return <div className={classes.root}>
    <Input.Edit {...props} actions={null} aside={<NetStats />}>
      <TabbedInputForm toolbar={<EditToolbar />} />
    </Input.Edit>
    <DynamicTable />
  </div>;
});
const Test = (props) => {
  return <div>test {console.log("edit form render",props)}.</div>
}
const SaveTouched: any = connect(
  (state: any) => {
    const recordForm = state.form["record-form"] || {};
    return {
      disabled: !recordForm.anyTouched,
      label: "Save"
    };
  },
  {}
)(Button.SaveButton);

/*

premade

delinquency report
workcode worker hours summary
workcode worker hours detailed
weekly deposit summary actual
weekly deposit ledger by association
weekly contribution summary
deposit date summary
deposit date detailed
receive date detailed
IPF contributors list
contribution summary

*/

const reports = [
  { title: "Delinquency Report", description: "Historic listing of all delinquent contributions." },
  { title: "Workcode Worker Hour Summary", description: "Summary of number of workers and hours worked grouped by workcode." },
  { title: "Workcode Worker Hour Detailed", description: "List of company worker and hours grouped by workcode." },
  { title: "Deposit Weekly Summary Actual", description: "Summary of contributions searched by deposit date range." },
  { title: "Association Deposit Weekly Ledger", description: "List of contributions grouped by affiliation, searched by deposit date range." },
  { title: "Weekly Contribution Summary", description: "" },
  { title: "Deposit Date Summary", description: "" },
  { title: "Deposit Date Detailed", description: "" },
  { title: "Receive Date Detailed", description: "" },
  { title: "IPF Contributors List", description: "Historic listing of all IPF contributing companies." },
  { title: "Contribution Summary", description: "" }
];
