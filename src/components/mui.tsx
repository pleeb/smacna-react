import * as React from "react";
import { TextField as _TextField, withStyles } from "@material-ui/core";

export const TextField = withStyles({ root: { marginTop: 16, marginBottom: 8 } })(_TextField) as typeof _TextField;
