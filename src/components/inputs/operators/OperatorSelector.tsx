import React from "react";
import { withStyles, Select, FormControl, InputLabel, FormHelperText, MenuItem, Input } from "@material-ui/core";
import { OperatorChoices } from "./choices";

interface IProps {
  onChange: (e: React.ChangeEvent) => void;
  value: any;
  readonly: boolean;
  choices: OperatorChoices;
  classes: { [props: string]: string };
}
const Selector = ({ onChange, value, readonly, classes, choices }: IProps) => (
  <FormControl className={classes.root}>
    <InputLabel shrink>Operator</InputLabel>
    <Select
      readOnly={readonly}
      value={value}
      onChange={onChange}
      input={<Input name="operator" className={classes.input} />}
      name="operator"
    >
      {choices.map(({ id, name }, i) => (
        <MenuItem value={id} key={i}>
          <strong>{name}</strong>
        </MenuItem>
      ))}
    </Select>
  </FormControl>
);
const styles = {
  root: { marginTop: 16, marginBottom: 8, width: 125 },
  input: { width: 125 }
};
export const OperatorSelector = withStyles(styles)(Selector);
