import React from "react";
import PropTypes from "prop-types";
import { withStyles, Typography } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";
import Toc from "@material-ui/icons/Toc";

const styles = theme => ({
  root: {
    width: "100%",
    borderTop: "1px solid LIGHTGRAY",
    backgroundColor: theme.palette.background.paper,
    marginTop: 14,
    paddingTop: 14
  },
  subheader: {
    paddingLeft: 12,
    color: theme.palette.primary.SECONDARY
  },
  item: {
    paddingLeft: 12
  },
  red: {
    backgroundColor: "crimson"
  },
  yellow: {
    backgroundColor: "orange"
  },
  green: {
    backgroundColor: "forestgreen"
  }
});

function FolderList(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <List
        disablePadding
        dense
        subheader={
          <Typography variant="button" className={classes.subheader}>
            <strong>COLOR LEGEND</strong>
          </Typography>
        }
      >
        <ListItem className={classes.item} divider>
          <Avatar className={classes.red}>
            <Toc />
          </Avatar>
          <ListItemText primary="Delinquent" secondary="Red" />
        </ListItem>
        <ListItem className={classes.item} divider>
          <Avatar className={classes.yellow}>
            <Toc />
          </Avatar>
          <ListItemText primary="Pending" secondary="Yellow" />
        </ListItem>
        <ListItem className={classes.item} divider>
          <Avatar className={classes.green}>
            <Toc />
          </Avatar>
          <ListItemText primary="Paid" secondary="Green" />
        </ListItem>
      </List>
    </div>
  );
}

FolderList.propTypes = {
  classes: PropTypes.object.isRequired
};

export const ColorLegend = withStyles(styles)(FolderList);
