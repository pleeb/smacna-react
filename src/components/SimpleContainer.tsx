import React from "react";
import { withStyles, Typography } from "@material-ui/core";
import SimpleForm from "./SimpleForm";
import { ReferenceManyField, SimpleShowLayout, Pagination } from "./fields";
import { Enum, ResourceType, FieldType, BasePathType } from "./";

interface ISimpleClasses {
  show: any;
  showWithPad: any;
  container: any;
  label: any;
  refMany: any;
  containerWithBorder: any;
}
interface ILabelable {
  label?: string;
  color?: "primary" | "secondary" | "textSecondary" | "textPrimary";
  classes?: ISimpleClasses;
}
interface ISimpleProps extends ILabelable {
  classes?: ISimpleClasses;
  className?: any;
  validate?: any;
  withPad?: boolean;
  withBorder?: boolean;
  record?: { id: number };
  data?: { id?: number };
  resource?: ResourceType;
  basePath?: BasePathType;
  redirect?: any;
  toolbar?: any;
}
const enhance = withStyles({
  refMany: {
    marginBottom: 0,
    width: "100%",
    display: "block",
    padding: 0,
    margin: 0
  },
  show: {
    width: "100%",
    padding: "0!important",
    margin: "0!important"
  },
  showWithPad: {
    width: "auto",
    paddingLeft: "24px!important",
    paddingRight: "24px!important",
    paddingBottom: "0!important",
    paddingTop: "0!important",
    margin: "0!important",
    display: "flex",
    flexDirection: "column"
  },
  container: {
    display: "flex",
    // paddingTop: "1em",
    flexDirection: "column"
  },
  containerWithBorder: {
    display: "flex",
    paddingTop: "1em",
    flexDirection: "column",
    border: "1px solid lightgrey",
    borderRadius: 3
  },
  label: {
    fontWeight: "bold",
    textTransform: "uppercase",
    textDecorationLine: "underline",
    textDecorationStyle: "dotted",
    textUnderlinePosition: "under",
    paddingLeft: "1em"
  }
} as ISimpleClasses);
const Label = enhance(({ label, classes, color }: ILabelable) =>
  label ? (
    <Typography color={color || "secondary"} variant="subheading">
      <span className={classes.label}>{label}</span>
    </Typography>
  ) : null
);
export const SimpleShowContainer = enhance(({ classes, withBorder, withPad, ...props }: ISimpleProps) =>
  props.record ? (
    <div className={withBorder ? classes.containerWithBorder : classes.container}>
      <Label {...props} classes={classes} />
      <SimpleShowLayout {...props} className={withPad ? classes.showWithPad : classes.show} />
    </div>
  ) : null
) as React.StatelessComponent<ISimpleProps>;
export const SimpleFormContainer = enhance(({ classes, withBorder, withPad, ...props }: any) => (
  <div className={withBorder ? classes.containerWithBorder : classes.container}>
    <Label {...props} classes={classes} />
    <SimpleForm {...props} className={withPad ? classes.showWithPad : classes.show} />
  </div>
)) as React.StatelessComponent<ISimpleProps>;
export interface ISort {
  field: FieldType;
  order: "ASC" | "DESC";
}
interface IRefProps {
  perPage?: number;
  sort?: ISort;
  filter?: any;
  reference: ResourceType;
  target: FieldType;
  label?: string;
  basePath?: string;
  resource?: string;
  withBorder?: boolean;
  source: FieldType;
  classes?: any;
  permissions?: Enum.Role;
}
export const ReferenceManyContainer = enhance(
  ({ classes, withBorder, ...props }: IRefProps) =>
    (
      <div className={withBorder ? classes.containerWithBorder : classes.container}>
        <Label {...props} classes={classes} />
        <ReferenceManyField
          perPage={10}
          sort={{ field: "updated_at", order: "DESC" }}
          pagination={<Pagination />}
          className={classes.refMany}
          {...props}
        />
      </div>
    ) as any
) as React.StatelessComponent<IRefProps>;
export { ReferenceManyField } from "./fields";
