import phone from "google-libphonenumber";

const PNF = phone.PhoneNumberFormat;
const phoneUtil = phone.PhoneNumberUtil.getInstance();

export const parse = {
  phone: value => (value ? phoneUtil.format(phoneUtil.parseAndKeepRawInput(value, "US"), PNF.NATIONAL) : "----"),
  any: value => (value ? value : "----"),
  number: value => +(value || 0),
  string: value => (value ? value.toString() : "----"),
  money: value => (value || 0).toLocaleString("en", { style: "currency", currency: "USD" }),
  date: value => new Date(value).toLocaleDateString(),
  datetime: value => new Date(value).toLocaleString(),
  time: value => new Date(value).toLocaleTimeString()
};
