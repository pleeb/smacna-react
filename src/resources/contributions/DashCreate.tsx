import React, { Fragment } from "react";
import { connect } from "react-redux";
import { withStyles, Typography } from "@material-ui/core";
import { InputTable } from "./table";
import { UserFields } from "../users/UserFields";
import { openDash } from "../../actions";
import { moment, Calendar, PaymentFields, ReportingFields, Toolbar, validate } from "./form";
import compose from "recompose/compose";
import {
  DashPanel,
  Enum,
  Input,
  Field,
  Alias,
  sourceField,
  GridItem,
  GridContainer,
  SimpleShowContainer,
  SimpleFormContainer,
  Actionbar,
  dataProvider
} from "../";

const NDash = props => <span style={{ margin: "0 14px" }}>&ndash;</span>;
const Actions = connect(({ form: { "record-form": form = {} } }: any) => {
  const { values = {} } = form;
  const { cid, reporting_week_end, reporting_week_start } = values;
  return { cid, reporting_week_end, reporting_week_start };
})(({ cid, reporting_week_end, reporting_week_start, permissions, ...props }: any) => {
  const end = reporting_week_end && reporting_week_end.toLocaleDateString();
  const start = reporting_week_start && reporting_week_start.toLocaleDateString();
  let header,
    hasSelection = false;
  if (start && end) {
    header = (
      <span>
        {start}
        <NDash />
        {end}
      </span>
    );
    hasSelection = true;
  } else if (!start && !end) {
    header = <span>Please select the starting and ending week.</span>;
  } else {
    header = (
      <span>
        {start}
        <NDash />
        Please select the ending week.
      </span>
    );
  }
  const title = (
    <span>
      {header}
      <br />
      <Typography color={hasSelection ? "primary" : "error"} variant="subheading">
        {hasSelection
          ? "Reselect from the valid dates to redo week selection."
          : "Select from the valid dates indicated in blue. Multiple week selection is allowed."}
      </Typography>
    </span>
  );
  return <Actionbar verb="Report Contribution" title={title} {...props} />;
}) as any;

const enhance = compose(
  connect(
    ({ theme }: any) => ({ theme }),
    dispatch => ({
      redirect: (basePath, id, data) => {
        dataProvider("GET_ONE", "contributions", { id }).then(({ data }) => {
          dispatch(openDash(Enum.Resource.Payments, Enum.Crud.None, { id: data.payment_id }));
        });
        return `/contributions/${id}/show`;
      }
    })
  ),
  withStyles(theme => {
    return {
      xsHide: {
        [theme.breakpoints.down("sm")]: {
          display: "none!important"
        }
      },
      smHide: {
        [theme.breakpoints.down("md")]: {
          display: "none!important"
        }
      }
    };
  })
);
const Wrap = ({ classes, ...props }) => (
  <Fragment>
    <Calendar />
    <SimpleFormContainer record={props.defaultValue} {...props} toolbar={<Toolbar />} validate={validate}>
      <GridContainer>
        <GridItem md={6} xs={9}>
          <InputTable />
        </GridItem>
        <GridItem className={classes.xsHide} xs={true}>
          <ReportingFields />
        </GridItem>
        <GridItem className={classes.smHide} xs={true}>
          <UserFields />
        </GridItem>
        <GridItem sm={true} xs={3}>
          <PaymentFields />
        </GridItem>
      </GridContainer>
    </SimpleFormContainer>
  </Fragment>
);
const defaultValue = {
  id: undefined,
  payment_id: undefined,
  num_mechanic: 0,
  num_apprentice: 0,
  num_light_commercial: 0,
  hours_mechanic: 0,
  hours_apprentice: 0,
  hours_light_commercial: 0,
  hours_target: 0
};
const Dash = enhance(({ permissions, classes, redirect, ...props }: any) => (
  <Input.Create
    {...props}
    title={` Report Contribution`}
    resource="contributions"
    basePath="/contributions"
    actions={<Actions dash permissions={permissions} />}
  >
    <Wrap classes={classes} defaultValue={defaultValue} redirect={redirect} />
  </Input.Create>
));
export const ReportContributionDashCreate = props => (
  <DashPanel resource={Enum.Resource.Contributions} anchor={Enum.Anchor.Bottom} crud={Enum.Crud.Create} {...props}>
    <Dash />
  </DashPanel>
);
