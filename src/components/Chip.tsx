import React from "react";
import { Chip as _Chip, withStyles, Avatar, Icon } from "@material-ui/core";

interface IProps {
  icon: string;
  value: string;
}
export const Chip = withStyles({
  root: {
    maxHeight: 48,
    height: 38,
    margin: 0
  },
  label: {
    whiteSpace: "initial",
    textAlign: "center",
    fontWeight: "bold",
    paddingLeft: 8,
    paddingRight: 8
  },
  clickable: {
    color: "#3f51b5"
  },
  colorPrimary: {},
  colorSecondary: {}
})(({ icon, color, value, clickable, classes, ...props }: any) => (
  <_Chip classes={classes} clickable={clickable} label={value} color={color} />
)) as React.StatelessComponent<any>;
