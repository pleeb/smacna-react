import React, { Children, cloneElement } from "react";
import { DashCloseButton, RefreshButton, ListButton } from "./buttons";
import { CardActions, withStyles } from "@material-ui/core";
import { FieldType, Enum, OwnerIcon, ProfileIcon } from "./";

const enhance = withStyles(theme => ({
  cardActions: {
    zIndex: 2,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    padding: 0,
    margin: 0,
    marginBottom: 14
  },
  header: {
    display: "inline-flex!important",
    alignItems: "flex-start",
    width: "100%",
    fontSize: 28,
    [theme.breakpoints.down("md")]: {
      fontSize: 26
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: 22
    }
  }
}));
interface IProps {
  classes?: any;
  children?: any;
  data?: any;
  resource?: Enum.Resource;
  verb: string;
  title: any;
  strong?: boolean;
  anchor?: Enum.Anchor;
  basePath?: string;
  dash?: boolean;
}
export const Actionbar = enhance(
  ({ classes, strong = false, title, verb, children = [], data, resource, anchor, basePath, dash, ...props }: IProps) => {
    const thin = anchor === Enum.Anchor.Right || anchor === Enum.Anchor.Left;
    const record = data || {};
    const nextProps = { record, resource, basePath };
    const Icon = resourceIconMap[resource];
    const variant = resource === Enum.Resource.Dashboard ? "display1" : "headline";
    return (
      <CardActions className={classes.cardActions}>
        <div style={{ display: "inline-flex", marginRight: 8 }}>
          {dash && thin ? <DashCloseButton iconOnly /> : <Icon role={record.role} color="secondary" style={style} />}
        </div>
        <div className={classes.header}>
          <div style={{ display: "inline-flex", flexDirection: thin ? "column" : "row", flexWrap: "wrap" }}>
            <div style={{ flexShrink: 0, display: "inline-flex", alignItems: "center" }}>
              <span style={{ flexShrink: 0 }}>{verb}</span>
              <Spacer />
              <OwnerIcon record={data} type="chip" />
              <Spacer />
            </div>
            <div style={{ flexShrink: 0, display: "inline-flex", fontWeight: strong ? "bold" : "initial" }}>{title}</div>
          </div>
        </div>
        <div style={{ flexShrink: 0 }}>
          {Children.map(children, child => child && cloneElement(child, nextProps))}
          {!dash && <RefreshButton />}
          {dash && !thin ? <DashCloseButton /> : null}
        </div>
      </CardActions>
    ) as any;
  }
) as React.StatelessComponent<IProps>;
const Spacer = props => <span style={{ marginLeft: 7 }} />;
const style = { verticalAlign: "bottom", fontSize: 32 };
const resourceIconMap = {
  [Enum.Resource.Dashboard]: Enum.Icon.Dashboard,
  [Enum.Resource.Companies]: Enum.Icon.Company,
  [Enum.Resource.Contributions]: Enum.Icon.Contribution,
  [Enum.Resource.Payments]: Enum.Icon.Payment,
  [Enum.Resource.Rates]: Enum.Icon.Rates,
  [Enum.Resource.Reports]: Enum.Icon.Report,
  [Enum.Resource.Users]: ProfileIcon
};
