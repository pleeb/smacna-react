import React, { Component } from "react";
import PropTypes from "prop-types";
import { reduxForm, Field } from "redux-form";
import { connect } from "react-redux";
import compose from "recompose/compose";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import LinearProgress from "@material-ui/core/LinearProgress";
import TextField from "@material-ui/core/TextField";
import { MuiThemeProvider, createMuiTheme, withStyles } from "@material-ui/core/styles";
import LockIcon from "@material-ui/icons/Lock";
import { Notification, translate, userLogin } from "react-admin";

import { ColorPicker } from "./ColorPicker";

const styles = theme => ({
  main: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  card: {
    minWidth: 300,
    marginTop: "6em"
  },
  button: {
    display: "flex",
    justifyContent: "center"
  },
  avatar: {
    display: "flex",
    justifyContent: "center",
    paddingTop: 24
  },
  icon: {
    width: "fit-content",
    height: "fit-content",
    borderRadius: 0
  },
  hint: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center"
  },
  hintValue: {
    display: "flex",
    justifySelf: "left",
    margin: "0.2em 0 0 1em",
    fontSize: "0.8em"
  },
  hintContainer: {
    marginTop: "1em",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    color: theme.palette.grey[500]
  },
  form: {
    padding: "0 2em 2em 2em"
  },
  input: {
    marginTop: "1em"
  },
  actions: {
    padding: "0 2em 2em 2em"
  }
});

// see http://redux-form.com/6.4.3/examples/material-ui/
const renderInput = ({ meta: { touched, error } = {} as any, input: { ...inputProps }, ...props }) => (
  <TextField
    InputLabelProps={{ style: { color: "white", fontWeight: "bold", letterSpacing: 1 } }}
    error={!!(touched && error)}
    helperText={touched && error}
    {...inputProps}
    {...props}
    fullWidth
  />
);

class Login extends Component<any, any> {
  state = { background: "Gainsboro", foreground: "DodgerBlue" };
  login = auth => {
    return this.props.userLogin(auth, this.props.location.state ? this.props.location.state.nextPathname : "/");
  };
  selectBackground = (background: string) => this.setState({ background });
  selectForeground = (foreground: string) => this.setState({ foreground });
  render() {
    const { classes, handleSubmit, isLoading, translate } = this.props;
    const dev = false;
    return (
      <div className={classes.main} style={{ backgroundColor: this.state.background }}>
        {dev ? (
          <ColorPicker
            background={this.state.background}
            foreground={this.state.foreground}
            selectBackground={this.selectBackground}
            selectForeground={this.selectForeground}
          />
        ) : null}
        <Card className={classes.card} style={{ backgroundColor: this.state.foreground }}>
          <div className={classes.avatar}>
            <Avatar className={classes.icon} src="assets/logo_256.png" />
          </div>
          <form onSubmit={handleSubmit(this.login)}>
            <div className={classes.hintContainer}>
              <div className={classes.hint}>
                <small>{null}</small>
              </div>
            </div>
            <div className={classes.form}>
              <div className={classes.input}>
                <Field
                  autoFocus
                  name="username"
                  component={renderInput}
                  label={translate("ra.auth.username")}
                  disabled={isLoading}
                  inputProps={{ style: { padding: 8, backgroundColor: "gainsboro", borderRadius: 5 } }}
                />
              </div>
              <div className={classes.input}>
                <Field
                  name="password"
                  component={renderInput}
                  label={translate("ra.auth.password")}
                  type="password"
                  disabled={isLoading}
                  inputProps={{ style: { padding: 8, backgroundColor: "gainsboro", borderRadius: 5 } }}
                />
              </div>
            </div>
            <CardActions className={classes.actions}>
              <Button variant="raised" type="submit" color="primary" disabled={isLoading} className={classes.button} fullWidth>
                {isLoading ? <LinearProgress /> : "Sign in"}
              </Button>
            </CardActions>
          </form>
        </Card>
        <Notification />
      </div>
    );
  }
}

// Login.propTypes = {
//   ...PropTypes,
//   authProvider: PropTypes.func,
//   classes: PropTypes.object,
//   previousRoute: PropTypes.string,
//   translate: PropTypes.func.isRequired,
//   userLogin: PropTypes.func.isRequired
// };

const mapStateToProps = state => ({ isLoading: state.admin.loading > 0 });

const enhance = compose(
  translate,
  reduxForm({
    form: "signIn",
    validate: (values: any, props) => {
      const errors = {} as any;
      const { translate } = props as any;
      if (!values.username) {
        errors.username = translate("ra.validation.required");
      }
      if (!values.password) {
        errors.password = translate("ra.validation.required");
      }
      return errors;
    }
  }),
  connect(
    mapStateToProps,
    { userLogin }
  ),
  withStyles(styles as any)
);

const EnhancedLogin = enhance(Login);
export default EnhancedLogin;
