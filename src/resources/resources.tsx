import React from "react";
import { Resource as _Resource } from "react-admin";
import { withStyles, createMuiTheme, colors } from "@material-ui/core";
import { CONTENT_MAX_WIDTH as maxWidth, CONTENT_MIN_WIDTH as minWidth } from "../consts";
import {
  ContributionShow,
  ContributionList,
  ReportContributionDashCreate,
  AdjustContributionDashEdit,
  ContributionDashShow
} from "./contributions";
import { CompanyShow, CompanyList, CompanyDashEdit, CompanyDashShow } from "./companies";
import { PaymentList, PaymentOutstandingDashCreate, PaymentProcessDashEdit, PaymentDashShow } from "./payments";
import { ReportEdit, ReportCreate, ReportList, ReportShow } from "./reports";
import { UserList, UserShow, UserDashEdit, UserDashShow } from "./users";
import { RatesShow, RateDashEdit } from "./rates";

export const Resources = {
  ContributionShow,
  ContributionList,
  ReportContributionDashCreate,
  AdjustContributionDashEdit,
  RatesShow,
  RateDashEdit,
  CompanyShow,
  CompanyList,
  CompanyDashEdit,
  CompanyDashShow,
  PaymentList,
  PaymentOutstandingDashCreate,
  PaymentProcessDashEdit,
  PaymentDashShow,
  ReportEdit,
  ReportCreate,
  ReportList,
  ReportShow,

  UserList,
  UserShow,
  UserDashEdit,
  UserDashShow
};
export { Dashboard, DashboardComponent } from "./Dashboard";
export { routes } from "./customRoutes";
const enhance = withStyles({ root: { maxWidth, minWidth } });
export const Resource = enhance(({ classes, ...props }: any) => (
  <div className={classes.root}>
    <_Resource {...props} />
  </div>
));
