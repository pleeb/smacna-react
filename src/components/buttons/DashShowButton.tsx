import React from "react";
import Icon from "@material-ui/icons/ChromeReaderMode";
import { DashButton, Enum } from "./";

export const DashShowButton = props => (
  <DashButton
    Icon={Icon}
    crud={Enum.Crud.Read}
    label="Dash"
    record={props.record}
    resource={props.resource}
    variant="flat"
  />
);
