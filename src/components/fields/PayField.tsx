import React from "react";
import get from "lodash/get";
import TextField from "@material-ui/core/TextField";
import withStyles from "@material-ui/core/styles/withStyles";
import { Enum, parse } from "../";
import { Filter as _Filter } from "react-admin";

export const Filter = withStyles({
  root: { marginTop: 0, alignItems: "flex-start" }
})(({ classes, ...props }) => <_Filter {...props} className={classes.root} />);
const InputProps = {
  fontWeight: "bold",
  fontSize: "0.875rem",
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  lineHeight: "1.46429em",
  margin: 0,
  display: "block"
} as any;
const styles = {
  root: {
    marginTop: 16,
    marginBottom: 8
  },
  InputLabel: {
    fontWeight: "bold",
    textTransform: "uppercase"
  }
};
const getMoneyClass = (classes, amount: number) => {
  return typeof amount !== "number" ? classes.BAD_VALUE : classes[Math.sign(amount)];
};
export const PayField = withStyles(theme => ({
  input: {
    width: "100%",
    fontSize: 44
  },
  root: {
    color: theme.palette.primary.main
  },
  error: {
    color: theme.palette.error.main
  }
}))(({ value, record, source, classes, label, error, help }: any) => {
  let amount = 0;
  if (typeof value === "number") amount = value;
  else if (record && source) amount = get(record, source);
  return (
    <TextField
      // fullWidth
      style={styles.root}
      error={error}
      InputProps={{ readOnly: true, classes }}
      InputLabelProps={{ style: styles.InputLabel as any }}
      value={parse.money(amount)}
      label={label}
      helperText={error && <strong>{help}</strong>}
    />
  );
});
export const PaymentField = withStyles(theme => ({
  BAD_VALUE: { ...InputProps, color: Enum.ColorMap.BAD_VALUE },
  amount_total: { ...InputProps, color: Enum.ColorMap.Money.AmountTotal },
  amount_paid: { ...InputProps, color: Enum.ColorMap.Money.AmountPaid },
  "0": { ...InputProps, color: Enum.ColorMap.Money.Zero },
  "-1": { ...InputProps, color: Enum.ColorMap.Money.UnderPay },
  "1": { ...InputProps, color: Enum.ColorMap.Money.OverPay }
}))(({ value, record, source, classes, label }: any) => {
  let amount = 0;
  if (typeof value === "number") amount = value;
  else if (record && source) amount = get(record, source);
  const className = source ? classes[source] : getMoneyClass(classes, value);
  return (
    <TextField
      margin="normal"
      fullWidth
      InputProps={{ readOnly: true, disableUnderline: true, className }}
      value={parse.money(amount)}
      label={label}
    />
  );
});
