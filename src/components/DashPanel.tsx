import React from "react";

import compose from "recompose/compose";

import Drawer from "@material-ui/core/Drawer";
import Dialog from "@material-ui/core/Dialog";

import { push } from "react-router-redux";
import { WithPermissions } from "react-admin";

import { connect } from "react-redux";
import { destroy } from "redux-form";
import { withStyles } from "@material-ui/core";
import { Crud, Anchor, Resource, Role } from "../enums";
import { Const, Enum } from "./";
import { dashAction } from "../actions/dash";
const enhance = compose(
  withStyles({
    drawer: {
      maxHeight: Const.HORIZONTAL_DASH_MAX_HEIGHT
    },
    drawerThin: {
      maxWidth: Const.VERTICAL_DASH_MAX_WIDTH
    },
    dialog: {
      maxWidth: "fit-content"
    }
  }),
  connect(
    ({ dash, router: { location }, session: user }: any, { anchor, resource, crud: _crud = Enum.Crud.None }: any) => {
      const test = dash[resource][_crud];
      const { record, open, crud } = test;
      const { id: user_id, company_id, role } = user;
      return { record, open: open && _crud === crud, crud, anchor, user_id, company_id, role, location, user };
    },
    (dispatch, { crud = Enum.Crud.None, resource }) => ({
      closeDash: () => {
        dispatch(dashAction[resource][crud].close());
        if (crud === Crud.Update || crud === Crud.Create) {
          dispatch(destroy("record-form"));
        }
      }
    })
  )
);
interface IProps {
  anchor?: Anchor;
  resource: Resource;
  crud?: Crud;
  dialog?: boolean;
}
export const DashPanel = enhance(
  ({
    dialog = false,
    classes,
    open,
    children,
    closeDash,
    anchor,
    record,
    resource,
    crud = Enum.Crud.None,
    user_id,
    company_id,
    role,
    location,
    user
  }: any) => {
    return dialog ? (
      <Dialog classes={{ paper: classes.dialog }} onClose={closeDash} open={open} title="Dash Dialog Test">
        {open ? (
          <WithPermissions
            authParams={{ record, resource, crud, user_id, company_id, role }}
            // location is not required but it will trigger a new permissions check if specified when it changes
            // location={location}
            render={({ permissions }) =>
              React.cloneElement(children, {
                id: record.id,
                user,
                user_id,
                company_id,
                permissions,
                anchor,
                record,
                resource,
                crud,
                location
              })
            }
          />
        ) : (
          ""
        )}
      </Dialog>
    ) : (
      <Drawer
        classes={{ paper: anchor === Anchor.Right || anchor === Anchor.Left ? classes.drawerThin : classes.drawer }}
        variant="temporary"
        open={open}
        anchor={anchor}
        onClose={closeDash}
      >
        {open ? (
          <WithPermissions
            authParams={{ record, resource, crud, user_id, company_id, role }}
            // location is not required but it will trigger a new permissions check if specified when it changes
            // location={location}
            render={({ permissions }) =>
              React.cloneElement(children, {
                id: record.id,
                user_id,
                company_id,
                permissions,
                anchor,
                record,
                resource,
                crud,
                location
              })
            }
          />
        ) : null}
      </Drawer>
    );
  }
) as React.StatelessComponent<IProps>;
