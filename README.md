# SMACNA

## SMACNA API GUIDELINES

### REQUEST => METHOD API CALLS

These are the methods and api calls that you will be exposed to.
(n) denotes multiple requests

- GET_LIST => `GET http://my.api.url/posts?sort=['title','ASC']&range=[0, 24]&filter={title:'bar'}`
- GET_ONE => `GET http://my.api.url/posts/123`
- CREATE => `POS http://my.api.url/posts/123`
- UPDATE => `PUT http://my.api.url/posts/123`
- UPDATE_MANY => `PUT(n) http://my.api.url/posts/123`
- DELETE => `DELETE http://my.api.url/posts/123`
- DELETE_MANY => `DELETE(n)http://my.api.url/posts/123`
- GET_MANY => `GET http://my.api.url/posts?filter={ids:[123,456,789]}`
- GET_MANY_REFERENCE => `GET http://my.api.url/posts?filter={author_id:345}`

#### Notes:

API should include a Content-Range header in the response to GET_LIST calls. The value must be the total number of resources in the collection. This allows for building of the pagination controls.

`Content-Range: posts 0-24/319`

If your API is on another domain as the JS code, you’ll need to whitelist this header with an Access-Control-Expose-Headers CORS header.

`Access-Control-Expose-Headers: Content-Range`

The request types like GET_LIST or CREATE are the kind of like semantic request identifiers that are being used on the frontend.

They won't be exposed to the backend; they are included to provide better context of the purpose of each api call.

### REQUEST / METHOD => RESPONSE FORMAT

These are the standard expected return shapes for all api requests.

`type Mixed = string | number;`

`interface IRecord { id: number; . . . }`

IRecord is an object literal with at least an id property, e.g. `{ id: 123, title: "hello, world" }`.

- GET_LIST / GET => `{ data: IRecord[], total: number }`
- GET_ONE / GET => `{ data: IRecord }`
- CREATE / POST => `{ data: IRecord }`
- UPDATE / PUT => `{ data: IRecord }`
- UPDATE_MANY / PUT => `{ data: Mixed[] }` The ids which have been updated.
- DELETE / DELETE => `{ data: IRecord }`
- DELETE_MANY / DELETE => `{ data: Mixed[] }` The ids which have been deleted.
- GET_MANY / GET => `{ data: IRecord]} }`
- GET_MANY_REFERENCE / GET => `{ data: IRecord]}, total: number }`
