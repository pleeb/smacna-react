import React, { Fragment, Component } from "react";
import { change } from "redux-form";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { SearchInput } from "react-admin";
import {
  Button,
  withStyles,
  Select,
  FormControl,
  InputLabel,
  FormHelperText,
  MenuItem,
  Input,
  TextField,
  InputAdornment
} from "@material-ui/core";
import { Alias, Enum, stringOperators as operators, required, OperatorSelector } from "./";

const { StringOperator: Operator } = Enum;
interface ISearchLike {
  alwaysOn?: boolean;
  placeholder: string;
  source: string;
  strategy?: "starts" | "ends" | "contains";
}
const parseStrategy = {
  [Operator.STARTS_WITH]: (v?: string) => (v ? `${v}%` : ""),
  [Operator.ENDS_WITH]: (v?: string) => (v ? `%${v}` : ""),
  [Operator.CONTAINS]: (v?: string) => (v ? `%${v}%` : "")
};
const defaultFormat = (v?: string) => (v ? v.replace(/%/g, "") : "");

type Props<Source = any> = {
  source: Source;
  dispatch: any;
  label?: string;
  defaultOperator?: Enum.StringOperator;
  noOptions?: boolean;
  classes?: any;
  meta?: any;
};
class OperatorInput extends Component<Props, any> {
  state = {
    operator: this.props.defaultOperator || Operator.STARTS_WITH
  };
  private get sourceLike() {
    return this.props.source + ".$like";
  }
  private onChange = event => {
    const operator = event.target.value;
    if (operator === Operator.NOOP) {
      this.props.dispatch(change(this.props.meta.form, this.sourceLike, ""));
    } else {
      this.props.dispatch(change(this.props.meta.form, this.sourceLike, ""));
    }
    this.setState({ operator, operators });
  };
  render() {
    const { source, classes, label, noOptions } = this.props;
    const operator = this.state.operator;
    const disabled = operator === Operator.NOOP;
    return (
      <div className={classes.container}>
        <TextField
          select={false}
          className={classes.input}
          value={label || Alias[source]}
          label="Search String"
          InputProps={{ readOnly: true, disabled, style: { width: 125 } }}
        />
        {noOptions ? null : <OperatorSelector readonly={false} value={operator} onChange={this.onChange} choices={operators} />}
        <SearchInput
          InputProps={{
            style: { width: 175 }
          }}
          source={this.sourceLike}
          format={defaultFormat}
          parse={parseStrategy[operator]}
          placeholder={Alias[operator]}
        />
      </div>
    );
  }
}
const styles = {
  container: { display: "flex", width: "100%", maxWidth: 425 },
  input: { marginTop: 16, marginBottom: 8, width: 125 }
};
const enhance = compose(
  connect(
    null,
    dispatch => ({ dispatch })
  ),
  withStyles(styles)
);
export const StringSearchInput = enhance(OperatorInput);
