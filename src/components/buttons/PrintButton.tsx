import React from "react";
import ReactDOMServer from "react-dom/server";
import { connect } from "react-redux";
import print from "print-html-element";
// import { Button } from "@material-ui/core";
import { Button } from "react-admin";
import Icon from "@material-ui/icons/Print";
import phone from "google-libphonenumber";
import { stylize } from "./DashButton";
import { printRecord } from "../../actions/print";
import { dataProvider } from "../../data/dataProvider";

const PNF = phone.PhoneNumberFormat;
const phoneUtil = phone.PhoneNumberUtil.getInstance();

const asyncPrint = async ({ id }) => {
  console.log("FETCH", id);
  const { data: record } = await dataProvider("GET_ONE", "payments", { id });
  console.log("RESPONSE", record);
  print.printHtml(ReactDOMServer.renderToString(<PrintPayment record={record} />), {
    pageTitle: "Employer's Report of Contribution Payment Receipt"
  });
};

export const PrintButton = (({ record, ...props }: any) => {
  return (
    <Button
      size="small"
      color="primary"
      label="Print"
      onClick={e => {
        e.stopPropagation();
        asyncPrint(record);
      }}
    >
      <Icon />
    </Button>
  );
}) as any;
const PrintPayment = ({ record }: any) => {
  const company = record.Company;
  return (
    <table
      style={{
        width: "100%"
      }}
    >
      <tbody>
        <tr>
          <td style={{ height: 15 }} />
        </tr>
        <tr>
          <td align="center" style={{ fontSize: 20 }}>
            <strong>SHEET METAL PROMOTION FUND OF NEW YORK CITY</strong>
          </td>
        </tr>
        <tr>
          <td style={{ height: 5 }} />
        </tr>
        <tr>
          <td align="center" style={{ fontSize: 18 }}>
            Employer's Report of Contribution Payment Receipt
          </td>
        </tr>
        <tr>
          <td style={{ height: 15 }} />
        </tr>
        <tr>
          <td>
            <table
              style={{
                width: "100%",
                tableLayout: "fixed"
              }}
            >
              <tbody>
                <tr
                  style={{
                    verticalAlign: "top"
                  }}
                >
                  <td>
                    <RenderAddress {...company} label="Employer:" />
                  </td>
                  <td>
                    <RenderAddress {...smacna} label="Make check payable to:" />
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style={{ height: 25 }} />
        </tr>
        <tr>
          <td>
            <RenderTable record={record.Contribution} />
          </td>
        </tr>
        <tr>
          <td style={{ height: 25 }} />
        </tr>
        <tr>
          <td>
            <RenderRecord label="Record:" record={record} />
          </td>
        </tr>
      </tbody>
    </table>
  );
};
const smacna = {
  company_name: "SHEET METAL INDUSTRY PROMOTION FUND",
  address: "16 Court Street, Suite 2100",
  city: "Brooklyn",
  state: "NY",
  zipcode: "11241",
  phone: "718-624-1234",
  fax: "718-855-9468"
};
const RenderValue = props => <span style={{ paddingLeft: "0 10px" }}>{props.value || <span>&mdash;</span>}</span>;
const RenderAddress = ({ label, company_name, address, city, state, zipcode, phone, fax }: any) => (
  <table
    style={{
      width: "100%",
      textAlign: "left",
      fontSize: 14
    }}
    cellSpacing={4}
  >
    <tbody>
      <tr>
        <td />
        <td style={{ fontSize: 12 }}>
          <strong>{label}</strong>
        </td>
      </tr>
      <tr>
        <td />
        <td>
          <strong>{company_name}</strong>
        </td>
      </tr>
      <tr>
        <td />
        <td>{address}</td>
      </tr>
      <tr>
        <td />
        <td>
          {city}, {state} {zipcode}
        </td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Phone</td>
        <td>
          <RenderPhone value={phone} />
        </td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Fax</td>
        <td>
          <RenderPhone value={fax} />
        </td>
      </tr>
    </tbody>
  </table>
);
const RenderPhone = ({ value }) =>
  value ? (
    <span>{phoneUtil.format(phoneUtil.parseAndKeepRawInput(value, "US"), PNF.NATIONAL)}</span>
  ) : (
    <span style={{ paddingLeft: 14 }}>----</span>
  );
const RenderUser = ({ full_name, title, email, phone }) => (
  <table
    style={{
      textAlign: "left",
      width: "100%",
      fontSize: 14
    }}
    cellSpacing={4}
  >
    <tbody>
      <tr>
        <td style={{ fontSize: 12 }}>Name</td>
        <td>
          <strong>{full_name}</strong>
        </td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Title</td>
        <td>{title}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Email</td>
        <td>{email}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Phone</td>
        <td>
          <RenderPhone value={phone} />
        </td>
      </tr>
      <tr>
        <td />
        <td />
      </tr>
      <tr>
        <td />
        <td />
      </tr>
    </tbody>
  </table>
);
const RenderContexts = ({
  reporting_week_end,
  reporting_week,
  reporting_week_count,
  amount_paid,
  amount_owed,
  amount_total,
  payment_date
}) => (
  <table
    style={{
      textAlign: "left",
      width: "100%",
      fontSize: 14
    }}
    cellSpacing={4}
  >
    <tbody>
      <tr>
        <td style={{ fontSize: 12 }}>Week Ending Date</td>
        <td>{new Date(reporting_week_end).toLocaleDateString()}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Week Number</td>
        <td>{reporting_week}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Number of Weeks</td>
        <td>{reporting_week_count}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Payment Date</td>
        <td>{new Date(payment_date).toLocaleDateString()}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>
          <strong>Amount Total</strong>
        </td>
        <td>
          <strong>${(amount_total || 0).toFixed(2)}</strong>
        </td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>
          <strong>Amount Paid</strong>
        </td>
        <td>
          <strong>${(amount_paid || 0).toFixed(2)}</strong>
        </td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>
          <strong>Amount Owed</strong>
        </td>
        <td>
          <strong>${(amount_owed || 0).toFixed(2)}</strong>
        </td>
      </tr>
    </tbody>
  </table>
);
const RenderIds = ({ payment_id, contribution_id, cid, company_id, user_id }) => (
  <table
    style={{
      textAlign: "left",
      width: "100%",
      fontSize: 14
    }}
    cellSpacing={4}
  >
    <tbody>
      {payment_id && (
        <tr>
          <td style={{ fontSize: 12 }}>Payment ID</td>
          <td>{payment_id}</td>
        </tr>
      )}
      <tr>
        <td style={{ fontSize: 12 }}>Contribution ID</td>
        <td>{contribution_id}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Company ID</td>
        <td>{company_id}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Contribution CID</td>
        <td>{cid}</td>
      </tr>
      {user_id && (
        <tr>
          <td style={{ fontSize: 12 }}>User ID</td>
          <td>{user_id}</td>
        </tr>
      )}
      <tr>
        <td />
        <td />
      </tr>
    </tbody>
  </table>
);
const RenderRecord = ({ record, label }) => {
  const contribution = record.Contribution;
  const user = record.User;
  const payment = record;
  return (
    <table
      style={{
        textAlign: "left",
        fontSize: 14,
        tableLayout: "fixed",
        width: "100%"
      }}
      cellSpacing={4}
    >
      <tbody>
        <tr>
          <td style={{ fontSize: 12 }}>
            <strong>{label}</strong>
          </td>
          <td />
          <td />
        </tr>
        <tr>
          <td>
            <RenderContexts payment_date={record.payment_date} {...contribution} />
          </td>
          <td style={{ verticalAlign: "top" }}>
            <RenderIds
              contribution_id={contribution.id}
              cid={contribution.cid}
              payment_id={payment ? payment.id : null}
              company_id={contribution.company_id}
              user_id={user ? user.id : null}
            />
          </td>
          <td style={{ verticalAlign: "top" }}>{user && <RenderUser {...user} />}</td>
        </tr>
        <tr>
          <td style={{ height: 44 }} />
        </tr>
        <tr>
          <td style={{ fontSize: 16 }}>
            <strong>Payment Date</strong>
          </td>
          <td style={{ fontSize: 14 }}>{new Date(record.payment_date).toLocaleDateString()}</td>
        </tr>
        <tr>
          <td style={{ fontSize: 16 }}>
            <strong>Payment Amount Total</strong>
          </td>
          <td style={{ fontSize: 14 }}>{record.amount_total}</td>
        </tr>
        <tr>
          <td style={{ fontSize: 16 }}>
            <strong>Payment Amount Paid</strong>
          </td>
          <td style={{ fontSize: 14 }}>{record.amount_paid}</td>
        </tr>
      </tbody>
    </table>
  );
};
const RenderTable = ({ record }) => (
  <table
    style={{
      width: "100%",
      textAlign: "left",
      tableLayout: "fixed",
      fontSize: 14
    }}
    cellSpacing={10}
  >
    <thead>
      <tr>
        <th style={{ fontSize: 12 }}>{null}</th>
        <th style={{ textAlign: "center", fontSize: 12 }}>Building Trade Mechanics</th>
        <th style={{ textAlign: "center", fontSize: 12 }}>Apprentice Workers</th>
        <th style={{ textAlign: "center", fontSize: 12 }}>Light Commercial Workers</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style={{ fontSize: 12 }}>Number of Employees</td>
        <td style={{ textAlign: "center" }}>{record.num_mechanic}</td>
        <td style={{ textAlign: "center" }}>{record.num_apprentice}</td>
        <td style={{ textAlign: "center" }}>{record.num_light_commercial}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>+ Hours Paid</td>
        <td style={{ textAlign: "center" }}>{record.hours_mechanic}</td>
        <td style={{ textAlign: "center" }}>{record.hours_apprentice}</td>
        <td style={{ textAlign: "center" }}>{record.hours_light_commercial}</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>&ndash; Target Hours</td>
        <td style={{ textAlign: "center" }}>{record.hours_target}</td>
        <td style={{ textAlign: "center" }}>&mdash;</td>
        <td style={{ textAlign: "center" }}>&mdash;</td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Contribution Hour</td>
        <td style={{ textAlign: "center" }}>
          ${(record.rate_mechanic || 0).toFixed(2)} x ({record.hours_mechanic} - {record.hours_target})
        </td>
        <td style={{ textAlign: "center" }}>
          ${(record.rate_apprentice || 0).toFixed(2)} x {record.hours_apprentice}
        </td>
        <td style={{ textAlign: "center" }}>
          ${(record.rate_light_commercial || 0).toFixed(2)} x {record.hours_light_commercial}
        </td>
      </tr>
      <tr>
        <td style={{ fontSize: 12 }}>Contribution Due</td>
        <td style={{ textAlign: "center" }}>${(record.amount_mechanic || 0).toFixed(2)}</td>
        <td style={{ textAlign: "center" }}>${(record.amount_apprentice || 0).toFixed(2)}</td>
        <td style={{ textAlign: "center" }}>${(record.amount_light_commercial || 0).toFixed(2)}</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td style={{ fontSize: 12 }}>
          <strong>Contribution Total</strong>
        </td>
        <td style={{ textAlign: "center" }}>
          <strong>${(record.amount_total || 0).toFixed(2)}</strong>
        </td>
        <td />
        <td />
      </tr>
    </tfoot>
  </table>
);
