import React, { Fragment, Component } from "react";
import { change } from "redux-form";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { withStyles, Select, FormControl, InputLabel, FormHelperText, MenuItem, Input, TextField, InputAdornment } from "@material-ui/core";
import { OperatorSelector, numberOperators, CompanyDateField, ContributionDateField, required, Enum, Alias } from "./";

type SourceField = {
  [props: string]: Object | string;
};
type TProps<Source extends "companies" | "contributions", Field extends CompanyDateField | ContributionDateField> = {
  source: Source;
  adornStart?: string;
  adornEnd?: string;
  field?: Field;
  label?: string;
  InputComponent: React.ComponentType<any>;
  dispatch: Function;
  defaultOperator?: Enum.NumberOperator;
  defaultStart?: string;
  defaultEnd?: string;
  classes?: any;
  fieldMap?: any;
  meta?: any;
};
class OperatorInput extends Component<TProps<"companies", CompanyDateField> | TProps<"contributions", ContributionDateField>, any> {
  state = {
    operator: this.props.defaultOperator || null,
    operators: this.getOperators(this.props.defaultOperator || Enum.NumberOperator.NOOP)
  };
  private join(...paths: string[]): string {
    return paths.filter(e => e).join(".");
  }
  private getOperators(operator: Enum.NumberOperator): [Enum.NumberOperator, Enum.NumberOperator] {
    let o1: Enum.NumberOperator = undefined,
      o2: Enum.NumberOperator = undefined;
    switch (operator) {
      case Enum.NumberOperator.BETWEEN:
        o1 = Enum.NumberOperator.GREATER_THAN_EQUAL;
        o2 = Enum.NumberOperator.LESS_THAN;
        break;
      case Enum.NumberOperator.NOT_BETWEEN:
        o1 = Enum.NumberOperator.LESS_THAN;
        o2 = Enum.NumberOperator.GREATER_THAN;
        break;
      case Enum.NumberOperator.GREATER_THAN:
      case Enum.NumberOperator.GREATER_THAN_EQUAL:
      case Enum.NumberOperator.LESS_THAN:
      case Enum.NumberOperator.LESS_THAN_EQUAL:
      case Enum.NumberOperator.EQUAL:
      case Enum.NumberOperator.NOT_EQUAL:
        o1 = operator;
        o2 = undefined;
        break;
      default:
        o1 = undefined;
        o2 = undefined;
        break;
    }
    return [o1, o2];
  }
  private onChange = event => {
    const operator = event.target.value;
    const operators = this.getOperators(operator);
    const pathto = this.join(this.props.source, this.props.field);
    if (operator === Enum.NumberOperator.NOOP) {
      this.props.dispatch(change(this.props.meta.form, pathto, {}));
    } else {
      const fields: any = {};
      operators.map(e => e && (fields[e] = ""));
      this.props.dispatch(change(this.props.meta.form, pathto, fields));
    }
    this.setState({ operator, operators });
  };
  private deriveOperator(o: Enum.NumberOperator[]): Enum.NumberOperator {
    switch (o.length) {
      case 0:
        return Enum.NumberOperator.NOOP;
      case 1:
        return o[0];
      case 2:
        if (o.includes(Enum.NumberOperator.GREATER_THAN_EQUAL) && o.includes(Enum.NumberOperator.LESS_THAN))
          return Enum.NumberOperator.BETWEEN;
        if (o.includes(Enum.NumberOperator.GREATER_THAN) && o.includes(Enum.NumberOperator.LESS_THAN))
          return Enum.NumberOperator.NOT_BETWEEN;
        throw "illegal: bad operator combination " + o.join(", ");
      default:
        throw "illegal: more than two operator values " + o.join(", ");
    }
  }
  private deriveOperators(map: SourceField): { operator: Enum.NumberOperator; operators: Enum.NumberOperator[] } {
    const fields = Object.keys(map) as Enum.NumberOperator[];
    const operator = this.deriveOperator(fields);
    return { operator, operators: this.getOperators(operator) };
  }
  render() {
    const {
      label,
      source,
      field,
      fieldMap,
      classes,
      defaultOperator,
      defaultStart,
      defaultEnd,
      InputComponent,
      adornStart,
      adornEnd
    } = this.props;
    const {
      operator,
      operators: [o1, o2]
    } = this.state.operator === null ? this.deriveOperators(fieldMap) : this.state;
    const disabled = operator === Enum.NumberOperator.NOOP;
    return (
      <div className={classes.container}>
        <TextField
          // fullWidth
          value={label || Alias[field || source]}
          label="Search Range"
          className={classes.input}
          InputProps={{ readOnly: true, disabled, style: { width: 125 } }}
        />
        <OperatorSelector readonly={!!defaultOperator} value={operator} onChange={this.onChange} choices={numberOperators} />
        {o1 ? (
          <InputComponent
            InputProps={{
              startAdornment: <InputAdornment position="start">{adornStart || ""}</InputAdornment>,
              endAdornment: <InputAdornment position="end">{adornEnd || ""}</InputAdornment>,
              style: { width: 175 }
            }}
            defaultValue={defaultStart || null}
            source={this.join(source, field, o1)}
            label={Alias[o1]}
            validate={required}
          />
        ) : null}
        {o2 ? (
          <InputComponent
            InputProps={{
              startAdornment: <InputAdornment position="start">{adornStart || ""}</InputAdornment>,
              endAdornment: <InputAdornment position="end">{adornEnd || ""}</InputAdornment>,
              style: { width: 175 }
            }}
            defaultValue={defaultEnd || null}
            source={this.join(source, field, o2)}
            label={Alias[o2]}
            validate={required}
          />
        ) : null}
      </div>
    );
  }
}
const styles = {
  container: { display: "flex", width: "100%", maxWidth: 600 },
  input: { marginTop: 16, marginBottom: 8, width: 125 }
};
const enhance = compose(
  connect(
    (state: any, props: any) => {
      const form = state.form[props.meta.form].values || {};
      const source = form[props.source] || {};
      const fieldMap = source[props.field] || {};
      return {
        fieldMap
      };
    },
    dispatch => ({ dispatch })
  ),
  withStyles(styles)
);
export const BaseRangeInput = enhance(OperatorInput);
