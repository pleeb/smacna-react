import { CLOSE_ALL_DASH, OPEN_DASH, CLOSE_DASH, cruds, resources } from "../actions/dash";
import { Crud, Resource } from "../enums";
import * as rr from "react-router-dom";
import { combineReducers } from "redux";

const isSentinelClose = type => {
  switch (type) {
    case "RA/CRUD_CREATE_SUCCESS":
    case "RA/CRUD_UPDATE_OPTIMISTIC":
    case "RA/CRUD_UPDATE_SUCCESS":
    case CLOSE_ALL_DASH:
      return true;
    default:
      return false;
  }
};
const ff_dash = (_resource: Resource, _crud: Crud) => (
  state = { open: false, record: null, crud: null },
  { type, record, resource, crud, meta, ...rest }
) => {
  if (isSentinelClose(type)) return { open: false, record: null, crud, resource };
  if (resource !== _resource || crud !== _crud) return state;
  switch (type) {
    case OPEN_DASH:
      return { open: true, record, crud, resource };
    case CLOSE_DASH:
      return { open: false, record: null, crud, resource };
    default:
      return state;
  }
};
const reducers: any = {};
resources.map(resource => {
  cruds.map(crud => ((reducers[resource] ? reducers[resource] : (reducers[resource] = {}))[crud] = ff_dash(resource, crud)));
  reducers[resource] = combineReducers(reducers[resource]);
});

export const dash = combineReducers(reducers);
