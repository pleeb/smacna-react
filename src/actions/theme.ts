export const CHANGE_THEME = "CHANGE_THEME";

export type Theme = "light" | "dark";

export const changeTheme = (theme: Theme) => ({
  type: CHANGE_THEME,
  theme
});
