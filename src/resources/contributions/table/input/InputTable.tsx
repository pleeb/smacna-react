import { React, component } from "../";
import { InputNumHours } from "./InputNumHours";
import { InputNumWorkers } from "./InputNumWorkers";
import { ConnectedFieldAmountCalculation } from "./ConnectedFieldAmountCalculation";
import { ConnectedFieldAmountDue } from "./ConnectedFieldAmountDue";

const { Mdash, Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow, tableStyles } = component;

export const InputTable = tableStyles(({ classes, ...props }: any) => {
  return (
    <Card className={classes.card}>
      <Table padding="checkbox" className={classes.table}>
        <TableHead className={classes.head}>
          <TableRow>
            <TableCell>{null}</TableCell>
            <TableCell className={classes.headCell} numeric>
              <strong>Building Trade Mechanics</strong>
            </TableCell>
            <TableCell className={classes.headCell} numeric>
              <strong>Apprentice Workers</strong>
            </TableCell>
            <TableCell className={classes.headCell} numeric>
              <strong>Light Commercial Workers</strong>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>Number of Employees</TableCell>
            <TableCell numeric>
              <InputNumWorkers source="num_mechanic" />
            </TableCell>
            <TableCell numeric>
              <InputNumWorkers source="num_apprentice" />
            </TableCell>
            <TableCell numeric>
              <InputNumWorkers source="num_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>+ Total Hours Paid</TableCell>
            <TableCell numeric>
              <InputNumHours source="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <InputNumHours source="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <InputNumHours source="hours_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>&ndash; Target Hours</TableCell>
            <TableCell numeric>
              <InputNumHours source="hours_target" />
            </TableCell>
            <TableCell numeric>
              <Mdash />
            </TableCell>
            <TableCell numeric>
              <Mdash />
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Contribution Hour</TableCell>
            <TableCell numeric>
              <ConnectedFieldAmountCalculation source="hours_mechanic" />
            </TableCell>
            <TableCell numeric>
              <ConnectedFieldAmountCalculation source="hours_apprentice" />
            </TableCell>
            <TableCell numeric>
              <ConnectedFieldAmountCalculation source="hours_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow className={classes.amountRow}>
            <TableCell>Contribution Due</TableCell>
            <TableCell numeric className={classes.amountCell}>
              <ConnectedFieldAmountDue source="amount_mechanic" />
            </TableCell>
            <TableCell numeric className={classes.amountCell}>
              <ConnectedFieldAmountDue source="amount_apprentice" />
            </TableCell>
            <TableCell numeric className={classes.amountCell}>
              <ConnectedFieldAmountDue source="amount_light_commercial" />
            </TableCell>
          </TableRow>
          <TableRow className={classes.amountTotalRow}>
            <TableCell>
              <strong>Contribution Total</strong>
            </TableCell>
            <TableCell numeric className={classes.totalCell}>
              <ConnectedFieldAmountDue source="amount_total" />
            </TableCell>
            <TableCell numeric />
            <TableCell numeric />
          </TableRow>
        </TableBody>
      </Table>
    </Card>
  );
});
