import process from "process";
const HOST_IP = "157.230.95.18";
const API_PORT = 3000;
const SERVE_PORT = 8080;
enum Env {
  Dev = "development",
  Prod = "production"
}
// PRODUCTION
// internal reverse proxy server redirects / and /v1 to :8080/ and :3000/v1 respectively
const PROD_HOST = `http://${HOST_IP}`;
const PROD_API = `http://${HOST_IP}/v1`;
// DEVELOPMENT
//
const DEV_HOST = "http://localhost:8080";
const DEV_API = "http://localhost:3000/v1";

// SET ENV HERE TO ASSIGN CORRECT CONFIGS
// TODO: detect and correctly assign env variable to reflect local and remote env.
// local env should always be dev.

let env: Env;
if (process) {
  env = process.env.NODE_ENV === "development" ? Env.Dev : Env.Prod;
}
if (document) {
  env = document.location.hostname === "localhost" ? Env.Dev : Env.Prod;
}

export const HOST_URL = env === Env.Dev ? DEV_HOST : PROD_HOST;
export const API_URL = env === Env.Dev ? DEV_API : PROD_API;

console.log(env, "env");
console.log("Using host url:", HOST_URL);
console.log("Using api url:", API_URL);

if (!env) throw "env could not be determined";
