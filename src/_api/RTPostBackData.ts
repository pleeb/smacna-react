import { Alphanumeric, Numeric, Decimal, DateTime } from "./";

/*
************ FREQUENTLY USED DATA FIELDS ************
Reference Number – Account Number (Authentication Value)
Company Name – Customer Name
Total – Payment Amount
Process Date – Date Payment Will Process
Create Date – Dated Payment Created
Biller Invoice Number
Transaction ID – Confirmation Number
Payment Account Type & Method – ACH, Card, SwipeCard, AmazonPay, PayPal, eLockbox
Payment Account Last Four – Last 4 digits of payment account
Payment Status – Status of Payment (Pending, Declined, Returned, etc.)
Payment Channel Type (Enrolled. One-Time, Future Dated, Recurring, CSR, IVR, etc.)
Client ID (GUID)
*/

/*
************ Customer ************
ExternalUserID Alphanumeric 128 Unique user ID that identifies this customer in an external system
FirstName Alphanumeric 32 First Name
LastName Alphanumeric 32 Last Name
Email Alphanumeric 128 Customer’s email address
Phone Alphanumeric 20 Customer's primary phone number

Customer
  ExternalUserID Alphanumeric
  FirstName Alphanumeric
  LastName Alphanumeric
  Email Alphanumeric
  Phone Alphanumeric
*/
export interface Customer {
  ExternalUserID: Alphanumeric;
  FirstName: Alphanumeric;
  LastName: Alphanumeric;
  Email: Alphanumeric;
  Phone: Alphanumeric;
}
/*
************ Payment ************
BillerID Numeric 10 Biller ID
BillerInvoiceNo Alphanumeric 50 Invoice Number generated by the biller
Last four characters of Reference Number left padded with asterisks to the length of the full Billing Account / Policy Number.
CreateDate Numeric 14 Date & Time payment record was created
CustomerName Alphanumeric 256 Payer’s Name.
Email Alphanumeric 255 Payer’s Email
ExternalUserID Alphanumeric 128 Unique user ID that identifies this customer in an external system
Status Numeric 10 See Status Description below
Total Numeric 10 Total Payment Amount
TransactionID Numeric 19 Unique payment confirmation number

Payment
  BillerID Numeric
  BillerInvoiceNo Alphanumeric
  CreateDate Numeric
  CustomerName Alphanumeric
  Email Alphanumeric
  ExternalUserID Alphanumeric
  Status Numeric
  Total Numeric
  TransactionID Numeric
*/
export interface Payment {
  BillerID: Numeric;
  BillerInvoiceNo: Alphanumeric;
  Channel: Alphanumeric;
  CreateDate: Numeric;
  CustomerName: Alphanumeric;
  Email: Alphanumeric;
  ExternalUserID: Alphanumeric;
  Status: Numeric;
  Total: Numeric;
  TransactionID: Numeric;
}
/*
************ Banking/Credit/Payment/Account ************
Account Alphanumeric 256 Bank Name if ACH or Card Type if CC (i.e. “Visa”, “MasterCard”, etc.)
AccountNumberLastFour Numeric 4 Last four characters of Payment Account Number

Banking/Credit/Payment/Account
  Account Alphanumeric
  AccountNumberLastFour Numeric
*/
export interface Account {
  Account: Alphanumeric;
  AccountNumberLastFour: Numeric;
}
/*
************ Invoice ************
Amount Numeric 19 Amount is the payment amount for payments
AmountDue Numeric 19 Invoice Amount Due
CompanyName Alphanumeric 256 Customer Full Name or Company Name
DueDate Numeric 14 i.e. "2018-07-31T00:00:00.0000000-00:00"
PaymentAmount Numeric 19 PaymentAmount is the amount for payments or reversals depending on what the payment was
PaymentCode Alphanumeric 50 This is the optional payment code (Short Pay / Over Pay, etc.) that can be put against an invoice in the payment
PaymentCodeReason Alphanumeric 256 Payment Code Reason
StatementDate Numeric 14 Invoice Date

Invoice
  AccountNumber Numeric
  Amount Numeric
  AmountDue Numeric
  CompanyName Alphanumeric
  DueDate Numeric
  PaymentAmount Numeric
  PaymentCode Alphanumeric
  PaymentCodeReason Alphanumeric
  StatementDate Numeric
*/
export interface Invoice {
  AccountNumber: Numeric;
  Amount: Numeric;
  AmountDue: Numeric;
  CompanyName: Alphanumeric;
  DueDate: Numeric;
  PaymentAmount: Numeric;
  PaymentCode: Alphanumeric;
  PaymentCodeReason: Alphanumeric;
  StatementDate: Numeric;
}
