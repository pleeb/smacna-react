import React, { Fragment } from "react";
import { connect } from "react-redux";
import { DashPanel, Enum, Button } from "../";
import { withStyles, Typography, DialogActions, DialogContent, DialogTitle, DialogContentText } from "@material-ui/core";

const styles = theme => ({
  root: {
    position: "absolute" as any,
    bottom: 100,
    right: 100,
    minWidth: 0,
    zIndex: 5
  },
  text: {
    border: "1px solid lightgray",
    borderRadius: 5
  }
});
const Dialog = ({ user, record: { id, payment_id } }: any) => (
  <Fragment>
    <DialogActions style={{ justifyContent: "space-between" }}>
      <Typography style={{ marginLeft: 24 }} variant="title">
        Payment submitted.
      </Typography>
      <Button.DashCloseButton iconOnly />
    </DialogActions>
    <DialogContent>
      <DialogContentText>Thank you {user.first_name}, your payment was successfully submitted.</DialogContentText>
      <DialogContentText>
        A payment receipt will be sent to your email at <a href={`mailto:${user.email}`}>{user.email}</a>.
      </DialogContentText>
      <br />
      <DialogContentText>Please print a copy of your payment receipt and mail with your check.</DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button.PrintButton record={{ id, payment_id }} />
    </DialogActions>
  </Fragment>
);
export const PaymentDialog = props => (
  <DashPanel dialog resource={Enum.Resource.Payments} anchor={Enum.Anchor.Center} {...props}>
    <Dialog />
  </DashPanel>
);
