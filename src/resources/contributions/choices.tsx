import React from "react";
import { createChoice, Enum } from "../";

export const status = [createChoice("_", "All"), createChoice(Enum.Status.PAID), createChoice(Enum.Status.DELINQUENT)];
export const calculation = [createChoice(Enum.Calculation.ACTUAL), createChoice(Enum.Calculation.ESTIMATE)];
export const attribute = [
  createChoice("id"),
  createChoice("cid"),
  createChoice("company_id"),
  createChoice("reporting_week_end"),
  createChoice("reporting_year"),
  createChoice("reporting_week_count"),
  createChoice("reporting_week"),
  createChoice("rate_apprentice"),
  createChoice("rate_mechanic"),
  createChoice("rate_light_commercial"),
  createChoice("num_apprentice"),
  createChoice("num_mechanic"),
  createChoice("num_light_commercial"),
  createChoice("num_total"),
  createChoice("hours_apprentice"),
  createChoice("hours_mechanic"),
  createChoice("hours_light_commercial"),
  createChoice("hours_total"),
  createChoice("amount_apprentice"),
  createChoice("amount_mechanic"),
  createChoice("amount_light_commercial"),
  createChoice("amount_total"),
  createChoice("amount_paid"),
  createChoice("amount_owed"),
  createChoice("calculation"),
  createChoice("status"),
  createChoice("comment"),
  createChoice("receive_date"),
  createChoice("deposit_date"),
  createChoice("created_at"),
  createChoice("updated_at")
];
