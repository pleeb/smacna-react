import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core";
import {
  isMaster,
  Button,
  Alias,
  GridContainer,
  GridItem,
  sourceFieldFromMap,
  sourceField,
  SimpleShowContainer,
  SimpleFormContainer,
  Actionbar,
  Input,
  Enum,
  DashPanel
} from "../";

const connection = connect(
  ({ session: { id: user_id } }: any, { id }: any) => {
    return {
      title: +user_id === +id ? "Update" : "Edit"
    };
  },
  {}
);
const Actions = connection(({ title, permissions, ...props }: any) => {
  return <Actionbar verb={title + " User"} title={(props.data || {})["full_name"]} {...props} />;
});
const Toolbar = props => (
  <Input.Toolbar {...props}>
    <Button.SaveButton />
  </Input.Toolbar>
);
const Dash = connection(({ title, permissions, ...props }: any) => {
  return (
    <Input.Edit
      resource="users"
      basePath="/users"
      title={` ${title}`}
      {...props}
      actions={<Actions dash anchor={props.anchor} permissions={permissions} id={props.id} />}
    >
      <SimpleFormContainer redirect="show" toolbar={<Toolbar permissions={permissions} />}>
        <GridContainer>
          <GridItem xs>
            <Input.TextInput disabled source="id" label={Alias["id"]} />
            <Input.TextInput disabled source="company_id" label={Alias["company_id"]} />
            <Input.TextInput disabled source="username" label={Alias["username"]} />
            <Input.TextInput source="first_name" label={Alias["first_name"]} />
            <Input.TextInput source="last_name" label={Alias["last_name"]} />
            <Input.TextInput source="full_name" label={Alias["full_name"]} />
            <Input.TextInput source="title" label={Alias["title"]} />
            <Input.TextInput source="email" label={Alias["email"]} />
            <Input.TextInput source="phone" label={Alias["phone"]} />
          </GridItem>
          {isMaster(permissions) &&
            // <GridItem xs>
            // {null && <Input.TextInput source="role" label={Alias["role"]} />}
            // {null && <Input.TextInput source="created_at" label={Alias["created_at"]} />}
            // {null && <Input.TextInput source="updated_at" label={Alias["updated_at"]} />}
            // </GridItem>
            null}
        </GridContainer>
      </SimpleFormContainer>
    </Input.Edit>
  );
});
export const UserDashEdit = (props: any) => {
  return (
    <DashPanel resource={Enum.Resource.Users} anchor={Enum.Anchor.Right} crud={Enum.Crud.Update} {...props}>
      <Dash />
    </DashPanel>
  );
};
