import React from "react";
import { connect } from "react-redux";
import { CardActions } from "@material-ui/core";
import * as choices from "./choices";
import { isMaster, GridItem, GridContainer, Button, DashPanel, Enum, Alias, sourceField, Input, Actionbar, SimpleFormContainer } from "../";

const connection = connect(
  ({ session: { company_id } }: any, { id }: any) => {
    return {
      title: +company_id === +id ? "Update" : "Edit"
    };
  },
  {}
);
const Actions = connection(({ title, permissions, ...props }: any) => {
  const verb = title + " Company";
  return <Actionbar verb={verb} title={(props.data || {})["company_name"]} {...props} />;
});
const Test = props => {
  console.log(props);
  return null;
};
const Toolbar = props => (
  <Input.Toolbar {...props}>
    <Test />
    <Button.SaveButton />
  </Input.Toolbar>
);
const Dash = connection(({ title, permissions, anchor, ...props }: any) => {
  const disabled = permissions !== Enum.Role.User || permissions !== Enum.Role.Master;
  return (
    <Input.Edit
      resource="companies"
      basePath="/companies"
      title={` ${title}`}
      {...props}
      actions={<Actions dash anchor={anchor} permissions={permissions} id={props.id} />}
    >
      <SimpleFormContainer
        // {...props}
        // record={record}
        // className={classes.root}
        redirect="show"
        toolbar={<Toolbar permissions={props.permissions} />}
        // validate={validate}
      >
        <GridContainer>
          <GridItem xs>
            <Input.TextInput disabled={false} source="company_name" label={Alias["company_name"]} />
            <Input.TextInput disabled={false} source="address" label={Alias["address"]} />
            <Input.TextInput disabled={false} source="city" label={Alias["city"]} />
            <Input.TextInput disabled={false} source="state" label={Alias["state"]} />
            <Input.TextInput disabled={false} source="zipcode" label={Alias["zipcode"]} />
            <Input.TextInput disabled={false} source="county" label={Alias["county"]} />
            <Input.TextInput disabled={false} source="contact_name" label={Alias["contact_name"]} />
            <Input.TextInput disabled={false} source="phone" label={Alias["phone"]} />
            <Input.TextInput disabled={false} source="fax" label={Alias["fax"]} />
          </GridItem>
          {isMaster(permissions) && (
            <GridItem xs>
              <Input.SelectInput source="croup" label={Alias["croup"]} choices={choices.croup} />
              <Input.SelectInput
                source="association_code"
                label={Alias["association_code"]}
                choices={choices.associationCode}
                validate={associationCode}
              />
              <Input.BooleanInput source="association" label={Alias["association"]} />
              <Input.SelectInput source="work_code" label={Alias["work_code"]} choices={choices.workCode} />
              <Input.BooleanInput source="permanent" label={Alias["permanent"]} />
              <Input.BooleanInput source="ifus" label={Alias["ifus"]} />
              <Input.DateInput source="start_date" label={Alias["start_date"]} />
              <Input.DateInput source="outbus_date" label={Alias["outbus_date"]} />
              <Input.DateInput source="inactive_date" label={Alias["inactive_date"]} />
              <Input.DateInput source="chap11_date" label={Alias["chap11_date"]} />
            </GridItem>
          )}
        </GridContainer>
      </SimpleFormContainer>
    </Input.Edit>
  );
});
const associationCode = (value, values, a, b) => {
  if (values["association"] && !value) return "Association code must be specified.";
  if (!values["association"] && value) return "Association code must be unassigned.";
};
export const CompanyDashEdit = props => (
  <DashPanel resource={Enum.Resource.Companies} anchor={Enum.Anchor.Right} crud={Enum.Crud.Update} {...props}>
    <Dash />
  </DashPanel>
);
/*
<Field.TextField source="id" label={Alias["id"]} />
<Input.SelectInput source="croup" label={Alias["croup"]} choices={choices.croup} />
<Input.SelectInput
source="association_code"
label={Alias["association_code"]}
choices={choices.associationCode}
/>
<Input.SelectInput source="work_code" label={Alias["work_code"]} choices={choices.workCode} />
<Input.BooleanInput source="permanent" label={Alias["permanent"]} />
<Input.BooleanInput source="ifus" label={Alias["ifus"]} />
<Input.DateInput source="start_date" label={Alias["start_date"]} />
<Input.DateInput source="outbus_date" label={Alias["outbus_date"]} />
<Input.DateInput source="inactive_date" label={Alias["inactive_date"]} />
<Input.DateInput source="chap11_date" label={Alias["chap11_date"]} />
*/
