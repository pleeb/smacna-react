import React, { Fragment } from "react";
import _ from "lodash";
import { Alias } from "../../";
import { EnumerableFieldType, enumerableHash, NumericFieldType, numericFields, integerFields, dateFields, currencyFields } from "../../";
import { IRequest, IReport } from "./";
import { CardHeader, CardContent, CardActions, Button, withStyles, LinearProgress } from "@material-ui/core";
import { Card, Table, TableBody, TableCell, TableHead, TableRow } from "./table";
import { connect } from "react-redux";
interface IProps {
  // request: IRequest;
  // datas: any[];
  // fetching: boolean;
  report: IReport;
}
const Progress = ({ fetching }) => (
  <div
    style={{
      width: "100%",
      display: "flex",
      flexDirection: "column"
    }}
  >
    {fetching ? <LinearProgress variant="query" /> : null}
  </div>
);
export const DynamicTable = connect((state: any) => ({ report: state.report }))(
  ({
    report: {
      request: { groups, selects, computes },
      datas,
      fetching
    }
  }: IProps) => {
    const allAttributes = _.uniq([...groups, ...selects, ...computes, "companies_count", "contributions_count"]);
    return (
      <Card>
        <Table>
          <TableHead>
            <RenderHeaderRow computes={allAttributes} />
          </TableHead>
          <TableBody>
            <RenderGroupRows allAttributes={allAttributes} allGroups={groups} datas={datas} index={0} />
          </TableBody>
        </Table>
        <Progress fetching={fetching} />
      </Card>
    );
  }
);
const RenderHeaderRow = ({ computes }) => {
  return (
    <TableRow>
      {computes.map((e, _i) => (
        <TableCell numeric={numericFields.has(e)} key={_i}>
          {format.alias(e)}
        </TableCell>
      ))}
    </TableRow>
  );
};
const RenderRowCells = ({ label, allAttributes, group, gattribute, data, groupStyle, dataStyle }) => {
  const cells = [];
  let reachedSelf = false;
  for (let attr of allAttributes) {
    if (data && reachedSelf) cells.push(<DataCell key={attr} attribute={attr} data={data} style={dataStyle} />);
    else if (attr === group)
      (reachedSelf = true) &&
        cells.push(
          <TableCell key={attr} numeric={numericFields.has(attr)}>
            <strong style={groupStyle}>{format.alias(gattribute)}</strong>
          </TableCell>
        );
    else
      cells.push(
        <TableCell key={attr} numeric={numericFields.has(attr)}>
          &nbsp;
        </TableCell>
      );
  }
  return <Fragment>{cells}</Fragment>;
};
const RenderGroupRows = ({ allAttributes, allGroups, index, datas }) => {
  const group = allGroups[index];
  if (!group) return null;
  if (!datas.length) return null;
  const rows = [];
  const lastGroup = allGroups[allGroups.length - 1] === group;
  let gcomputes = enumerableHash[group];
  if (group === "company_id") {
    gcomputes = datas.map(e => e.company_id);
  }
  let _i = 0;
  for (let gattribute of gcomputes) {
    const filtered = datas.filter(d => d[group] === gattribute);
    if (filtered.length) {
      if (lastGroup) {
        if (filtered.length > 1) throw `last group but multiple data points ${group} ${gattribute}`;
        rows.push(
          <RenderGroupRow key={index + "-" + _i++} allAttributes={allAttributes} group={group} gattribute={gattribute} data={filtered[0]} />
        );
      } else {
        rows.push(
          <RenderGroupRow key={index + "-" + _i++} allAttributes={allAttributes} group={group} gattribute={gattribute} data={null} />,
          <RenderGroupRows
            key={index + "-" + _i++}
            allAttributes={allAttributes}
            allGroups={allGroups}
            index={index + 1}
            datas={filtered}
          />
        );
      }
    } else {
      rows.push(
        <RenderGroupRows key={index + "-" + _i++} allAttributes={allAttributes} allGroups={allGroups} index={index + 1} datas={filtered} />
      );
    }
  }
  if (lastGroup)
    rows.push(
      <RenderTotalRow
        key={index + "-" + _i++}
        allAttributes={allAttributes}
        datas={datas}
        style={{ color: "tomato", fontSize: "1.1em" }}
        label="Sub Total:"
      />
    );
  if (index === 0)
    rows.push(
      <RenderTotalRow
        key={index + "-" + _i++}
        allAttributes={allAttributes}
        datas={datas}
        style={{ color: "crimson", fontSize: "1.2em" }}
        label="Grand Total:"
      />
    );
  return <Fragment>{rows}</Fragment>;
};
const RenderTotalRowCells = ({ allAttributes, datas, label, style }) => {
  const cells = [];
  let i = 0;
  for (let attr of allAttributes) {
    if (i++ === 0) {
      cells.push(<ValueCell key={attr} attribute={attr} style={style} value={label} />);
    } else if (numericFields.has(attr)) {
      const value = datas.map(e => +e[attr]).reduce((a, b) => a + b);
      cells.push(<ValueCell key={attr} attribute={attr} style={style} value={value} />);
    } else
      cells.push(
        <TableCell key={attr} numeric={numericFields.has(attr)}>
          &nbsp;
        </TableCell>
      );
  }
  return <Fragment>{cells}</Fragment>;
};
const RenderTotalRow = ({ allAttributes, datas, label, style }) => {
  return (
    <TableRow>
      <RenderTotalRowCells allAttributes={allAttributes} datas={datas} label={label} style={style} />
    </TableRow>
  );
};
const RenderGroupRow = ({ allAttributes, group, gattribute, data }) => {
  return (
    <TableRow>
      <RenderRowCells
        label={null}
        allAttributes={allAttributes}
        gattribute={gattribute}
        group={group}
        data={data}
        groupStyle={{ color: "slateblue" }}
        dataStyle={null}
      />
    </TableRow>
  );
};
const ValueCell = ({ attribute, value, style }) => {
  const numeric = numericFields.has(attribute);
  value = numeric ? format.numeric(attribute, value) : format.alias(value);
  return (
    <TableCell numeric={numeric}>
      <strong style={style}>{value}</strong>
    </TableCell>
  );
};
const DataCell = ({ attribute, data, style }) => {
  const numeric = numericFields.has(attribute);
  const value = numeric ? format.numeric(attribute, data[attribute]) : format.alias(data[attribute], attribute);
  return (
    <TableCell style={style} numeric={numeric}>
      {value}
    </TableCell>
  );
};
class Format {
  private static instance: Format = null;
  static create(): Format {
    return this.instance || (this.instance = new Format());
  }
  private constructor() {}
  private _currency = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD"
  });
  private _decimal = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 1,
    maximumFractionDigits: 1
  });
  private _integer = new Intl.NumberFormat("en-US", {
    minimumFractionDigits: 0,
    maximumFractionDigits: 0
  });
  currency(value: any): string {
    return this._currency.format(value);
  }
  decimal(value: any): string {
    return this._decimal.format(value);
  }
  integer(value: any): string {
    return this._integer.format(value);
  }
  date(value: any): string {
    return new Date(value).toLocaleDateString();
  }
  numeric(attr: NumericFieldType, value: any): string {
    if (currencyFields.has(attr)) return this.currency(value);
    else if (integerFields.has(attr)) return this.integer(value);
    else if (dateFields.has(attr as any)) return this.date(value);
    else return this.decimal(value);
  }
  alias(value: any, attr?: string): string {
    const alias = attr && dateFields.has(attr as any) ? new Date(value).toLocaleDateString() : Alias[value];
    // if (!alias) console.log("WARNING: NO ALIAS FOUND FOR ", value);
    return alias || value;
  }
}
const format = Format.create();
