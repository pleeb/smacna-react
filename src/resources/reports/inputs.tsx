import React from "react";
import { withStyles } from "@material-ui/core";
import RunIcon from "@material-ui/icons/Send";
import { withDataProvider } from "react-admin";
import * as choices from "./choices";
import * as company from "../companies/choices";
import * as contribution from "../contributions/choices";
import { Input, Enum, Alias } from "../";
import { connect } from "react-redux";
import compose from "recompose/compose";

export const TabbedInputForm = withStyles({})(({ toolbar, classes, ...props }: any) => {
  // console.log(props);
  return (
    <Input.TabbedForm
      toolbar={toolbar}
      redirect={false}
      {...props}
      defaultValue={{
        companies: {},
        contributions: {},
        computes: [],
        groups: [],
        selects: []
      }}
    >
      <Input.FormTab label="report">
        <Input.TextInput fullWidth source="title" style={{ fontWeight: "bold" }} validate={Input.required} defaultValue="RUN TEST REPORT" />
        <Input.DateRangeInput source="contributions" field="reporting_week_end" defaultOperator={Enum.NumberOperator.BETWEEN} />
      </Input.FormTab>
      <Input.FormTab label="compute">
        <Input.SelectArrayInput
          fullWidth
          style={{ minWidth: "6em" }}
          label="Group by Attribute"
          source="groups"
          defaultValue={["work_code", "company_id"]}
          validate={undefined}
          choices={choices.enumerate}
        />
        <SelectCompAttr />
        <Input.SelectArrayInput
          fullWidth
          style={{ minWidth: "6em" }}
          label="Compute Attribute"
          source="computes"
          defaultValue={["num_total", "hours_total", "amount_total", "amount_paid", "amount_owed"]}
          validate={undefined}
          choices={choices.attribute}
        />
      </Input.FormTab>
      <Input.FormTab label="contributions">
        <Row source="companies">
          <Input.NumberInput fullWidth label={Alias["reporting_week_count"]} source="contributions.reporting_week_count" />
          <Input.SelectInput
            allowEmpty
            fullWidth
            label={Alias["calculation"]}
            source="contributions.calculation"
            choices={choices.calculation}
          />
          <Input.SelectInput allowEmpty fullWidth label={Alias["status"]} source="contributions.status" choices={choices.status} />
        </Row>
        <Input.DateRangeInput source="contributions" field="reporting_week_end" defaultOperator={Enum.NumberOperator.BETWEEN} />
        <Input.DateRangeInput source="contributions" field="receive_date" />
        <Input.DateRangeInput source="contributions" field="deposit_date" />
      </Input.FormTab>
      <Input.FormTab label="companies">
        <Row source="companies">
          <Input.SelectInput source="companies.state" label="State" choices={choices.state} allowEmpty />
          <Input.BooleanInput label={Alias["ifus"]} source="companies.ifus" />
          <Input.BooleanInput label={Alias["permanent"]} source="companies.permanent" />
          <Input.BooleanInput label={Alias["association"]} source="companies.association" />
        </Row>
        <Input.DateRangeInput source="companies" field="start_date" />
        <Input.DateRangeInput source="companies" field="outbus_date" />
        <Input.DateRangeInput source="companies" field="inactive_date" />
        <Input.DateRangeInput source="companies" field="chap11_date" />
      </Input.FormTab>
    </Input.TabbedForm>
  );
});
const Row = withStyles({
  root: {
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center"
  }
})(({ classes, source, children }: any) => <div className={classes.root}>{children}</div>);

const SelectCompAttr = connect((state: any) => {
  const recordForm = state.form["record-form"] || {};
  const values = recordForm.values || {};
  const groups = values.groups || [];
  const show = groups.includes("company_id");
  return {
    show
  };
})(({ show }: any) => {
  return show ? (
    <Input.SelectArrayInput
      fullWidth
      label="Include Company Attribute"
      source="selects"
      defaultValue={["company_name", "association_code"]}
      validate={Input.required}
      choices={choices.select}
    />
  ) : null;
});
