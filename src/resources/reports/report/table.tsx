import React from "react";
import {
  Card as _Card,
  withStyles,
  LinearProgress,
  TableFooter as _TableFooter,
  Table as _Table,
  TableBody as _TableBody,
  TableCell as _TableCell,
  TableHead as _TableHead,
  TableRow as _TableRow
} from "@material-ui/core";

export const Card = withStyles({
  root: {
    margin: 0,
    marginTop: 10,
    flexGrow: 1,
    display: "flex",
    flexDirection: "column"
  },
  noShrink: {
    margin: 0,
    flexShrink: 0,
    display: "flex",
    flexDirection: "column"
  }
})((props: any) => {
  return <_Card className={props.noShrink ? props.classes.noShrink : props.classes.root}>{props.children}</_Card>;
});
export const Table = withStyles({
  root: {
    height: "100%"
  }
})(({ children, classes }) => {
  return (
    <_Table padding="checkbox" classes={classes}>
      {children}
    </_Table>
  );
});
export const TableHead = withStyles({
  root: {
    background: "bisque"
  }
})(({ children, classes }) => {
  return <_TableHead classes={classes}>{children}</_TableHead>;
});
export const TableBody = withStyles({
  root: {
    // height: "36px"
  }
})(({ children, classes }) => {
  return <_TableBody classes={classes}>{children}</_TableBody>;
});
export const TableRow = withStyles({
  root: {
    height: "36px",
    "&:nth-of-type(odd)": {
      background: "lightslateblue"
    }
  }
})(({ children, classes }) => {
  return <_TableRow classes={classes}>{children}</_TableRow>;
});
export const TableCell = withStyles({
  root: {
    // height: "36px"
  },
  body: {
    // height: "36px"
  },
  head: {
    // height: "36px"
  },
  footer: {
    // height: "36px"
  }
})(({ children, classes, numeric, ...props }: any) => {
  return (
    <_TableCell numeric={numeric} {...props}>
      {children}
    </_TableCell>
  );
});
export const TableFooter = withStyles({
  root: {
    // height: "36px"
  }
})(({ children, classes, numeric }: any) => {
  return <_TableFooter classes={classes}>{children}</_TableFooter>;
});
