import React from "react";
import { Card, Chip, CardActions, withStyles } from "@material-ui/core";
import { isMaster, Button, Enum, Input, Alias, Field, Pagination, Actionbar } from "../";
import { connect } from "react-redux";
import { Add } from "@material-ui/icons";
import * as choices from "./choices";
import * as company from "../companies/choices";
import * as contribution from "../contributions/choices";

const Filters = props => (
  <Field.Filter {...props}>
    <Input.StringSearchInput source="title" alwaysOn />
  </Field.Filter>
);
const Actions = ({
  bulkActions,
  basePath,
  currentSort,
  displayedFilters,
  exporter,
  filters,
  filterValues,
  onUnselectItems,
  resource,
  selectedIds,
  showFilter,
  total,
  permissions
}: any) => (
  <CardActions>
    {filters &&
      React.cloneElement(filters, {
        resource,
        showFilter,
        displayedFilters,
        filterValues,
        context: "button"
      })}
    {isMaster(permissions) && <Button.CreateButton basePath={basePath} resource={resource} />}
    <Button.ExportButton disabled={total === 0} resource={resource} sort={currentSort} filter={filterValues} exporter={exporter} />
    <Button.RefreshButton />
  </CardActions>
);
export const ReportList = connect(
  ({ session: { role: permissions } }: any) => ({ permissions }),
  {}
)(({ permissions, ...props }: any) => {
  return (
    <Field.List
      {...props}
      pagination={<Pagination />}
      filters={<Filters permissions={permissions} />}
      actions={<Actions permissions={permissions} />}
      bulkActionButtons={false}
    >
      <Field.Datagrid rowClick="edit">
        <Field.TextField source="title" label={Alias["title"]} />
        <ChipCollection source="groups" label={Alias["groups"]} />
        <ChipCollection source="selects" label={Alias["selects"]} />
        <ChipCollection source="computes" label={Alias["computes"]} />
        <Field.DateField source="updated_at" label={Alias["updated_at"]} />
      </Field.Datagrid>
    </Field.List>
  );
});
const ChipCollection = withStyles({
  chip: {
    margin: 2
  }
})(({ record, source, classes }: any) => {
  const arr = record[source];
  return (
    <div>
      {arr.map((e, i) => (
        <Chip className={classes.chip} label={Alias[e]} key={i} />
      ))}
    </div>
  );
});
