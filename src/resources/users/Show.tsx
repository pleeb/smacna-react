import React from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
import { withStyles } from "@material-ui/core";
import {
  isMaster,
  DatagridMapper,
  Enum,
  DashPanel,
  sourceField,
  GridItem,
  GridContainer,
  ReferenceManyContainer,
  Button,
  SimpleShowContainer,
  Actionbar,
  Field
} from "../";
import { ReferenceManyPayment } from "../payments";

const { Tab, TabbedShowLayout } = Field;

const Actions = connect(
  ({ session: { id: user_id } }: any, { id }: any) => {
    return {
      isOwner: +user_id === +id
    };
  },
  {}
)(({ isOwner, permissions, ...props }: any) => {
  const record = props.data || {};
  const role = Enum.Role[record.role];
  const type = props.dash ? "Viewing" : "Showing";
  const verb = `${type} ${role}`;
  const editable = isOwner || isMaster(permissions);
  return (
    <Actionbar verb={verb} title={record["full_name"]} {...props}>
      {editable && <Button.DashEditButton label={isOwner ? "Update" : "Edit"} />}
      <Button.ListButton />
    </Actionbar>
  );
});
export const UserShow = ({ permissions, ...props }: any) => (
  <Field.Show resource="users" basePath="/users" id={0} {...props} actions={<Actions id={props.id} permissions={permissions} />}>
    <SimpleShowContainer>
      <GridContainer withMargin>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("Company.company_name")}
            {sourceField("company_id")}
            {sourceField("id")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("username")}
            {sourceField("first_name")}
            {sourceField("last_name")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("full_name")}
            {sourceField("title")}
            {sourceField("email")}
            {sourceField("phone")}
          </SimpleShowContainer>
        </GridItem>
        <GridItem xs={true}>
          <SimpleShowContainer>
            {sourceField("role")}
            {sourceField("created_at")}
            {sourceField("updated_at")}
          </SimpleShowContainer>
        </GridItem>
      </GridContainer>
      <Wrap permissions={permissions} />
    </SimpleShowContainer>
  </Field.Show>
);
const Wrap = ({ permissions, ...props }) =>
  props.record.role === Enum.Role.User ? (
    <TabbedShowLayout {...props}>
      <Tab label="Successful Payments">
        <ReferenceManyPayment permissions={permissions} filter={{ pay_status: Enum.PayStatus.SUCCESS }} target="user_id" />
      </Tab>
      <Tab label="Pending Payments" path="pending">
        <ReferenceManyPayment permissions={permissions} filter={{ pay_status: Enum.PayStatus.PENDING }} target="user_id" />
      </Tab>
      <Tab label="Short Payments" path="short">
        <ReferenceManyPayment permissions={permissions} filter={{ pay_status: Enum.Status.SHORT as any }} target="user_id" />
      </Tab>
      <Tab label="Failed Payments" path="failed">
        <ReferenceManyPayment permissions={permissions} filter={{ pay_status: Enum.PayStatus.FAILED }} target="user_id" />
      </Tab>
    </TabbedShowLayout>
  ) : null;
const Dash = ({ permissions, ...props }: any) => {
  return (
    <Field.Show
      resource="users"
      basePath="/users"
      id={0}
      {...props}
      actions={<Actions anchor={props.anchor} id={props.id} permissions={permissions} dash />}
    >
      <SimpleShowContainer withPad>
        {sourceField("Company.company_name")}
        {sourceField("company_id")}
        {sourceField("id")}
        {sourceField("username")}
        {sourceField("first_name")}
        {sourceField("last_name")}
        {sourceField("full_name")}
        {sourceField("title")}
        {sourceField("email")}
        {sourceField("phone")}
        {sourceField("role")}
        {sourceField("created_at")}
        {sourceField("updated_at")}
      </SimpleShowContainer>
    </Field.Show>
  );
};
export const UserDashShow = (props: any) => {
  return (
    <DashPanel resource={Enum.Resource.Users} anchor={Enum.Anchor.Right} crud={Enum.Crud.Read} {...props}>
      <Dash />
    </DashPanel>
  );
};
