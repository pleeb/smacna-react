import { PRINT_FETCH, printRecordSuccess } from "../actions/print";
import { dataProvider } from "../data/dataProvider";
import { call, put, takeLatest } from "redux-saga/effects";

// dataProvider(GET_ONE, 'posts', { id: 123 }) => Promise.resolve({ data: { id: 123, title: "hello, world" } })

function* fetchPrintRecord({ type, record: { id }, resource }) {
  console.log("fetch record", id);
  const response = yield call(dataProvider("GET_ONE", "payments", { id }));
  console.log("saga fetch response", response);
  const record = response.data;
  const test = yield put(printRecordSuccess(record, resource));
}

export function* printSaga() {
  yield takeLatest(PRINT_FETCH, fetchPrintRecord);
}
