import { React, parse, getRateSource, TSourceRecord, HoursSource } from "../";

export const FieldAmountCalculation = ({ record, source }: TSourceRecord<HoursSource>) => {
  const rate = parse.money(record[getRateSource(source)]);
  const hoursTarget = parse.number(source === "hours_mechanic" ? record["hours_target"] : 0);
  const hours = parse.number(record[source]);
  return (
    <span style={{ display: "inline-flex", flexWrap: "wrap" }}>
      <span>{rate}</span>
      <span>&nbsp;x&nbsp;</span>
      {hoursTarget ? (
        <span>
          (&nbsp;{hours}&nbsp;-&nbsp;{hoursTarget}&nbsp;)
        </span>
      ) : (
        <span>{hours}</span>
      )}
    </span>
  );
};
export type ComponentSignature = React.StatelessComponent<TSourceRecord<HoursSource>>;
