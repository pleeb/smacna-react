import React from "react";
import { withStyles, Card, TableFooter, Table, TableBody, TableCell, TableHead, TableRow, TextField } from "@material-ui/core";
import { parse, isMaster, SimpleShowContainer, Field, Actionbar, DialogWrapper, Button } from "../";
import { connect } from "react-redux";

const Actions = ({ permissions, ...props }) => {
  return (
    <Actionbar {...props} verb="Current" title="Contribution Rates">
      {isMaster(permissions) ? <Button.DashEditButton label="Update" /> : null}
    </Actionbar>
  );
};
export const RatesShow = ({ permissions, ...props }: any) => {
  return (
    <Field.Show {...props} actions={<Actions permissions={permissions} />}>
      <SimpleShowContainer>
        <Wrap />
      </SimpleShowContainer>
    </Field.Show>
  );
};

const TCell = withStyles({ root: { fontSize: 16 } })(TableCell);
const Wrap = ({ record, classes, ...props }: any) => {
  return (
    <Table padding="checkbox">
      <TableHead>
        <TableRow>
          <TableCell />
          <TableCell>
            <strong>Last Updated</strong>
          </TableCell>
          <TableCell numeric>
            <strong>Building Trade Mechanics</strong>
          </TableCell>
          <TableCell numeric>
            <strong>Apprentice Workers</strong>
          </TableCell>
          <TableCell numeric>
            <strong>Light Commercial Workers</strong>
          </TableCell>
          <TableCell />
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell />
          <TCell>
            <strong>{parse.datetime(record.updated_at)}</strong>
          </TCell>
          <TCell numeric>
            <strong>{parse.money(record.rate_mechanic)}</strong>
          </TCell>
          <TCell numeric>
            <strong>{parse.money(record.rate_apprentice)}</strong>
          </TCell>
          <TCell numeric>
            <strong>{parse.money(record.rate_light_commercial)}</strong>
          </TCell>
          <TableCell />
        </TableRow>
      </TableBody>
    </Table>
  );
};
