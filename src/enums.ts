import {
  Dashboard,
  Settings,
  Home,
  ExitToApp as Logout,
  //
  Work as Company,
  Business as Companies,
  //
  Person as User,
  VerifiedUser as Admin,
  Security as Master,
  RecentActors as Users,
  //
  Payment,
  LocalAtm as Payments,
  //
  Today as Contribution,
  EventNote as Contributions,
  //
  Timeline as Rates,
  //
  Subject as Report,
  LibraryBooks as Reports
} from "@material-ui/icons";

export enum PayStatus {
  PENDING = "PENDING",
  FAILED = "FAILED",
  SUCCESS = "SUCCESS"
}
export enum PayMethod {
  MAIL = "MAIL",
  ELECTRONIC = "ELECTRONIC"
}
export enum Calculation {
  ACTUAL = "ACTUAL",
  ESTIMATE = "ESTIMATE"
}
export enum Status {
  PAID = "PAID",
  DELINQUENT = "DELINQUENT",
  UNREPORTED = "UNREPORTED",
  PENDING = "PENDING",
  SHORT = "SHORT",
  OVERPAY = "OVERPAY",
  FAILED = "FAILED",
  SUCCESS = "SUCCESS"
}
export enum AssociationCode {
  NON_ASSOCIATION_NY = "NON_ASSOCIATION_NY",
  NON_ASSOCIATION_LI = "NON_ASSOCIATION_LI",
  ASSOCIATION_NY = "ASSOCIATION_NY",
  ASSOCIATION_LI = "ASSOCIATION_LI",
  OTHER = "OTHER"
}
export enum Group {
  IND = "IND"
}
export enum WorkCode {
  TESTING_AND_BALANCING = "TESTING_AND_BALANCING",
  VENTILATION = "VENTILATION",
  SPECIALTY = "SPECIALTY",
  ROOFING = "ROOFING",
  MANUFACTURING = "MANUFACTURING"
}
export const StatusColorMap = {
  __Identifier: "__StatusColorMap",
  PENDING: "goldenrod",
  SUCCESS: "green",
  FAILED: "crimson",
  PAID: "green",
  DELINQUENT: "crimson"
};
export const AmountColorMap = {
  Amount: "slategrey",
  AmountTotal: "darkslateblue",
  AmountPaid: "slateblue",
  Zero: "steelblue",
  Delinquent: "crimson",
  Paid: "green"
};
export const ColorMap = {
  BAD_VALUE: "fuchsia",
  Money: {
    Amount: "slategrey",
    AmountTotal: "darkslateblue",
    AmountPaid: "slateblue",
    Zero: "steelblue",
    UnderPay: "crimson",
    OverPay: "green"
  },
  Status: {
    Default: "slateblue",
    PENDING: "goldenrod",
    SUCCESS: "green",
    FAILED: "crimson",
    PAID: "green",
    DELINQUENT: "crimson",
    UNREPORTED: "goldenrod"
  },
  Role: {
    0: "crimson",
    1: "darkblue",
    2: "goldenrod",
    3: "green"
  }
};
export const Icon = {
  Settings,
  Home,
  Logout,
  Company,
  Companies,
  User,
  Admin,
  Master,
  Users,
  Payment,
  Payments,
  Contribution,
  Contributions,
  Report,
  Reports,
  Rates,
  Dashboard
};
export enum SelectionOperator {
  NOOP = "$$NOOP",
  IN = "$in",
  ANY = "$any",
  AND = "$and",
  OR = "$or"
}
export enum NumberOperator {
  NOOP = "$$NOOP",
  GREATER_THAN = "$gt",
  GREATER_THAN_EQUAL = "$gte",
  LESS_THAN = "$lt",
  LESS_THAN_EQUAL = "$lte",
  NOT_EQUAL = "$ne",
  EQUAL = "$eq",
  BETWEEN = "$between",
  NOT_BETWEEN = "$notBetween"
}
export enum StringOperator {
  NOOP = "$$NOOP",
  STARTS_WITH = "$$starts",
  CONTAINS = "$$contains",
  ENDS_WITH = "$$ends"
}
export enum Anchor {
  Top = "top",
  Right = "right",
  Bottom = "bottom",
  Left = "left",
  Center = "center"
}
export enum Crud {
  None = "NONE",
  Update = "UPDATE",
  Create = "CREATE",
  Read = "READ",
  Delete = "DELETE"
}
export enum BasePath {
  None = "/",
  Dashboard = "/dashboard",
  Payments = "/payments",
  Contributions = "/contributions",
  Companies = "/companies",
  Users = "/users",
  Rates = "/rates",
  Reports = "/reports"
}
export enum Resource {
  Dashboard = "dashboard",
  Payments = "payments",
  Contributions = "contributions",
  Companies = "companies",
  Users = "users",
  Rates = "rates",
  Reports = "reports"
}
export enum Context {
  None,
  Show,
  List,
  ListExpand
}
export enum Role {
  None,
  User,
  Admin,
  Master
}
