import { Enum } from "../";
export const isMaster = (permissions: Enum.Role | { permissions: Enum.Role }): boolean | void => {
  if (permissions === null) return false;
  const permission = typeof permissions === "number" ? permissions : permissions && permissions.permissions;
  // if (!permission) return console.log(Error("WARNING: Called for isMaster(permissions) but permissions was " + permissions));
  return permission >= Enum.Role.Master;
};
export * from "./parse";
