import React from "react";
import { Alias, Input } from "../";
import { Card, CardHeader, CardContent, CardActions, Button, withStyles } from "@material-ui/core";
import RunIcon from "@material-ui/icons/Send";
import { withDataProvider } from "react-admin";
import * as choices from "./choices";
import { connect } from "react-redux";
import compose from "recompose/compose";

export const UserCreate = withStyles({
  root: {
    display: "flex",
    flexDirection: "column"
  }
})(({ classes, ...props }: any) => {
  return (
    <Input.Create {...props}>
      <Input.SimpleForm redirect="show">
        <Input.TextInput source="id" label={Alias["id"]} />
        <Input.TextInput source="username" label={Alias["username"]} />
        <Input.TextInput source="password" label={Alias["password"]} />
        <Input.TextInput source="last_name" label={Alias["last_name"]} />
        <Input.TextInput source="first_name" label={Alias["first_name"]} />
        <Input.TextInput source="title" label={Alias["title"]} />
        <Input.TextInput source="email" label={Alias["email"]} />
        <Input.TextInput source="created_at" label={Alias["created_at"]} />
        <Input.TextInput source="updated_at" label={Alias["updated_at"]} />
      </Input.SimpleForm>
    </Input.Create>
  );
});
