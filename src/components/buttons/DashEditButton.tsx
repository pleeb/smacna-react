import React from "react";
import Icon from "@material-ui/icons/Edit";
import { DashButton, Enum } from "./";

export const DashEditButton = props => (
  <DashButton
    Icon={Icon}
    crud={Enum.Crud.Update}
    label={props.label || "Edit"}
    record={props.record}
    resource={props.resource}
    variant="flat"
  />
);
