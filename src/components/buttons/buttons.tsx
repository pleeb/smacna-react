import React from "react";
import { stylize } from "./DashButton";
import { isMaster } from "../";
import { connect } from "react-redux";

import {
  CreateButton as _CreateButton,
  ShowButton as _ShowButton,
  EditButton as _EditButton,
  ListButton as _ListButton,
  DeleteButton as _DeleteButton,
  RefreshButton as _RefreshButton,
  ExportButton as _ExportButton,
  SaveButton as _SaveButton
} from "react-admin";

export const ShowButton = stylize(_ShowButton) as any;
export const ListButton = stylize(_ListButton) as any;
export const RefreshButton = stylize(_RefreshButton) as any;
export const ExportButton = stylize(_ExportButton) as any;

const ff_mapStateToProps = (_label: string) => ({ session: { role: permissions } }: any, { label = _label }: any) => {
  return { label: isMaster(permissions) ? `Master ${label}` : label };
};
export const CreateButton = connect(
  ff_mapStateToProps("Create"),
  {}
)(stylize(_CreateButton)) as any;
export const EditButton = connect(
  ff_mapStateToProps("Edit"),
  {}
)(stylize(_EditButton)) as any;
export const DeleteButton = connect(
  ff_mapStateToProps("Delete"),
  {}
)(stylize(_DeleteButton)) as any;
export const SaveButton = connect(
  ff_mapStateToProps("Save"),
  {}
)(stylize(_SaveButton)) as any;
