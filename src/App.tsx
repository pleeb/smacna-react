import React from "react";
import * as r from "react-admin";
import { Admin } from "react-admin";
import { withStyles, createMuiTheme, colors } from "@material-ui/core";
import * as data from "./data";
import { ContactUsDialog, PaymentDialog } from "./resources/misc";
import { Layout, Menu, Login } from "./layout";
import { Resources, Resource, Dashboard, routes } from "./resources";
import reducers from "./reducers";
import sagas from "./sagas";
import { Enum } from "./";
import { DialogUpdating } from "./components/DialogWrapper";

// import { Signatures } from "./test";
// <Signatures />;
const { Role } = Enum;

console.log("react-admin", r);

// const test = <Signatures />;
const Wrap = props => <DialogUpdating />;
class App extends React.Component<any, any> {
  render() {
    return (
      <Admin
        customRoutes={routes}
        customSagas={sagas}
        customReducers={reducers}
        title="NYC SMACNA"
        loginPage={Login}
        appLayout={Layout}
        menu={Menu}
        dataProvider={data.dataProvider}
        authProvider={data.authProvider}
        dashboard={Dashboard}
      >
        {permissions => [
          +permissions >= +Role.User ? (
            <Resource
              name="contributions"
              create={Resources.ContributionShow}
              show={Resources.ContributionShow}
              list={Resources.ContributionList}
              // edit={Resources.AdjustContributionDashEdit}
            />
          ) : null,
          +permissions >= +Role.User ? (
            <Resource
              name="payments"
              // create={Resources.PaymentCreate}
              // show={Resources.PaymentShow}
              list={+permissions >= +Role.Admin ? Resources.PaymentList : null}
              // edit={Resources.PaymentProcessDashEdit}
            />
          ) : null,
          +permissions >= +Role.User ? (
            <Resource
              name="companies"
              // create={Resources.CompanyCreate}
              show={Resources.CompanyShow}
              list={+permissions >= +Role.Admin ? Resources.CompanyList : null}
              // edit={Resources.CompanyDashEdit}
            />
          ) : null,
          +permissions >= +Role.User ? (
            <Resource
              name="users"
              // create={Resources.UserCreate}
              show={Resources.UserShow}
              list={Resources.UserList}
              // edit={Resources.UserDashEdit}
            />
          ) : null,
          +permissions >= +Role.Admin ? (
            <Resource
              name="reports"
              list={Resources.ReportList}
              create={+permissions >= +Role.Master ? Resources.ReportCreate : null}
              edit={+permissions >= +Role.Master ? Resources.ReportEdit : null}
              show={Resources.ReportShow}
            />
          ) : null,
          +permissions >= +Role.Admin ? <Resource name="rates" list={null} create={null} edit={null} show={Resources.RatesShow} /> : null,
          <Resources.ReportContributionDashCreate />,
          // <Resources.ContributionDashShow />,
          <Resources.AdjustContributionDashEdit />,
          //
          <Resources.PaymentOutstandingDashCreate />,
          <Resources.PaymentProcessDashEdit />,
          //
          <Resources.CompanyDashEdit />,
          //
          <Resources.UserDashEdit />,
          <Resources.RateDashEdit />,
          <ContactUsDialog />,
          <PaymentDialog />
        ]}
      </Admin>
    );
  }
}
export default App;
