import React from "react";
import { connect } from "react-redux";
import { TextField as _TextField } from "@material-ui/core";
import { Enum, Input, Field, Alias, isMaster } from "../../";
import * as validate from "./validation";

const Hidden = props => (
  <div style={{ display: "none" }}>
    <Input.TextInput {...props} source="cid" defaultValue="" />
    <Input.NumberInput {...props} source="contribution_id" defaultValue={props.contribution_id} />
    <Input.NumberInput {...props} source="company_id" defaultValue={props.company_id} />
    <Input.NumberInput {...props} source="user_id" defaultValue={props.user_id} />
    <Input.NumberInput {...props} source="reporting_week" defaultValue={0} />
    <Input.NumberInput {...props} source="reporting_week_count" defaultValue={0} />
    <Input.DateInput {...props} source="reporting_week_start" defaultValue="" />
    <Input.DateInput {...props} source="reporting_week_end" defaultValue="" />
    <Input.NumberInput {...props} source="amount_total" defaultValue={0} />
    <Input.NumberInput {...props} source="amount_owed" defaultValue={0} />
    <Input.NumberInput {...props} source="amount_paid" defaultValue={0} validate={validate.amountPaid} />
  </div>
);
export const PaymentFields = connect(
  ({ session: { id: user_id, company_id, role: permissions }, ...props }: any, { record: { id: contribution_id } }: any) => {
    const form = props.form["record-form"] || {};
    const { amount_total = 0, amount_paid = 0 } = form.values || ({} as any);
    const amount_owed = +amount_paid - +amount_total;
    const pay_amount = -amount_owed;
    return { contribution_id, user_id, company_id, amount_total, amount_paid, amount_owed, pay_amount, permissions };
  }
)(({ contribution_id, user_id, company_id, amount_total, amount_paid, amount_owed, pay_amount, permissions }: any) => {
  const error = isMaster(permissions) ? false : pay_amount < 0;
  return (
    <React.Fragment>
      <Hidden {...{ contribution_id, user_id, company_id, amount_total, amount_paid, amount_owed, pay_amount, permissions }} />
      <Field.PaymentField value={amount_total} source="amount_total" label={Alias["amount_total"]} />
      <Field.PaymentField value={amount_paid} source="amount_paid" label={Alias["amount_paid"]} />
      <Field.PaymentField value={amount_owed} label={Alias["amount_owed"]} />
      <Field.PayField
        value={pay_amount}
        label={error ? "Negative Adjustment" : "Pay Amount"}
        error={error}
        help="Please call us at (718) 624-1234 to submit a negative adjustment."
      />
    </React.Fragment>
  );
});
export const ReportingFields = connect(
  ({ session: { company_id }, form: { "record-form": form = {} }, ...props }: any, { record }: any) => {
    const { certified, cid, reporting_week_start, reporting_week_end, reporting_week, reporting_week_count } = form.values || ({} as any);
    return { certified, cid, reporting_week_start, reporting_week_end, reporting_week, reporting_week_count };
  }
)(({ certified, cid, reporting_week_start, reporting_week_end, reporting_week, reporting_week_count }: any) => {
  return (
    <React.Fragment>
      <NumberField value={reporting_week} label={Alias["reporting_week"]} error={certified && !reporting_week} />
      <NumberField value={reporting_week_count} label={Alias["reporting_week_count"]} error={certified && !reporting_week_count} />
      <DateField
        value={reporting_week_start}
        label="Week Starting"
        error={certified && !reporting_week_start}
        help="Please select the starting week."
      />
      <DateField
        value={reporting_week_end}
        label={Alias["reporting_week_end"]}
        error={certified && !reporting_week_end}
        help="Please select the ending week."
      />
    </React.Fragment>
  );
});

const DateField = ({ value, label, error, help }) => (
  <_TextField
    margin="normal"
    fullWidth
    helperText={error && <strong>{help}</strong>}
    error={error}
    InputProps={{ readOnly: true, disableUnderline: error ? false : true }}
    value={value ? value.toLocaleDateString() : "----"}
    label={label}
  />
);
const TextField = ({ value, label }) => (
  <_TextField margin="normal" fullWidth InputProps={{ readOnly: true, disableUnderline: true }} value={value || "----"} label={label} />
);
const NumberField = ({ value, label, help = "", error = false }) => (
  <_TextField
    margin="normal"
    helperText={error && help}
    error={error}
    fullWidth
    InputProps={{ readOnly: true, disableUnderline: true }}
    value={value || "----"}
    label={label}
  />
);
