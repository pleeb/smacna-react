import { FieldType, Alias, EnumType, Enum } from "../components";

export const createChoice = (value: any, name?: string) => ({ id: value, name: name || Alias[value] });
export const createContextPermission = (context: Enum.Context, permissions: Enum.Role) => {};
