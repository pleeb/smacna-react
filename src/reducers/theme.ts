import { createMuiTheme } from "@material-ui/core";
import { CHANGE_THEME, Theme } from "../actions/theme";

const defaultTheme = createMuiTheme({
  breakpoints: {
    values: { xs: 0, sm: 980, md: 1080, lg: 1440, xl: 1600 }
  }
});
const themeMap = {
  light: {
    ...defaultTheme,
    palette: {
      secondary: {
        light: "#5f5fc4",
        main: "#283593",
        dark: "#001064",
        contrastText: "#fff"
      },
      error: {
        light: "#5f5fc4",
        main: "#283593",
        dark: "#001064",
        contrastText: "#fff"
      }
    }
  },
  dark: {
    ...defaultTheme,
    palette: {
      type: "dark"
    }
  }
};

export function theme(state = themeMap.light, { type, theme }) {
  switch (type) {
    case CHANGE_THEME:
      return themeMap[theme];
    default:
      return state;
  }
}
