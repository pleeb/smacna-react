import React from "react";
import _ from "lodash";
import util from "util";
import { connect } from "react-redux";
import { ITime, IRequest, IReport } from "./";
import {
  CardHeader,
  CardContent,
  CardActions,
  Button,
  withStyles,
  LinearProgress,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from "@material-ui/core";
// import { Card, Table, TableBody, TableCell, TableHead, TableRow } from "./table";

export const NetStats = connect((state: any) => ({ report: state.report }))(
  ({
    report: {
      time: { query, latency, total }
    }
  }: {
    report: IReport;
  }) => {
    return (
      <CardContent style={{ display: "flex", flexDirection: "column", flexShrink: 1, flexGrow: 0 }}>
        <Table padding="checkbox">
          <TableHead>
            <TableRow>
              <TableCell
                component="th"
                scope="row"
                style={{
                  color: "dodgerblue",
                  fontSize: "14px",
                  letterSpacing: "1.5px"
                }}
              >
                <strong>Netstats</strong>
              </TableCell>
              <TableCell numeric>
                <strong>Time(ms)</strong>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                Query
              </TableCell>
              <TableCell numeric>{query} ms</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                Latency
              </TableCell>
              <TableCell numeric>{latency} ms</TableCell>
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">
                <strong>Total</strong>
              </TableCell>
              <TableCell numeric>
                <strong>{total} ms</strong>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </CardContent>
    );
  }
);
