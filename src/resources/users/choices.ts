import { createChoice, FieldType } from "../";

export const attribute = [
  createChoice("id"),
  createChoice("username"),
  createChoice("last_name"),
  createChoice("first_name"),
  createChoice("title"),
  createChoice("email"),
  createChoice("created_at"),
  createChoice("updated_at")
];

